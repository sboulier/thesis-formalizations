Require Import Category FibRepl Cylinder Id Equivalences.




Set Implicit Arguments.
(* g is a retract of f *)
(* f in the middle, g on the side *)
Record Retract {A B} (g : A -> B) {A' B'} (f : A' -> B') :=
  { ret_s : A -> A' ;
    ret_r : A' -> A ;
    ret_s' : B -> B' ;
    ret_r' : B' -> B ;
    ret_sr : ret_r o ret_s == idmap ;
    ret_sr' : ret_r' o ret_s' == idmap;
    ret_H1 : f o ret_s == ret_s' o g ;
    ret_H2 : ret_r' o f == g o ret_r }.
Unset Implicit Arguments.

Global Arguments Build_Retract {A B g A' B' f} s r s' r' sr sr' H1 H2 : rename.

Infix "RetractOf" := Retract (at level 30).

Lemma LP_Retract {A A' B' B C C' D D'}
      {f: A -> A'} {g: B' -> B} {f': C -> C'} {g': D' -> D}
      (Hf : f' RetractOf f) (Hg : g' RetractOf g)
  : LP (C:=TYPE) f g -> LP (C:=TYPE) f' g'.
Proof.
  intros H F G H1; cbn in *.
  assert (X: g o (ret_s Hg o F o ret_r Hf) == ret_s' Hg o G o ret_r' Hf o f). {
    intro. refine (ret_H1 Hg _ E@ _). apply Eap.
    refine (Eap10 H1 _ E@ _). apply Eap.
    exact (ret_H2 Hf _)^E. }
  specialize (H ((ret_s Hg) o F o (ret_r Hf)) ((ret_s' Hg) o G o (ret_r' Hf)) (funext X)).
  destruct H as [h [H2 H3]]; cbn in *.
  exists ((ret_r Hg) o h o (ret_s' Hf)). split; apply funext; intro x; simpl.
  - transitivity (ret_r Hg (h (f (ret_s Hf x)))).
    repeat apply Eap. exact (ret_H1 Hf _)^E.
    transitivity (ret_r Hg (ret_s Hg (F (ret_r Hf (ret_s Hf x))))).
    apply Eap. apply (Eap10 H2).
    transitivity (F (ret_r Hf (ret_s Hf x))).
    apply (ret_sr Hg). apply Eap. apply (ret_sr Hf).
  - refine ((ret_H2 Hg _)^E E@ _).
    refine (Eap _ (Eap10 H3 _) E@ _).
    refine (ret_sr' Hg _ E@ _).
    apply Eap. exact (ret_sr' Hf _).
Defined.

Definition retract_id {A B} (f: A -> B) : f RetractOf f.
Proof.
  unshelve eapply Build_Retract; intro; cbn; try reflexivity.
Defined.


Record IsFib {A B} (f : A -> B) := (* F *)
  { fib_A : Type ;
    fib_P : fib_A -> Type ;
    fib_Fib : UFib fib_P;
    fib_H : f RetractOf (π1 fib_P) }.

Arguments IsFib A B f : clear implicits.
Arguments Build_IsFib {A B f B'} P {FibP} H : rename.



Definition fiber {A B} (f: A -> B) := fun y => exists x, repl_f f x I~ η y.

Definition id_fiber {A B} (f : A -> B) {y x x' p p'}
           (q : x I~ x') (q' : p I~ Iap (repl_f f) q I@ p')
  : Id (fiber f y) (x; p) (x'; p').
Proof.
  destruct_Id q. clear x' q.
  rewrite Iap_1 in q'.
  pose proof (q' I@ Iconcat_1p _).
  clear q'. destruct_Id X. exact I1.
Defined.


Let f' {A B} (f: A -> B) : A -> ∃ y, fiber f y
  := fun x => (f x ; η x ; idrefl (η (f x))).


Definition π1_F {A B} (f: A -> B) : UFib (fiber f).
Proof.
  exact _.
Defined.


Definition LP_f'_F {A B} (f: A -> B)
  : LLP (C:=TYPE) IsFib (f' f).
Proof.
  intros A'' B'' g [B' P HP Hg].
  refine (LP_Retract (retract_id _) Hg _).
  intros F G H; cbn in *.
  transparent assert (α: ((∃ y, fiber f y) -> ∃ y, P (G y))). {
    refine (fun y => (y; _)). destruct y as [y [x p]].
    revert x p. refine (repl_ind' _ _).
    cbn; intros x p.
    pose proof (Etransport P (Eap10 H x) (F x).2); cbn in X.
    (* refine (Itransport (P o G) _ X). unfold f'. *)
    refine (repl_J (fun y p => P (G (y; (η x; p)))) p X). }
  transparent assert (β: ((∃ y, P (G y)) -> sigT P)). {
    exact (fun w => (G w.1; w.2)). }
  exists (β o α). split; [|reflexivity].
  apply funext; intro x.
  refine (eq_sigma _ (Eap10 H _)^E _); cbn.
  rewrite repl_J_refl.
  now rewrite Etransport_Vp.
Defined.


Definition wfs_AC_F : weak_factorization_system (@LLP TYPE IsFib) IsFib.
Proof.
  unshelve eapply Build_weak_factorization_system; cbn.
  - intros A B f. exists (∃ y, fiber f y).
    exists (f' f). exists (π1 _).
    split; [reflexivity|]. split.
    + apply LP_f'_F.
    + refine (Build_IsFib _ (retract_id _)).
  - intros A B f; split; auto.
  - intros A B f; split; intros Hf.
    + intros A' B' g Hg. now apply Hg.
    + assert (LP (C:=TYPE) (f' f) f). {
        apply Hf. apply LP_f'_F. }
      specialize (X idmap (π1 _) E1); cbn in X. destruct X as [g [Hg1 Hg2]].
      refine (Build_IsFib (fiber f) _).
      refine (Build_Retract (f' f) g idmap idmap _ _ _ _);
        intro; try reflexivity; cbn.
      exact (Eap10 Hg1 _). exact (Eap10 Hg2^E _).
Defined.


Record AFib A B (f : A -> B) := (* F *)
  { afib_A : Type ;
    afib_P : afib_A -> Type ;
    afib_Fib : UFib afib_P ;
    afib_H1 : forall y, Contr (afib_P y) ;
    afib_H2 : f RetractOf (π1 afib_P) }.

Global Arguments Build_AFib {A B f B'} P {FibP} HP H : rename.

Definition AFib_Fib {A B} (f: A -> B)
  : AFib _ _ f -> IsFib _ _ f.
Proof.
  intros [B' P FibP HP H].
  ref (Build_IsFib _ H).
Defined.



Definition LP_Cyl_AF {A B} (f : A -> B) (P : B -> Type)
           (HP : UFib P) (HP' : forall y, Contr (P y))
           (F : forall x, P (f x))
  : {α : forall y, P y & α o f == F}.
Proof.
  pose (P' := lift P).
  unshelve econstructor.
Abort.

Section Cylinder.
  Context {X Y} {f: X -> Y}.

  Definition top' : X -> exists y, Cyl (repl_f f) (η y)
    := fun x => (f x; top (f:= repl_f f) (η x)).
  Definition base' : Y -> exists y, Cyl (repl_f f) (η y)
    := fun y => (y; base (η y)).
  (* Definition cyl_eq' `{Fibrant Y} : ∀ x, base' (f x) = top' x *)
  (* := fun x => path_sigma (Cyl f) 1 (cyl_eq x). *)
End Cylinder.

Definition LP_top'_AF {A B} (f: A -> B)
  : LLP (C:=TYPE) AFib (top' (f:=f)).
Proof.
  intros A'' B'' g [B' P FibP HP Hg].
  refine (LP_Retract (retract_id _) Hg _).
  clear g Hg. intros F G H; cbn in *.
  unshelve eapply exist.
  - intro w. exists (G w).
    transparent assert (P': ((exists y, Cyl (repl_f f) y) -> Type)). {
      intro y. refine (_ (repl_sigma (Cyl (repl_f f)) y)).
      unshelve eapply repl_rec'. exact (P o G). }
    assert (forall y w, P' (y; w)). {
      unshelve refine (Cyl_ind (fun y w => P' (y; w)) _ _ _).
      + unfold P'; exact _.
      + unshelve refine (repl_ind' (fun x => P' (repl_f f x; top x)) _).
        unfold P'; exact _.
        intro x. exact ((Eap10 H x) E# (F x).2).
      + unshelve eapply repl_ind'.
        unfold P'; exact _.
        intro y. refine (@contr _ (HP _)).1.
      + unfold P'. unshelve eapply repl_ind'. exact _.
        intro. apply paths_Id.
        exact (path_contr (HP _) _ _). }
    exact (X (η w.1) w.2).
  - split. 2: reflexivity.
    apply funext; intro x. unshelve eapply eq_sigma. exact (Eap10 H x)^E.
    refine (Eap _ _ E@ _).
    exact (Cyl_ind_top _ _ _ _ (η x)).
    cbn. rewrite Etransport_Vp. reflexivity.
Defined.

Definition wfs_C_AF : weak_factorization_system (@LLP TYPE AFib) AFib.
Proof.
  unshelve eapply Build_weak_factorization_system; cbn.
  - intros A B f. exists (exists y, Cyl (repl_f f) (η y)).
    exists top'. exists (π1 _).
    split; [reflexivity|]. split.
    + apply LP_top'_AF.
    + refine (Build_AFib _ _ (retract_id _)); cbn.
      intro. apply Cyl_Contr.
  - intros; split; auto.
  - intros A B f; split; intros Hf.
    + intros A' B' g Hg. now apply Hg.
    + assert (LP (C:=TYPE) (top' (f:=f)) f). {
        apply Hf. apply LP_top'_AF. }
      specialize (X idmap (π1 _) E1); cbn in X. destruct X as [g [Hg1 Hg2]].
      refine (Build_AFib (fun y => Cyl (repl_f f) (η y)) _ _); cbn.
      intro. apply Cyl_Contr.
      refine (Build_Retract top' g idmap idmap _ _ _ _);
        intro; try reflexivity; cbn.
      exact (Eap10 Hg1 _). exact (Eap10 Hg2^E _).
Defined.

Definition WEquiv {A B} (f : A -> B) := IsEquiv (repl_f f).

Definition weak_eq_retract {A B A' B'}
           (f : A -> B) (f' : A' -> B')
           (Hf' : f' RetractOf f) (Hf : WEquiv f)
  : WEquiv f'.
Proof.
  destruct Hf as [g Hg1 Hg2 _].
  destruct Hf' as [s r s' r' sr sr' Hf1 Hf2].
  refine (isequiv_adjointify (repl_f r o g o repl_f s') _ _); intro.
  - rewrite repl_f_compose. rewrite (funext Hf2)^E.
    rewrite <- repl_f_compose. refine (Iap _ (Hg1 _) I@ _).
    rewrite repl_f_compose. rewrite (funext sr'). apply eq_to_Id, repl_f_idmap.
  - rewrite repl_f_compose. rewrite <- (funext Hf1). rewrite <- (repl_f_compose s).
    refine (Iap _ (Hg2 _) I@ _). rewrite repl_f_compose.
    rewrite (funext sr). rewrite repl_f_idmap. exact I1.
Defined.

Definition TYPEH : Category.
Proof.
  unshelve eapply Build_Category.
  - exact TypeH.
  - exact (fun A B => A.1 -> B.1).
  - exact (fun A => idmap).
  - exact (fun A B C g f => g o f).
  - reflexivity.
  - reflexivity.
  - reflexivity.
Defined.


Definition two_out_of_three_IsEquiv
  : @two_out_of_three TYPEH (fun A B f => IsEquiv f).
(* Proof. *)
(*   intros A B C f g; cbn in *. repeat split; intros H1 H2. *)
(*   - rewrite <- (funext (repl_f_compose f g)). *)
(*     now apply isequiv_compose. *)
(*   - rewrite <- (funext (repl_f_compose f g)) in H2. *)
(*     apply (cancelL_isequiv (Hg:=H1) (Hgf:=H2) (repl_f g)). *)
(*   - rewrite <- (funext (repl_f_compose f g)) in H1. *)
(*     apply (cancelR_isequiv (Hf:=H2) (Hgf:=H1) (repl_f f)). *)
(* Defined. *)
Admitted.

Definition two_out_of_three_weak_equiv
  : @two_out_of_three TYPE (fun A B f => WEquiv f).
Proof.
  unfold WEquiv. intros A B C f g; cbn.
  simple refine (let H := two_out_of_three_IsEquiv
                            (repl A; _) (repl B; _) (repl C; _)
                            (repl_f f) (repl_f g) in _); try (cbn; exact _).
  repeat split; intros H1 H2.
  all: rewrite <- (funext (repl_f_compose f g)) in *; now apply H.
Defined.


Definition AFib_aux {B} {P: B -> Type} {FibP: UFib P} (H: WEquiv (π1 P))
  : forall y, Contr (P y).
Proof.
  destruct H as [g Hg1 Hg2 Hg3]. intro y.
  unshelve refine (let f := repl_ind' (A:=sigT P)
                             (fun z => repl_rec' P (repl_f pr1 z)) pr2 in _).
  all: try exact _.
  unshelve econstructor.
  - change (repl_rec' P (η y)).
    refine (Itransport (repl_rec' P) (Hg1 (η y)) (f (g (η y)))).
  - intro w. eapply Id_paths.
    refine (_ I@ _). eapply Iap10, Iap. exact (Hg3 (η (y; w))).
    refine ((Itransport_compose (repl_rec' P) (repl_f (π1 P)) (Hg2 (η (y; w))) _)^I I@ _).
    exact (IapD f (Hg2 (η (y; w)))).
Defined.


Definition AFib_ok {A B} (f: A -> B)
  : AFib _ _ f  <->  WEquiv f × IsFib _ _ f.
Proof.
  split; intro H.
  - split.
    + destruct H as [B' P FibP HP Hf].
      eapply weak_eq_retract. exact Hf. clear f Hf.
      unshelve eapply isequiv_adjointify.
      * unshelve eapply repl_rec'. intro x. apply η. exists x. apply HP.
      * refine (repl_ind' _ _); cbn. intro; exact I1.
      * refine (repl_ind' _ _); cbn. intros [x w]; cbn.
        pose ((HP x).2 w)^. set (HP x).1 in *. clearbody p p0.
        destruct_path p. exact I1.
    + destruct H; econstructor; eassumption.
  - refine (Build_AFib (fiber f) _ _).
    + pose proof (two_out_of_three_weak_equiv _ _ _ (f' f) pr1); cbn in X.
      destruct X as [_ [_ H2]]. specialize (H2 (fst H)).
      refine (AFib_aux (P:=fiber f) (H2 _)).
      clear H2 H.
      unshelve eapply isequiv_adjointify.
      * unshelve eapply repl_rec'. exact (fun w => w.2.1).
      * refine (repl_ind' _ _); cbn. intros [y [x p]]; cbn.
        revert x p. refine (repl_ind' _ _); cbn. intros x p.
        transparent assert (F : ((exists y, η (f x) I~ y) -> repl (exists y, fiber f y))). {
          clear. intros [y p]; revert y p.
          refine (repl_ind' _ _). intros y p.
          apply η. exact (y; η x; p). }
        change (F (η (f x); I1) I~ F (η y; p)).
        apply Iap. apply IContr_singleton.
      * refine (repl_ind' _ _); cbn. intro; exact I1.
    + assert (LP (C:=TYPE) (f' f) f). {
        apply LP_f'_F. apply H. }
      specialize (X idmap pr1 E1); cbn in X. destruct X as [g [Hg1 Hg2]].
      refine (Build_Retract (f' f) g idmap idmap _ _ _ _);
        intro; try reflexivity; cbn.
      exact (Eap10 Hg1 _). exact (Eap10 Hg2^E _).
Defined.


Definition LLPAFib_ok {A B} (f : A -> B)
  : LLP (C:=TYPE) IsFib f  <->  (WEquiv f × LLP (C:=TYPE) AFib f).
Proof.
  split.
  - intro H; split.
    + unshelve refine (let X := H _ B pr1 _ (f' f) idmap E1 in _).
      refine (Build_IsFib _ (retract_id _)).
      destruct X as [g [Hg1 Hg2]]; cbn in *.
      unshelve eapply isequiv_adjointify.
      * unshelve eapply repl_rec'. exact (fun w => (g w).2.1).
      * refine (repl_ind' _ _); cbn.
        intro x; pose (p := (g x).2.2). cbn in *.
        refine (p I@ _).
        apply eq_to_Id, Eap. exact (Eap10 Hg2 _).
      * refine (repl_ind' _ _); cbn.
        intro x. rewrite (Eap10 Hg1 x). exact I1.
    + intros A' B' F Hf. apply H. now apply AFib_Fib.
  - intros [H2 H1] A' B' g Hg.
    refine (LP_Retract (f:=f' f) _ (retract_id _) _);
      [|now apply LP_f'_F].
    clear A' B' g Hg.
    assert (X : AFib (∃ y, fiber f y) B pr1). {
      destruct H2 as [g Hg1 Hg2 Hg3]. clear H1.
      refine (Build_AFib _ _ (retract_id _)).
      intro y; cbn. exists (g (η y); Hg1 _).
      intros [x p]. eapply Id_paths. revert x p.
      refine (repl_ind' _ _); cbn.
      intros a p.
      unshelve refine (_ I@ _).
      + exists (g (η (f a))). refine (_ I@ p). apply Hg1.
      + unshelve eapply id_fiber. exact (Iap g p^I).
        set (η y) in *. clearbody r.
        destruct_Id p. refine ((Iconcat_1p _)^I I@ Iconcat2 _ (Iconcat_p1 _)^I).
        rewrite Iinverse_1. rewrite !Iap_1. exact I1.
      + unshelve eapply id_fiber. exact (Hg2 (η a)).
        refine (Iconcat2 _ I1).
        exact (Hg3 (η a)). }
    specialize (H1 (sigT (fiber f)) B pr1 X (f' f) idmap E1); clear X.
    destruct H1 as [Ɣ [H H']]; cbn in *.
    unshelve eapply (Build_Retract idmap idmap Ɣ pr1); intro; try reflexivity.
    exact (Eap10 H' _). exact (Eap10 H^E _).
Defined.



Definition type_model_structure : model_structure TYPE.
Proof.
  unshelve econstructor.
  - exact (fun A B f => WEquiv f).
  - exact IsFib.
  - exact (@LLP TYPE AFib).
  - apply two_out_of_three_weak_equiv.
  - eapply wfs_iff_R. apply @AFib_ok.
  exact wfs_C_AF.
  - eapply wfs_iff_L. apply @LLPAFib_ok. exact wfs_AC_F.
Defined.

(* Print Assumptions type_model_structure. *)


(* F are retracts of their π1 : Σ fiber -> B *)
Definition F_caract {A B} {f: A -> B}
  : IsFib _ _ f <-> ∃ (g : sigT (fiber f) -> A), f o g == pr1 × g o (f' f) == idmap.
Proof.
  split.
  - intros [B' P IsFibP [s r s' r' rs rs' H1 H2]].
    unshelve eapply exist.
    + intros [y [x p]]. apply r. exists (s' y).
      revert x p. unshelve refine (repl_ind' _ _); cbn. intros x p.
      refine (repl_J (fun y _ => P (s' y)) p _); cbn.
      refine ((H1 x) E# _). exact (s x).2.
    + split; cbn.
      * intros [y [x p]]; cbn.
        refine ((H2 _)^E E@ _); cbn. apply rs'.
      * intro x. refine (_ E@ rs x).
        apply Eap. unshelve eapply eq_sigma. exact (H1 _)^E.
        rewrite repl_J_refl. apply Etransport_Vp.
  - intros [g [H1 H2]].
    refine (Build_IsFib (fiber f) _).
    refine (Build_Retract (f' f) g idmap idmap _ _ _ _);
      intro; cbn; try reflexivity.
    apply H2. sym H1.
Defined.


(* AC are retracts of their f' *)
Definition AC_caract {A B} (f: A -> B)
  : LLP (C:=TYPE) IsFib f <-> ∃ (g: B -> sigT (fiber f)), g o f == f' f × pr1 o g == idmap.
Proof.
  split; intro H.
  - specialize (H (sigT (fiber f)) _ pr1 (Build_IsFib _ (retract_id _))).
    specialize (H (f' f) idmap E1).
    destruct H as [g [H1 H2]]. exists g. split; apply Eap10; assumption.
  - intros X Y g Hg. refine (LP_Retract (f:=f' f) _ (retract_id _) _).
    unshelve eapply (Build_Retract idmap idmap H.1 pr1); try (intro; reflexivity).
    apply H.2. cbn. symmetry. apply H.2.
    now apply LP_f'_F.
Defined.

Record Cofib {A B} (f : A -> B) := (* C *)
  { cofib_A : Type ;
    cofib_B : Type ;
    cofib_k : cofib_A -> cofib_B ;
    cofib_H : f RetractOf (top' (f:=cofib_k)) }.
Arguments Build_Cofib {A B f A' B'} k H : rename.

(* C are injections into cylinders *)
Definition C_caract {A B} (f: A -> B)
  : LLP (C:=TYPE) AFib f <-> Cofib f.
Proof.
  split.
  - intro Hf. unshelve eapply (Build_Cofib f).
    specialize (Hf (exists y, Cyl (repl_f f) (η y)) B pr1).
    specialize (Hf (Build_AFib (fun y => Cyl (repl_f f) (η y))
                               (fun y => Cyl_Contr (η y))
                               (retract_id _))).
    specialize (Hf top' idmap E1).
    destruct Hf as [g [Hg1 Hg2]]; cbn in *.
    unshelve eapply (Build_Retract idmap idmap g pr1); intro; try reflexivity.
    exact (Eap10 Hg2 _). exact (Eap10 Hg1^E _).
  - intros [A' B' f' Hf] A'' B'' g Hg.
    unshelve eapply (LP_Retract Hf (retract_id _)).
      now apply LP_top'_AF.
Defined.

(* C are injections in THEIR cylinders *)
Definition C_caract2 {A B} (f: A -> B)
  : LLP (C:=TYPE) AFib f <-> ∃ (g: B -> (exists y, Cyl (repl_f f) (η y))), g o f == top' × pr1 o g == idmap.
Proof.
  split.
  - intro Hf.
    specialize (Hf (exists y, Cyl (repl_f f) (η y)) B pr1).
    specialize (Hf (Build_AFib (fun y => Cyl (repl_f f) (η y))
                               (fun y => Cyl_Contr (η y))
                               (retract_id _))).
    specialize (Hf top' idmap E1).
    destruct Hf as [g [Hg1 Hg2]]; cbn in *.
    exists g; split; now apply Eap10.
  - intros [g [H1 H2]].
    intros A' B' h H.
    refine (LP_Retract (f:=top' (f:=f)) _ (retract_id _) _).
    + unshelve eapply (Build_Retract idmap idmap g pr1); try (intro; reflexivity).
      exact H2. intro; cbn; sym H1.
    + apply LP_top'_AF. assumption.
Defined.

(* Definition Cofib_W {A B} (f: A -> B) *)
  (* : WEquiv (top' (f:=f)) <-> WEquiv f. *)
(* Proof. *)
  (* assert (H': WEquiv (π1 (fun y => Cyl (repl_f f) (η y)))). { *)
  (*   unshelve eapply isequiv_adjointify. *)
  (*   + unshelve eapply repl_f. exact base'. *)
  (*   + intro; cbn. rewrite repl_f_compose; cbn. rewrite repl_f_idmap. exact I1. *)
  (*   + refine (repl_ind' _ _); cbn. intros [y w]; cbn. *)
  (*     transparent assert (P :(forall y, Cyl (repl_f f) y -> Type)). { *)
  (*       refine (fun y w => _ (repl_sigma   _ (y; w))). *)
  (*       unshelve eapply repl_rec'; cbn. exact (fun w => η (base' w.1) ~ η w). } *)
  (*     assert (forall y w, P y w). { *)
  (*       (* assert (FibrantF2 P). *) *)
  (*     (* unfold P; unshelve eapply FibrantF_compose. *) *)
  (*     unshelve refine (Cyl_ind _ _ _ _).  *)
  (*     + refine (repl_ind' _ _ ). *)
  (*       intro x. cbn. unfold base'. *)
  (*       apply ap, ap. exact (cyl_eq (η x))^. *)
  (*       (* refine (transport (fun y => η (f x; y) ~ _) (cyl_eq (η x)) P1). *) *)
  (*     + refine (repl_ind' _ _ ). *)
  (*       intro; exact P1. *)
  (*     + refine (repl_ind' _ _ ). *)
  (*       intro; subst P; unfold base'; cbn. *)
  (*       admit. } *)
  (*       (* intro; cbn. subst P; cbn. unfold base'. *) *)
  (*       (* exact P1. } *) *)
  (*   apply paths_Id; exact (X (η y) w). } *)
  (* split; intro H. *)
  (* - admit. *)
  (* - admit. *)
  (* (* - pose proof (isequiv_compose H H'). *) *)
  (* (*   cbn in X; rewrite (funext (repl_f_compose _ _)) in X. *) *)
  (* (*   exact X. *) *)
  (* (* - refine (cancelL_isequiv (repl_f (π1 (fun y => Cyl (repl_f f) (η y))))). *) *)
  (* (*   exact H'. cbn. *) *)
  (* (*   rewrite (funext (repl_f_compose *) *)
  (* (*                         top' (π1 (fun y => Cyl (repl_f f) (η y))))). *) *)
  (* (*   exact H. *) *)
(* Admitted. *)

(* Record ACofib {A B} (f : A -> B) := (* AC *) *)
(*   { acofib_A : Type ; *)
(*     acofib_B : Type ; *)
(*     acofib_k : acofib_A -> acofib_B ; *)
(*     acofib_Hk : WEquiv acofib_k ; *)
(*     acofib_H : f RetractOf (top' (f:=acofib_k)) }. *)

(* Arguments Build_ACofib {A B f A' B'} k Hk H : rename. *)

(* (* AC are injections into cylinders of weak equivalences *) *)
(* Definition AC_caract2 {A B} (f: A -> B) *)
(*   : LLP (C:=TYPE) IsFib f <-> ACofib f. *)
(* Proof. *)
(*   unshelve eapply transitive_iff. 2: apply LLPAFib_ok. split. *)
(*   - intros [Hf H]. *)
(*     refine (Build_ACofib f Hf _). *)
(*     specialize (H (exists y, Cyl (repl_f f) (η y)) B pr1). *)
(*     assert (AFib (exists y, Cyl (repl_f f) (η y)) B pr1). { *)
(*       refine (Build_AFib _ _ (retract_id _)).  *)
(*       intro; cbn. apply Cyl_Contr. } *)
(*     specialize (H X top' idmap E1); clear X. *)
(*     destruct H as [g [H1 H2]]; cbn in *. *)
(*     refine (Build_Retract idmap idmap g pr1 _ _ _ _); *)
(*       intro; try reflexivity. *)
(*     apply (Eap10 H2). exact (Eap10 H1 _)^E. *)
(*   - intros [A' B' k Hk H]; split. *)
(*     + refine (weak_eq_retract _ _ H _); clear H. *)
(*       revert Hk. apply Cofib_W. *)
(*     + apply C_caract. refine (Build_Cofib k H). *)
(* Defined. *)

Definition IsInjEq {A B} (f: A -> B)
  := ∃ (r: B -> repl A) (H1: r o f == η) (H2: (repl_f f) o r I~~ η),
  forall x, H2 (f x) = eq_to_Id (Eap (repl_f f) (H1 x)).

(* AC are injective equivalences *)
Definition AC_InjEq {A B} (f: A -> B)
  : LLP (C:=TYPE) IsFib f <-> IsInjEq f.
Proof.
  eapply transitive_iff. apply AC_caract. split.
  - intros [g [H1 H2]].
    unshelve refine (_; _; _; _).
    + exact (fun y => (g y).2.1).
    + intro x. exact (Eap (fun w => w.2.1) (H1 x)).
    + intro y. pose (g y).2.2. cbn in *.
      refine (Iconcat_pE (g y).2.2 _). exact (Eap η (H2 y)).
    + intro x; cbn.
      pose (EapD (fun w : sigT (fiber f) => w.2.2) (H1 x)^E); cbn in *.
      rewrite <- e; clear e.
      etransitivity. eapply Eap10, Eap.
      exact (Etransport_Id_FlFrE (f:=fun w => repl_f f w.2.1) (g:= η o pr1)
                                 (H1 x)^E E1).
      refine (Iconcat_pE_ETP _ _ E@ Eap _ _).
      apply uip.
  - intros [s [H1 [H2 H3]]].
    unshelve eapply exist. refine (fun y => (y; s y; H2 y)).
    split; cbn.
    intro x. refine (eq_sigma _ E1 _); cbn.
    refine (eq_sigma _ (H1 x) _). rewrite (H3 x).
    (* clear. set (H1 x) in *. set (η x) in *. destruct e. *)
    refine (Etransport_Id_FlFrE (H1 x) (Eap (repl_f f) (H1 x)) E@ _).
    refine (Eap eq_to_Id (y:=E1) _).
    rewrite Eap_const, Econcat_p1.
    rewrite Eap_V. unshelve eapply Econcat_Vp.
    reflexivity.
Defined.

(* F are retracts of their π1 : Σ Cyl -> B *)
Definition AF_caract {A B: Type} (f: A -> B)
  : AFib _ _ f <-> ∃ g : (∃ x, Cyl (repl_f f) (η x)) -> A, g o top' == idmap × f o g == pr1.
Proof.
  split.
  - intro H.
    pose proof (LP_top'_AF f _ _ f H idmap pr1 E1).
    destruct X as [g [Hg1 Hg2]]; cbn in *.
    exists g. split; refine (Eap10 _); assumption.
  - intros [g [Hg1 Hg2]]. unshelve eapply (Build_AFib (fun y => Cyl (repl_f f) (η y))).
    intro; cbn. apply Cyl_Contr.
    refine (Build_Retract top' g idmap idmap _ _ _ _); cbn; try reflexivity.
    exact Hg1. intro; sym Hg2.
Defined.


Definition results := (type_model_structure, @F_caract,
                       @AC_caract, @C_caract2, @AF_caract, @AC_InjEq).

Print Assumptions results.
