Global Set Universe Polymorphism.

Axiom proof_admitted : forall {a}, a.
Tactic Notation "myadmit" := exact proof_admitted.
Tactic Notation "admitok" := myadmit.

Tactic Notation "sym" simple_intropattern(H) := symmetry; apply H.

Tactic Notation "ref" uconstr(p) := simple refine p.

(* From the HoTT library. *)
Tactic Notation "transparent" "assert" "(" ident(name) ":" constr(type) ")" :=
  simple refine (let __transparent_assert_hypothesis := (_ : type) in _);
  [
  | (let H := match goal with H := _ |- _ => constr:(H) end in
    rename H into name) ].
Tactic Notation "transparent" "assert" "(" ident(name) ":" constr(type) ")" "by" tactic3(tac) := let name := fresh "H" in transparent assert (name : type); [ solve [ tac ] | ].



Notation idmap := (fun x => x).

Notation compose := (fun g f x => g (f x)).
Notation " g 'o' f " := (compose g f) (at level 40, left associativity) : core_scope.

Set Primitive Projections.
Record sigT {A} (P : A -> Type) := exist { π1 : A ; π2 : P π1 }.
Arguments exist {_} _ _ _.
Scheme sigT_rect := Induction for sigT Sort Type.

Notation "{ x & P }" := (sigT (fun x => P)) (only parsing) : type_scope.
Notation "{ x : A & P }" := (sigT (A:=A) (fun x => P)) (only parsing) : type_scope.
Notation "'exists' x .. y , P"
  := (sigT (fun x => .. (sigT (fun y => P)) ..))
       (at level 200, x binder, y binder, right associativity) : type_scope.
Notation "∃ x .. y , P"
  := (sigT (fun x => .. (sigT (fun y => P)) ..))
       (at level 200, x binder, y binder, right associativity) : type_scope.
Notation "( x ; y )" := (exist _ x y) : core_scope.
Notation "( x ; y ; z )" := (x ; (y ; z)) : core_scope.
Notation "( x ; y ; z ; u )" := (x ; (y ; (z; u))) : core_scope.
Notation "( x ; y ; z ; u ; v )" := (x ; (y ; (z; (u; v)))) : core_scope.
(* Notation "( x ; y ; .. ; z )" := (exist _ .. (exist _ x y) .. z) : core_scope. *)
Notation pr1 := (π1 _).
Notation pr2 := (π2 _).
Notation "x .1" := (π1 _ x) (at level 3, format "x '.1'") : core_scope.
Notation "x .2" := (π2 _ x) (at level 3, format "x '.2'") : core_scope.

Definition prod A B := sigT (fun _ : A => B).
Notation "A * B" := (prod A B) (at level 40, left associativity) : type_scope.
Notation "A × B" := (prod A B) (at level 90, right associativity) : type_scope.
Definition pair {A B} : A -> B -> A × B := fun x y => (x; y).
Notation "( x , y , .. , z )" := (pair .. (pair x y) .. z) : core_scope.
Definition fst {A B} : A × B -> A := pr1.
Definition snd {A B} : A × B -> B := pr2.


Definition iff (A B : Type) := (A -> B) × (B -> A).
Notation "A <-> B" := (iff A B)%type : type_scope.

Definition transitive_iff {A B C}
  : A <-> B -> B <-> C -> A <-> C.
Proof.
  intros H1 H2. split; firstorder.
Defined.

Open Scope type_scope.


Axiom propext : forall {A B : Prop}, (A <-> B) -> A = B.
Axiom proof_irr : forall {P : Prop} (x y : P), x = y.


(* Axiom or_elim : forall {P Q : Prop} A (g1 : P -> A) (g2 : Q -> A) (g12 : forall x y, g1 x = g2 y), P \/ Q -> A. *)

(* Axiom or_elim_l : forall {P Q : Prop} A (g1 : P -> A) (g2 : Q -> A) g12 x, *)
(*     or_elim A g1 g2 g12 (or_introl x) = g1 x. *)
(* Axiom or_elim_r : forall {P Q : Prop} A (g1 : P -> A) (g2 : Q -> A) g12 y, *)
(*     or_elim A g1 g2 g12 (or_intror y) = g2 y. *)

Definition eq_False (P : Prop) (e : ~ P) : P = False.
  eapply propext; split. exact e. exact (False_ind _).
Defined.

Definition eq_True (P : Prop) (e : P) : P = True.
  eapply propext; split. exact (fun _ => Logic.I). exact (fun _ => e).
Defined.
