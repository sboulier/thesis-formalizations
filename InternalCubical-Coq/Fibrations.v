Require Export Cof.

Definition Comp (e : OI) (A : I -> Type)
  := forall (φ : Cof) (p : forall i, El φ -> A i),
    {a0 : A e & extends (φ; p e) a0}
    -> { a1 : A !e & extends (φ; p !e) a1}.

Definition Comp' (e : OI) (A : I -> Type)
  := forall φ (p : forall i : I, El φ \/ i = e -> A i),
        {a1 : A !e & extends (φ; fun x => p !e (or_introl x)) a1}.

Definition CompComp' e A : Comp' e A <-> Comp e A.
Proof.
  split; intro H.
  - intros φ p p0.
    simple refine (let XX := H φ _ in _).
    + simple refine (fun i => join _ _ _); cof.
      apply p.
      refine (fun q => _ E# p0.1). exact q^E.
      intros x y; cbn. destruct y.
      exact (p0.2 x).
    + clearbody XX. exists XX.1.
      intro x; cbn in *. rewrite <- (XX.2 x); cbn.
      rewrite join_l. reflexivity.
  - intros φ p.
    refine (H φ (fun i x => p i (or_introl x)) _).
    simple refine (p e _; _). right. reflexivity.
    intro; cbn. eapply Eap. apply proof_irr.
Defined.

Definition SEquiv_CompComp' e A : SEquiv (Comp' e A) (Comp e A).
Proof.
  unshelve econstructor. eapply CompComp'.
  unshelve econstructor. eapply CompComp'.
  - intros H. eapply funext; intro φ.
    eapply funext; intro a. cbn.
    unshelve eapply eq_sigma. 2: eapply proof_irr.
    match goal with
    | |- (H _ ?aa).1 = (H _ ?bb).1 => assert (X : aa = bb);
                                      [|now rewrite X]
    end.
    apply funext; intro i.
    apply funext; intros [w|eq].
    apply @join_l.
    rewrite join_r. now destruct eq.
  - intros H. eapply funext; intro φ.
    eapply funext; intro a.
    eapply funext; intro a0. cbn.
    unshelve eapply eq_sigma. 2: eapply proof_irr.
    match goal with
    | |- (H _ ?aa ?bb).1 = (H _ _ _).1 =>
      refine (@Eap (exists a : forall i : I, φ -> A i, exists a0 : A e, extends (φ; a e) a0)
                   _ (fun X => (H φ X.1 X.2).1) (aa; bb) (a; a0) _)
    end.
    unshelve eapply eq_sigma.
    + apply funext; intro i.
      apply funext; intro w.
      apply join_l.
    + rewrite Etransport_sigma'.
    unshelve eapply eq_sigma. 2: eapply proof_irr.
    cbn. apply @join_r.
Defined.



Class UFib {A} (P : A -> Type)
  := ufib : forall e (a : I -> A), Comp e (P o a).

Notation " 'Fib' A " := (UFib (fun _ : unit => A)) (at level 10).

Definition fib A :
  (forall (e : OI) (φ : Cof) (a : I -> El φ -> A),
      {a0 : A & extends (φ; a e) a0} -> {a1 : A & extends (φ; a !e) a1})
  <-> Fib A.
Proof.
  split; intro H.
  intros φ _. exact (H φ).
  intro φ. exact (H φ (fun _ => tt)).
Defined.


Instance UFib_precomp {A} (B : A -> Type) (HB : UFib B) {A'} (f : A' -> A)
           : UFib (B o f).
Proof.
  intros φ a. exact (HB φ (f o a)).
Defined.

Definition UFib_const {A B} (HA : Fib A) : UFib (fun _ : B => A)
  := UFib_precomp (fun _ => A) _ (fun _ => tt).

Hint Extern 4 (UFib (fun _ :?B => ?A)) => refine (@UFib_const A B _)
                                      : typeclass_instances.

Notation " 'UFib2' P " := (UFib (fun w => P w.1 w.2)) (at level 30).

Instance UFib2_UFib {A} {B : A -> Type} (P : forall x, B x -> Type) (HP : UFib2 P) x
  : UFib (P x)
  := UFib_precomp _ HP (fun w => (x; _)).


Definition Ext_Fib A : Ext A -> Fib A.
  intros H e _ φ p _. apply H.
Defined.

Instance Fib_Type : Fib Type := Ext_Fib _ Ext_Type.


Definition Etransport_comp {A} (P : A -> Type) {H : UFib P} e (a a' : I -> A)
           φ p a0 (q : a == a')
  : Etransport P (q !e) (H e a φ p a0).1 =
    (H e a' φ (fun i w => q i E# p i w)
       (exist (extends (φ; (fun i w => q i E# p i w) e))
              (q e E# a0.1) (fun w => Eap (Etransport P (q e)) (a0.2 w)))).1.
Proof.
  pose proof (funext q).
  assert (q = Eap10 H0) by (apply funext; intro; apply uip).
  rewrite H1; clear H1 q. destruct H0. simpl.
  eapply Eap, Eap. unshelve eapply eq_sigma.
  reflexivity. cbn. funext w. apply uip.
Defined.

Definition eq_comp {A} (P : A -> Type) {H : UFib P} e a φ φ' p p' a0 a0'
           (e1 : φ = φ' :> Cof) (e2 : forall i w, p i w = p' i (e1 E# w))
           (e3 : a0.1 = a0'.1)
  : (H e a φ p a0).1 = (H e a φ' p' a0').1.
  destruct e1.
  assert (e2' : p = p') by (funext i w; apply e2).
  clear e2; destruct e2'.
  assert (e3' : a0 = a0'). {
    apply (eq_sigma _ e3). funext w. apply uip. }
  destruct e3'. reflexivity.
Defined.
  


Require Import Peano_dec.

Instance Fib_nat : Fib nat.
  eapply fib. intros e φ p [x y].
  exists x. intro w; cbn.
  simple refine (let XX := connected (fun i => p i w = x) _ in _).
  - intro. eapply dec_eq_nat.
  - specialize (y w). destruct e; cbn in *; destruct XX; exact y.
Defined.

Definition Fill (e : OI) (A : I -> Type)
  := forall (φ : Cof) (p : forall i, El φ -> A i)
       (a0 : {a0 : A e & extends (φ; p e) a0}),
    { a : forall i, A i & extends (φ; fun w i => p i w) a /\ a e = a0.1 }.

Definition UFill {A} (P : A -> Type) :=
  forall (e : OI) (a : I -> A), Fill e (P o a).

Definition ufill : forall {A} (P : A -> Type), UFib P -> UFill P.
Proof.
  intros A P H.
  intros e a φ p [p0 Hp0].
  simple refine (fun i => _; (conj _ _)).
  - pose (a' := fun i j => a (cnx e i j)).
    simple refine (let XX := (H e (fun j => a (cnx e i j)) (or φ (is_e e i))
                                _ (_; _)).1 in _).
    + intro j. cbn. unshelve eapply join; cof.
      * auto.
      * cbn. intro eq. refine (Etransport (P o a) _ p0).
        refine (_ E@ (Eap (fun i => cnx e i j) eq^E)). auto.
      * cbn. intros w eq.
        rewrite <- (Hp0 w). cbn. set eq^E.
        destruct e0. cbn. set (e_cnx e j).
        destruct e0; reflexivity.
    + cbn. refine (Etransport (P o a) _ p0). auto.
    + intros [w|eq]; cbn.
      * rewrite join_l.
        rewrite <- (Hp0 w). cbn.
        set (cnx_e e i).
        destruct e0; reflexivity.
      * rewrite join_r.
        eapply Eap10, Eap. apply uip.
    + cbn in *. refine (Etransport (P o a) _ XX). auto.
  - intro w; cbn in *.
    funext i. symmetry.
    match goal with
    | |- Etransport _ _ (H ?aa ?bb ?cc ?dd ?ee).1 = _
      => unshelve rewrite <- (H aa bb cc dd ee).2; cbn
    end. left; assumption.
    rewrite join_l.
    set (cnx_ne' e i). destruct e0; reflexivity.
  - cbn.
    match goal with
    | |- Etransport _ _ (H ?aa ?bb ?cc ?dd ?ee).1 = _
      => unshelve rewrite <- (H aa bb cc dd ee).2; cbn
    end. right; reflexivity.
    rewrite join_r. cbn.
    set (cnx_ne' e e). set (e_cnx e !e).
    assert (e0 = e1) by apply uip. destruct H0.
    clear. exact (Etransport_pV (P o a) e0 p0).
Defined.


Definition ufill_begin {A} (P : A -> Type) H e a φ p a0
  : (ufill P H e a φ p a0).1 e = a0.1
  := proj2 (ufill P H e a φ p a0).2.

Definition ufill_end {A} (P : A -> Type) H e a φ p a0
  : (ufill P H e a φ p a0).1 !e = (H e a φ p a0).1.
Proof.
  unfold ufill. cbn.
  rewrite Etransport_compose.
  refine (Eap10 (Eap _ (uip _ _)) _ E@
                Etransport_comp P e _ _ _ _ _ (fun i => (Eap a (ne_cnx' e i))) E@ _).
  cbn; unshelve eapply eq_comp.
  - apply eq_Cof; cbn. apply propext.
    split; intuition. destruct e; cbn in H1.
    contradiction (zero_one H1^E).
    contradiction (zero_one H1).
  - intros i [w|w]; cbn.
    + rewrite join_l.
      rewrite <- (Etransport_compose P a).
      refine (EapD (B:=P o a) (fun i => p i w) (ne_cnx' e i) E@ _).
      eapply Eap, proof_irr.
    + cbn in *. destruct e; cbn in w.
    contradiction (zero_one w^E).
    contradiction (zero_one w).
  - cbn. rewrite (Etransport_compose P a).
    rewrite <- Etransport_pp.
    assert ((Eap a (cnx_e e !e)^E E@ Eap a (ne_cnx' e e)) = E1) by apply uip.
    rewrite H0; reflexivity.
Defined.





Instance UFib_sigma {A} (B : A -> Type) (C : forall x, B x -> Type)
           (HB : UFib B) (HC : UFib (fun w => C w.1 w.2))
  : UFib (fun x => sigT (C x)).
Admitted.

Definition Fib_sigma {A} (B : A -> Type) (HA : Fib A) (HB : UFib B)
  : Fib (sigT B).
  eapply UFib_sigma. assumption.
  intros φ a. exact (HB φ (fun i => (a i).2)).
Defined.

Instance UFib_forall {A} (B : A -> Type) (C : forall x, B x -> Type)
           (HB : UFib B) (HC : UFib (fun w => C w.1 w.2))
  : UFib (fun x => forall y, C x y).
Admitted.


(* Goal forall A (P : A -> Type) φ (a : I -> A), *)
(*     Iso (forall i, El (or φ (is0 i)) -> P (a i)) *)
(*         (exists (p : forall i, El φ -> P (a i)) (p0 : P (a 0)), extends (φ; p 0) p0). *)
(*   intros A P φ a. unshelve econstructor. *)
(*   2: unshelve econstructor. *)
(*   - intro p. exists (fun i x => p i (coe_Prop El_or^E (or_introl x))). *)
(*     simple refine (p 0 (coe_Prop El_or^E (or_intror (coe_Prop _ Logic.I))); _). *)
(*     exact (Eap El is0_zero E@ El_top)^E. *)
(*     intro x; cbn in *. eapply Eap, proof_irr. *)
(*   - intros [p [p0 H]]. intro i. refine (_ o coe_Prop El_or). *)
(*     unshelve eapply join. apply p. *)
(*     intro. refine (Etransport (P o a) _ p0). *)
(*     exact (coe_Prop El_is0 H0)^E. *)
(*     intros x y. cbn. destruct (coe_Prop El_is0 y)^E. exact (H x). *)
(*   - intro p. apply funext; intro i. cbn. apply funext; intro x. *)
(*     set (x' := coe_Prop El_or x). *)
(*     assert (coe_Prop El_or^E x' = x). { *)
(*       apply (Etransport_Vp (fun P : Prop => P)). } *)
(*     clearbody x'. destruct H. revert x'. unshelve eapply joinD. *)
(*     3: intros; apply uip. *)
(*     + intro; cbn. rewrite join_l. reflexivity. *)
(*     + intro; cbn. rewrite join_r. set (coe_Prop (@El_is0 i) x)^E. *)
(*       destruct e. cbn. eapply Eap. apply proof_irr. *)
(*   - intros x. destruct x as [p [p0 H]]. *)
(*     unshelve eapply eq_sigma. *)
(*     + apply funext; intro i. apply funext; intro x. *)
(*       rewrite (Etransport_pV (fun P : Prop => P) El_or), join_l. reflexivity. *)
(*     + unshelve eapply eq_sigma'. 2: apply funext; intro; apply proof_irr. *)
(*       rewrite Etransport_sigma'; cbn. *)
(*       rewrite (Etransport_pV (fun P : Prop => P) El_or), join_r. cbn. *)
(*       match goal with *)
(*       | |- Etransport _ ?pp _ = _ => assert (pp = E1) by apply uip *)
(*       end. rewrite H0; reflexivity. *)
(* (* Defined. *) *)
(* (* stack overflow *) *)
(* Admitted. *)


Definition UFib' {A} (P : A -> Type) := forall e (a : I -> A), Comp' e (P o a).

Definition ufib' {A} (P : A -> Type)
  : UFib' P <-> UFib P.
Proof.
  split; intros H e a; eapply CompComp', H.
Defined.

Definition Fib' A
  : (forall (e : OI) φ (a : forall i : I, El φ \/ i = e -> A),
        {a1 : A & extends (φ; fun x => a !e (or_introl x)) a1})
    <-> Fib A.
  eapply transitive_iff. 2: apply ufib'.
  unfold UFib', Comp'.
  split. intros H e _. exact (H e).
  intros H e. exact (H e (fun _ => tt)).
Defined.

Notation "'DFib' P" := (forall x, Fib (P x)) (at level 10).

Instance UFib_DFib {A} (P : A -> Type) (HA : UFib P) : DFib P.
Proof.
  intro x. refine (UFib_precomp P _ (fun _ => x)).
Defined.

(* Definition comp (A : I -> Type) {HA : UFib A} (e : OI) (φ : Cof) *)
(*            (a : forall i : I, φ \/ i = e -> A i) *)
(*   : exists a1 : A !e, extends (φ; fun x => a !e (or_introl x)) a1 *)
(*   := snd (UFib' A) HA e φ idmap a. *)



(* Definition discrete A := isIso (fun a : A => (fun _ : I => a)). *)

(* Definition Fib_discrete A (HA : discrete A) : Fib A. *)
(*   destruct HA as [g gh fg]. *)
(*   intros φ _ p a0. *)
(*   exists a0.1. unfold extends in *; cbn in *. *)
(*   intro x. refine (_ E@ a0.2 x). *)
(*   specialize (fg (fun i => p i x)). *)
(*   refine (Eap10 fg^E 1 E@ Eap10 fg 0). *)
(* Defined. *)


Definition Trans {A} (P : A -> Type) :=
  forall (e : OI) (a : I -> A) (φ : Cof) (cst : φ -> forall i, a e = a i) (p0 : P (a e)),
  exists p1 : P (a !e), forall w, cst w !e E# p0 = p1.


Definition UFib_Trans {A} (P : A -> Type) (HA : UFib P)
  : Trans P.
Proof.
  intros e a φ cst p0.
  simple refine ((HA e a φ _ _).1; _).
  - exact (fun i w => Etransport P (cst w i) p0).
  - exists p0. intro; cbn. now rewrite (uip (cst x e) E1).
  - intros w; cbn.
    match goal with
    | |- _ = (HA ?aa ?bb ?cc ?dd ?ee).1 =>
      unshelve rewrite <- (HA aa bb cc dd ee).2; cbn
    end. exact w. reflexivity.
Defined.


Definition Etransport_trans {A} {P : A -> Type} (TA : Trans P) e {a a' : I -> A}
           φ p a0 (q : a == a')
  : Etransport P (q !e) (TA e a φ p a0).1 =
    (TA e a' φ (fun w i => (q _)^E E@' p w i E@ q i) (q _ E# a0)).1.
Proof.
  pose proof (funext q).
  assert (q = Eap10 H) by (apply funext; intro; apply uip).
  rewrite H0; clear H0 q. destruct H. simpl.
  reflexivity.
Defined.

Definition eq_trans {A} {P : A -> Type} (TA : Trans P) e a φ φ' p p' a0 a0'
           (e1 : φ = φ' :> Cof)
           (e2 : a0 = a0')
  : (TA e a φ p a0).1 = (TA e a φ' p' a0').1.
Proof.
  destruct e1, e2.
  assert (e3 : p = p') by (funext w i; apply uip).
  destruct e3. reflexivity.
Defined.


Definition TransFib_HFib {A} (P : A -> Type) (HA : DFib P) (TA : Trans P)
  : UFib P.
Proof.
  intros e a φ p p0.
  pose (p' := fun i j => a (cnx !e i j)).
  assert (fcst : forall (u : φ) (i : I) , i = !e -> forall (j : I), p' i e = p' i j). {
    intros u i H j. subst p'; cbn. rewrite H.
    autorewrite with cnx. reflexivity. }
  transparent assert (f' : (forall (u : φ) (i : I), exists f' : P (a !e),
                                 forall w, fcst u i w !e E# p (cnx !e i e) u
                                      = Etransport (P o a) (cnx_e _ _)^E f')). {
    intros u i.
    pose (TA e (p' i) (is_e !e i) (fcst u i) (p _ u)).
    refine (Etransport (P o a) (cnx_e _ _) s.1; _).
    intro w; rewrite Etransport_Vp; apply (s.2 w). }
  unshelve refine (let XX := HA (a !e) e (fun _ => tt) φ (fun u i => (f' i u).1) (_; _)
                   in _).
  - refine (TA e a false (False_rect _) p0.1).1.
  - intro u; cbn in *.
    rewrite <- (p0.2 u); cbn.
   rewrite Etransport_compose.
   rewrite (moveL_Etransport_V _ _ _ _
               (Etransport_trans TA e _ _ _ (fun i => Eap a (cnx_ne e i)))).
   rewrite <-Etransport_pp.
   match goal with
   | |- ?XX E# _ = _ => assert (Y : XX = E1) by apply uip;
                        rewrite Y; clear Y; simpl
   end.
   unshelve eapply eq_trans.
   apply eq_Cof. apply eq_False; cbn.
   intro H; symmetry in H. contradiction (not_neg _ H).
   rewrite <- Etransport_compose.
   exact (EapD (fun e => p e u) (cnx_ne e e)).
  - exists XX.1. intro u; cbn in *. refine (_ E@ XX.2 u); cbn.
    clear XX.
    refine (_ ((f' u !e).2 E1)).
    cbn; intro X. rewrite Etransport_Vp in X.
    rewrite <- X. clear f' X.
    refine ((EapD (fun i => p i u) (e_cnx !e e))^E E@ _).
    rewrite !(Etransport_compose P a). rewrite <- Etransport_pp.
    eapply Eap10, Eap. apply uip.
Defined.


Instance Fib_unit : Fib unit.
Proof.
  apply Fib'.
  intros e φ a. exists tt. intro wx; apply eta_unit.
Defined.



Definition SEquiv_UFib {X} {A B : X -> Type} (s : forall x, SEquiv (A x) (B x))
           (HA : UFib A) : UFib B.
Proof.
  intros e a φ p p0.
  simple refine (let XX := HA e a φ (fun i u => sequiv_g _ (s (a i)) (p i u)) _ in _).
  - exists (sequiv_g _ (s (a e)) p0.1).
    intro u; cbn in *. exact (Eap _ (p0.2 u)).
  - cbn in *. exists (s _ XX.1).
    intro u; cbn in *. rewrite <- (XX.2 u).
    cbn. symmetry; eapply sequiv_fg.
Defined.

Definition SEquiv_Fib {A B} (s : SEquiv A B) (HA : Fib A) : Fib B
  := @SEquiv_UFib unit (fun _ => A) (fun _ => B) (fun _ => s) HA.



Definition TypeH := {A : Type & Fib A}.

Definition Ext_TypeH : Ext TypeH.
Proof.
  intros [φ A]. unfold extends; cbn.
  unshelve econstructor.
  - induction (Cof_dec φ). exact (A a). refine (forall u, (A u).1; _).
    eapply SEquiv_Fib. 2: exact Fib_unit.
    unshelve (econstructor; econstructor).
    + intros _ u; contradiction (b u).
    + exact (fun _ => tt).
    + intro; apply eta_unit.
    + intro f; funext u; contradiction (b u).
  - induction (Cof_dec φ); cbn.
    intro; apply Eap, proof_irr.
    intro u; contradiction (b u).
Defined.

Definition Ext_TypeH' : Ext TypeH.
Proof.
  intros [φ A]. unfold extends; cbn.
  unshelve eexists ((strictness (pr1 o A) _ _).1; _).
  - exact (forall u, (A u).1).
  - intro u. unshelve (econstructor; econstructor). 
    + intros x u'. exact (Etransport (pr1 o A) (proof_irr _ _) x).
    + intro f; exact (f u).
    + intro x; cbn. now rewrite (uip (proof_irr u u) E1).
    + intro f; cbn. funext u'. now destruct (proof_irr u u').
  - admit.
  - intro u; cbn. unshelve eapply eq_sigma'.
    exact ((strictness _ _ _).2.2.1 u).
Abort.
