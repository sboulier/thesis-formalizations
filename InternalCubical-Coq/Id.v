Require Export Paths.

Definition Id A (x y : A) :=
  { p : paths A x y & { φ : Cof & φ -> forall i, p i = x }}.

Notation " a I~ b " := (Id _ a b) (at level 70).

Definition idrefl {A} a : Id A a a := (idpath; true; fun _ _ => E1).

Arguments idrefl [A] a, [A a].

Definition paths_Id {A x y} (p : paths A x y) : Id A x y
  := (p; false; False_rect _).

Definition Id_paths {A x y} (p : Id A x y) : paths A x y := p.1.


Definition eq_Id {A x y} (p q : Id A x y)
           (e : p.1 == q.1) (e' : p.2.1.1 = q.2.1.1)
  : p = q.
Proof.
  destruct p as [p [φ F]], q as [q [φ' F']].
  destruct (eq_paths p q e); clear e.
  destruct (eq_Cof φ φ' e'); clear e'.
  refine (eq_sigma _ E1 _); cbn.
  refine (eq_sigma _ E1 _); cbn.
  apply proof_irr.
Defined.

Definition Etransport_Id {A x y y'} (e : y = y') (q : Id A x y)
           (foo : (q.2).1 -> forall i : I, (Etransport (paths A x) e q.1) i = x
            := fun f i => Eap10 (Eap (pr1_paths A x y') (Etransport_sigma' e q.1)) i
                             E@ (q.2).2 f i)
  : Etransport (Id A x) e q = (Etransport (paths A x) e q.1; (q.2).1; foo).
Proof.
  destruct e; unfold foo; cbn.
  refine (eq_sigma _ E1 _); cbn.
  refine (eq_sigma _ E1 _); cbn.
  apply proof_irr.
Defined.




Definition Itransport {A} (P : A -> Type) {H : UFib P} {x y} (p : Id A x y)
           (u : P x) : P y.
Proof.
  destruct p as [p φ].
  unshelve refine (let XX := H O' (fun i => p i) φ.1 _ _ in _).
  - intros i f; cbn. refine (_ E# u).
    symmetry; now apply φ.2.
  - unshelve econstructor. refine (_ E# u).
    exact p.2.1^E.
    intro w; cbn in *.
    eapply Eap10, Eap, uip.
  - cbn in XX.
    refine (_ E# XX.1).
    exact p.2.2.
Defined.

Notation "p I# x" := (Itransport _ p x)
  (right associativity, at level 65, only parsing) : path_scope.
     
Definition Itransport_refl {A} (P : A -> Type) {H : UFib P} {x} u
  : Itransport P (idrefl x) u = u.
Proof.
  unfold Itransport. simpl.
  match goal with
    |- (H ?aa ?bb ?cc ?dd ?ee).1 = _
    => now unshelve rewrite (H aa bb cc dd ee).2; cbn
  end.
Defined.

Definition J {A x} (P : forall y, Id A x y -> Type) {HP : UFib2 P}
           {y} (p : Id A x y) (u : P x (idrefl x))
  : P y p.
Proof.
  refine (Itransport (fun w => P w.1 w.2) (x:=(x; idrefl x)) (y:=(y;p)) _ u).
  destruct p as [p φ].
  unshelve econstructor.
  - path i. 
    + exists (p i). unshelve econstructor.
    path j.
    * exact (p (inf i j)).
    * cbn. refine (_ E@ p.2.1).
      eapply Eap, inf_zero.
    * cbn. eapply Eap, inf_one.
    * exists (or φ.1 (is0 i)). 
      intros w j; cbn. induction w.
      now apply φ.2.
      refine (_ E@ p.2.1).
      now rewrite H, zero_inf.
    + destruct p as [p [[] []]]. cbn in *; clear y.
      unshelve refine (eq_sigma _ E1 _); cbn.
      eapply eq_Id; cbn.
      * intro j. eapply Eap, zero_inf.
      * apply eq_True. now right.
    + destruct p as [p [[] []]]. cbn in *; clear y.
      unshelve refine (eq_sigma _ E1 _); cbn.
      eapply eq_Id; cbn.
      * intro j; now rewrite one_inf.
      * eapply propext; split; intro w.
        induction w. assumption. symmetry in H. now apply zero_one in H.
        now left.
  - exists φ.1. intros w i. cbn.
    unshelve eapply eq_sigma. now apply φ.2.
    destruct p as [p [[] []]]. cbn in *; clear y.
    rewrite Etransport_Id; cbn.
    eapply eq_Id; cbn.
    * intro j. unfold paths, pathsD.
      rewrite Etransport_sigma'; cbn.
      now apply φ.2.
    * apply eq_True. now left.
Defined.

Definition J_refl {A x} (P : forall y, Id A x y -> Type) {HP : UFib2 P} u
  : J P (idrefl x) u = u.
Proof.
  unfold J. cbn. refine (_ E@ Itransport_refl _ u).
  eapply Eap10, Eap. eapply eq_Id; cbn.
  2: reflexivity.
  intro J.
  unshelve refine (eq_sigma _ E1 _); cbn.
  eapply eq_Id; cbn.
  intro; reflexivity.
  eapply eq_True. now left.
Defined.


Instance UFib_Id {A} (B : A -> Type) (HB : UFib B)
  : UFib (fun x : exists x : A, B x × B x => Id (B x.1) (fst x.2) (snd x.2)).
Admitted.

Instance UFib_Id' {A} (B : A -> Type) (HB : UFib B)
           (t u : forall x, B x)
  : UFib (fun x => Id (B x) (t x) (u x))
  := UFib_precomp _ (UFib_Id B HB) (fun x => (x; (t x, u x))).

Instance UFib_Id_const {A B} (HB : Fib B)
           (t u : A -> B)
  : UFib (fun x => Id B (t x) (u x))
  := UFib_Id' (fun _ => B) _ t u.

Definition Iinverse {A} {FibA: Fib A} {x y : A} (p : x I~ y) : y I~ x
  := Itransport (fun y => y I~ x) p idrefl.

Notation "f I~~ g" := (forall x, f x I~ g x) (at level 70, no associativity) : type_scope.

Definition Iconcat {A} {FibA : Fib A} {x y z : A} (p : x I~ y) (q : y I~ z) : x I~ z
  := Itransport (fun z => x I~ z) q (Itransport (fun y => x I~ y) p idrefl).
Arguments Iconcat {A FibA x y z} !p !q.

Delimit Scope id_scope with id.
Open Scope id_scope.

Notation "p I@ q" := (Iconcat p%id q%id) (at level 20) : id_scope.
Notation "p ^I" := (Iinverse p%id) (at level 3, format "p '^I'") : id_scope.
Notation " 'I1' " := idrefl : id_scope.


Definition eq_to_Id {A : Type} {x y : A} (p : x = y) : x I~ y :=
  match p with
    | eq_refl => idrefl
  end.



Definition Iap {A B: Type} (f: A -> B) {x y: A} (p: x I~ y)
  : f x I~ f y.
Proof.
  exists (ap f p.1). exists p.2.1.
  intros w i. exact (Eap f (p.2.2 w i)).
Defined.

Arguments Iap {A B}%type_scope f {x y} p%id_scope.

Definition IapD {A: Type} x {B: A -> Type} {FibB: UFib B}
           (f: forall a: A, B a) {y: A} (p: x I~ y)
  : p I# (f x) I~ f y.
Proof.
  refine (J (x:=x) (fun y p => Itransport B p (f x) I~ f y) p _^I).
  rewrite Itransport_refl. eapply idrefl.
Defined.
Arguments IapD {A%type_scope x B FibB} f {y} p%path_scope : simpl nomatch.

Definition IapD10 {A} {B : A -> Type} {f g : forall x, B x} (p : f I~ g) x
           : f x I~ g x.
Proof.
  unshelve econstructor. path i. exact (p.1 i x).
  exact (EapD10 p.1.2.1 x).
  exact (EapD10 p.1.2.2 x).
  exists p.2.1. cbn. intros u i. exact (EapD10 (p.2.2 u i) x).
Defined.


Definition Iap10 {A B} {f g : A -> B} (p : f I~ g) x : f x I~ g x
  := IapD10 p x.

Definition Id_sigma {A} (P : A -> Type) {FibP : UFib P}
           {x x'} {y : P x} {y' : P x'}
           (p : x I~ x') (q : p I# y I~ y')
  : (x; y) I~ (x'; y').
Admitted.

Ltac destruct_Id p :=
  let t := type of p in
  match t with
    @Id _ ?x ?y =>
    mark p;
      repeat match goal with
             | [X: context[y] |- _] =>
               match type of X with
               | myid _ _ => fail 1
               | _ => revert X;
                   match goal with
                   | |- forall (X: ?T), ?G => change (forall (X: myid _ T), G)
                   end
               end
             end;
      unmark p;
      generalize y p; clear p y;
      match goal with
      | |- forall y p, @?P y p => let y := fresh y in
                                  let p := fresh p in
                                  intros y p; refine (J (x:=x) P p (y:=y) _)
      end;
      repeat match goal with
             | |- forall (H: myid _ _), _ => let H := fresh H in
                                             intro H; unfold myid in H
             end
  end.

Definition Iap_1 {A B} {HB : Fib B} (f : A -> B) x
  : Iap f (idrefl x) = idrefl (f x).
Proof.
  unshelve eapply eq_sigma. eapply eq_paths. reflexivity.
  cbn. rewrite Etransport_sigma'.
  unshelve eapply eq_sigma. reflexivity.
  cbn. funext i j; apply uip.
Defined.

Definition Itransport_compose {A B} {FibA: Fib A} {FibB: Fib B}
           {x y: A} (P: B -> Type) {FibP : UFib P}
           (f : A -> B) (p : x I~ y) (z : P (f x))
  : Itransport (P o f) p z I~ Itransport P (Iap f p) z.
Proof.
  destruct_Id p. rewrite Iap_1.
  rewrite !Itransport_refl.
  exact I1.
Defined.

Definition Itransport_const {A B} {FibA: Fib A} {FibB: Fib B}
           {x1 x2 : A} (p : x1 I~ x2) (y : B)
  : Itransport (fun _ => B) p y I~ y.
Proof.
  destruct_Id p.
  (* refine (paths_ind x1 (fun x2 p => p # y I~ y) _ p). *)
  rewrite Itransport_refl; exact I1.
Defined.


Definition Iconcat_1p {A} {FibA: Fib A} {x y: A} (p: x I~ y)
  : I1 I@ p I~ p.
Proof.
  destruct_Id p. unfold Iconcat.
  rewrite !Itransport_refl. exact I1.
Defined.

Definition Iconcat_p1 {A} {FibA: Fib A} {x y: A} (p: x I~ y)
  : p I@ I1 I~ p.
Proof.
  destruct_Id p. unfold Iconcat.
  rewrite !Itransport_refl. exact I1.
Defined.

Definition Iinverse_1 {A} {HA : Fib A} x
  : (idrefl x)^I = idrefl x.
Proof.
  unfold Iinverse. exact (Itransport_refl (fun y => y I~ x) I1).
Defined.

Definition Iconcat2 {A} {HA : Fib A} {x y z : A}
           {p p' : x I~ y} {q q' : y I~ z} (h : p I~ p') (h' : q I~ q')
  : p I@ q I~ p' I@ q'.
Proof.
  destruct_Id h.
  destruct_Id h'.
  exact I1.
Defined.

Definition Iconcat_Ep {A} {FibA: Fib A} {x y z : A} (e: x = y) (p: y I~ z) : x I~ z
  := Etransport (fun u => u I~ z) e^E p.

Definition Iconcat_EVp {A} {FibA: Fib A} {x y z : A} (e: y = x) (p: y I~ z) : x I~ z
  := Etransport (fun u => u I~ z) e p.

Definition Iconcat_pE {A} {FibA: Fib A} {x y z : A} (p: x I~ y) (e: y = z) : x I~ z
  := Etransport (fun v => x I~ v) e p.

Definition Iconcat_Ep_ETP {A} {FibA: Fib A} {x y z: A} (e: x = y :> A) (p: y = z)
  : Iconcat_Ep e (eq_to_Id p) = eq_to_Id (e E@ p).
Proof.
  now destruct e, p.
Defined.

Definition Iconcat_EVp_ETP {A} {FibA: Fib A} {x y z: A} (e: y = x :> A) (p: y = z)
  : Iconcat_EVp e (eq_to_Id p) = eq_to_Id (e^E E@ p).
Proof.
  now destruct e, p.
Defined.

Definition Iconcat_pE_ETP {A} {FibA: Fib A} {x y z: A} (p: x = y) (e: y = z)
  : Iconcat_pE (eq_to_Id p) e = eq_to_Id (p E@ e).
Proof.
  now destruct e, p.
Defined.


Definition Etransport_Id_FlFrE {A B} {FibB: Fib B} {f g: A -> B} {x1 x2: A} (p: x1 = x2) (q: f x1 = g x1)
  : Etransport (fun x => f x I~ g x) p (eq_to_Id q) = eq_to_Id ((Eap f p^E) E@ q E@ (Eap g p)).
Proof.
  destruct p; simpl. apply Eap. now destruct q.
Defined.


Definition IContr_singleton {A} {HA: Fib A} {a : A} b (p : a I~ b)
  : Id (exists b, a I~ b) (a; I1) (b; p).
Proof.
  refine (J (fun b p => Id (exists b, a I~ b) (a; I1) (b; p)) p I1).
Defined.
