Require Export Strict_eq Interval.

Axioms (cof : Prop -> Prop)
       (cof0  : forall i, cof (i = 0))
       (cof1  : forall i, cof (i = 1))
       (cof_or  : forall P Q, cof P -> cof Q -> cof (P \/ Q))
       (cof_and  : forall P Q, cof P -> cof Q -> cof (P /\ Q))
       (cof_forall  : forall P : I -> Prop, (forall i, cof (P i)) -> cof (forall i, P i)).

Definition Cof := { P : Prop & cof P }.

Definition El : Cof -> Prop := π1 _.
Coercion El : Cof >-> Sortclass.

Definition false : Cof.
  exists False.
  refine (propext _ E# _).
  split. eapply zero_one. eapply False_ind.
  apply cof1.
Defined.

Definition true : Cof.
  exists True.
  refine (@propext (0 = 0) _ _ E# _).
  split; intuition.
  apply cof0.
Defined.

Definition and (φ φ' : Cof) : Cof
  := (φ.1 /\ φ'.1; cof_and _ _ φ.2 φ'.2).

Definition or (φ φ' : Cof) : Cof
  := (φ.1 \/ φ'.1; cof_or _ _ φ.2 φ'.2).

Definition is0 (i : I) : Cof
  := (i = 0; cof0 i).

Definition is1 (i : I) : Cof
  := (i = 1; cof1 i).

Definition cof_ise (e : OI) (i : I) : cof (i = e)
  := match e with O' => cof0 i | I' => cof1 i end.

Definition is_e (e : OI) (i : I) : Cof
  := (i = e; cof_ise e i).

Definition cof_Cof (φ : Cof) : cof φ
  := φ.2.

Definition cof_Cof' (φ : Cof) : cof φ.1
  := φ.2.

Hint Resolve cof0 cof1 cof_or cof_and cof_forall cof_ise cof_Cof cof_Cof'
  : cof.
Tactic Notation "cof" :=
  try match goal with
      | |- cof _ => auto with cof
      end.


Definition eq_Cof (φ φ' : Cof) (e : φ.1 = φ'.1)
  : φ = φ'.
  eapply (eq_sigma' _ e). apply proof_irr.
Defined.


Definition partial A := { φ : Cof & El φ -> A }.

Definition empty {A} : partial A.
  refine (false; False_rect _ ).
Defined.

Definition extends {A} (u : partial A) (a : A)
  := forall x, u.2 x = a.

Definition Ext A
  := forall u : partial A, exists a, extends u a.


Axiom join : forall {A φ φ'} {Hφ : cof φ} {Hφ' : cof φ'}
               (f : φ -> A) (g : φ' -> A) (H : forall u v, f u = g v),
    φ \/ φ' -> A.
Axiom join_l : forall {A φ φ' Hφ Hφ'} f g H u,
    @join A φ φ' Hφ Hφ' f g H (or_introl u) = f u.
Axiom join_r : forall {A φ φ' Hφ Hφ'} f g H v,
    @join A φ φ' Hφ Hφ' f g H (or_intror v) = g v.


(* Goal forall P, P \/ ~P -> {P} + {~P}. *)
(*   intro P. unshelve eapply or_elim. *)
(*   intro; now left. *)
(*   intro; now right. *)
(*   intros x y; contradiction (y x). *)
(* Defined. *)

Axiom Cof_dec : forall φ : Cof, {El φ} + {~ (El φ)}.

Definition join' {A φ φ'} {Hφ : cof φ} {Hφ' : cof φ'}
           (f : φ -> A) (g : φ' -> A) (H : forall u v, f u = g v)
  : φ \/ φ' -> A.
Proof.
  intro w.
  induction (Cof_dec (φ; Hφ)).
  exact (f a).
  induction (Cof_dec (φ'; Hφ')).
  exact (g a).
  refine (False_rect _ _).
  induction w; auto.
Defined.

Definition join_l' {A φ φ' Hφ Hφ'} f g H u :
    @join' A φ φ' Hφ Hφ' f g H (or_introl u) = f u.
Proof.
  unfold join'; cbn.
  induction (Cof_dec (φ; Hφ)); cbn.
  apply Eap, proof_irr.
  induction (Cof_dec (φ'; Hφ')); cbn.
  symmetry; apply H.
  contradiction (b u).
Defined.

Definition join_r' {A φ φ' Hφ Hφ'} f g H v
  : @join' A φ φ' Hφ Hφ' f g H (or_intror v) = g v.
Proof.
  unfold join'; cbn.
  induction (Cof_dec (φ; Hφ)); cbn.
  apply H.
  induction (Cof_dec (φ'; Hφ')); cbn.
  apply Eap, proof_irr.
  contradiction (b0 v).
Defined.


Axiom strictness : forall {φ : Cof}(A : El φ -> Type)(B : Type)
                     (s : forall u, SEquiv (A u) B),
    exists (B' : Type) (s' : SEquiv B' B) (e : forall u, A u = B'),
      forall u, Etransport (fun X => SEquiv X B) (e u) (s u) = s'.

Definition strictness' : forall {φ : Cof}(A : El φ -> Type)(B : Type)
                     (s : forall u, SEquiv (A u) B),
    exists (B' : Type) (s' : SEquiv B' B) (e : forall u, A u = B'),
      forall u, Etransport (fun X => SEquiv X B) (e u) (s u) = s'.
  intros φ A B s.
  pose proof (Cof_dec φ).
  simple refine (_; _; _; _).
  - induction H.
    + exact (A a).
    + exact B.
  - unshelve (econstructor; econstructor); cbn.
    + induction H; cbn. exact (s a). exact idmap.
    + induction H; cbn. exact (sequiv_g _ (sequiv_is_sequiv _ _ (s a))).
      exact idmap.
    + induction H; cbn. refine (sequiv_gf _ _). reflexivity.
    + induction H; cbn. refine (sequiv_fg _ _). reflexivity.
  - induction H; cbn. intro u; exact (Eap A (proof_irr _ _)).
    intro u; contradiction (b u).
  - cbn. induction H; cbn.
    2: intro u; contradiction (b u).
    intro u. destruct (proof_irr u a). cbn.
    set (s0 := s u). destruct s0 as [s1 []]. reflexivity.
Defined.



Definition Ext_Type : Ext Type.
  intros [φ A]. unfold extends; cbn.
  simple refine ((strictness A _ _).1; _).
  - exact (forall u, A u).
  - intro u. unshelve (econstructor; econstructor). 
    + intros x u'. refine (proof_irr _ _ E# x).
    + intro f; exact (f u).
    + intro x; cbn. now rewrite (uip (proof_irr u u) E1).
    + intro f; cbn. funext u'. now destruct (proof_irr u u').
  - intro u; cbn. exact ((strictness _ _ _).2.2.1 u).
Defined.

Definition Ext_Type' : Ext Type.
  intros [φ A]. unfold extends; cbn.
  simple refine ((strictness A _ _).1; _).
  - exact (exists u, A u).
  - intro u. unshelve (econstructor; econstructor). 
    + intro x. exact (u; x).
    + intros [u' x]; exact (proof_irr _ _ E# x).
    + intro x; cbn. now rewrite (uip (proof_irr u u) E1).
    + intros [u' x]; cbn. eapply (eq_sigma _ (proof_irr u u')).
      assert (proof_irr u u' = (proof_irr u' u)^E) by apply uip.
      rewrite H. apply Etransport_Vp.
  - intro u; cbn. exact ((strictness _ _ _).2.2.1 u).
Defined.
