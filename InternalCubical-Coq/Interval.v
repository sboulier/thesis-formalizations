
Axioms (I : Type)
       (zero one : I)
       (zero_one : ~ zero = one)
       (inf sup : I -> I -> I)
       (connected : forall (P : I -> Prop), (forall (i : I), P i \/ ~(P i))
                                   -> P zero = P one).

Notation "0" := zero.
Notation "1" := one.

Axioms (zero_inf : forall {i}, inf 0 i = 0)
       (one_inf : forall {i}, inf 1 i = i)
       (inf_zero : forall {i}, inf i 0 = 0)
       (inf_one : forall {i}, inf i 1 = i)
       (zero_sup : forall {i}, sup 0 i = i)
       (one_sup : forall {i}, sup 1 i = 1)
       (sup_zero : forall {i}, sup i 0 = i)
       (sup_one : forall {i}, sup i 1 = 1).


Hint Resolve zero_one zero_inf one_inf inf_zero inf_one
     zero_sup one_sup sup_zero sup_one.

Inductive OI := O' | I'.

Definition neg (e : OI) : OI :=
  match e with
  | O' => I'
  | I' => O'
  end.

Notation " ! e " := (neg e) (at level 3, format "! e").

Definition neg_neg e : ! (! e) = e.
Proof.
  destruct e; reflexivity.
Defined.

Definition el (e : OI) : I :=
  match e with
  | O' => zero
  | I' => one
  end.

Coercion el : OI >-> I.

Definition not_neg e : ~ (! e) = e :> I.
Proof.
  destruct e; auto.
Defined.

Definition cnx (e : OI) : I -> I -> I :=
  match e with
  | O' => inf
  | I' => sup
  end.


Definition e_cnx e i : cnx e e i = e.
Proof.
  now destruct e.
Defined.

Definition cnx_e e i : cnx e i e = e.
Proof.
  now destruct e.
Defined.

Definition cnx_ne e i : cnx !e e i = i.
Proof.
  now destruct e.
Defined.

Definition ne_cnx e i : cnx !e i e = i.
Proof.
  now destruct e.
Defined.

Definition cnx_ne' e i : cnx e i !e = i.
Proof.
  now destruct e.
Defined.

Definition ne_cnx' e i : cnx e !e i = i.
Proof.
  now destruct e.
Defined.

Hint Resolve cnx_e cnx_ne e_cnx ne_cnx cnx_ne' ne_cnx' .

Global Hint Rewrite @zero_inf @one_inf @inf_zero @inf_one @zero_sup @one_sup
       @sup_zero @sup_one cnx_e cnx_ne e_cnx ne_cnx cnx_ne' ne_cnx' : cnx.
