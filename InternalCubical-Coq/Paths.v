Require Export Fibrations.

Definition pathsD (A : I -> Type) (a : A 0) (b : A 1)
  := { p : forall i, A i & p 0 = a × p 1 = b }.

Definition paths A := pathsD (fun _ => A).

Definition pr1_paths A a b : paths A a b -> (I -> A) := π1 _.
Coercion pr1_paths : paths >-> Funclass.

Notation " a ~ b :> A " := (paths A a b) (at level 70).
Notation " a ~ b " := (paths _ a b) (at level 70).

Definition idpath {A} (a : A) : a ~ a
  := (fun i => a ; (eq_refl, eq_refl)).

Arguments idpath [A] a, [A a].


Definition eq_paths {A a b} (p q : paths A a b)
  : (forall i, p i = q i) -> p = q.
Proof.
  intro H. unshelve eapply eq_sigma'.
  apply funext, H.
  unshelve eapply eq_sigma'; apply uip.
Defined.

Tactic Notation "path" := simple refine (fun i => _; _; _).
Tactic Notation "path" ident(i) := simple refine (fun i => _; _; _).


Definition fun_paths {X} (p : I -> X) : p 0 ~ p 1.
  now econstructor.
Defined.


Definition transport {A} (P : A -> Type) {H : UFib P} {x y} (p : paths A x y)
           (u : P x) : P y.
Proof.
  refine (p.2.2 E# _). unshelve refine (H O' p.1 false _ _).1.
  intro. exact (False_rect _).
  unshelve econstructor. refine (p.2.1^E E# _). exact u.
  intro w; contradiction w.
Defined.

Notation "p # x" := (transport _ p x)
 (right associativity, at level 65, only parsing) : path_scope.


Definition transport_idpath {A} (P : A -> Type) {H : UFib P} x u
  : u ~ transport P (idpath x) u.
Proof.
  simple refine ((ufill P H O' (idpath x) false (fun _ => False_rect _)
                        (u; _)).1; _; _).
  - intro w; contradiction w.
  - rewrite ufill_begin; reflexivity.
  - rewrite ufill_end; reflexivity.
Defined.


Class Contr A := contr : { x : A & forall y, x ~ y }.

Arguments contr {A} Contr.

Instance Contr_singleton {A} {a : A} : Contr {a' : A & a ~ a'}.
Proof.
  exists (a; idpath a).
  intros [b [p [[] []]]].
  path i.
  - refine (p i; fun j => p (inf i j); _).
    split. apply Eap, inf_zero. apply f_equal, inf_one.
  - unshelve eapply eq_sigma. reflexivity.
    apply eq_paths; cbn. intro; apply f_equal, zero_inf.
  - unshelve eapply eq_sigma. reflexivity.
    apply eq_paths; cbn. intro; apply f_equal, one_inf.
Defined.

(* Definition Contr_singleton' {A} {a : A} : Contr {a' : A & a' ~ a}. *)
(* Proof. *)
(*   unshelve econstructor. exists a. apply idpath. *)
(*   intros [b [p [[] []]]]. *)
(*   path i. *)
(*   - refine (p (not i); fun j => p (sup (not i) j); _). *)
(*     split. apply f_equal, sup_zero. apply f_equal, sup_one. *)
(*   - unshelve eapply eq_sigma. exact (Eap p not_zero). *)
(*     apply eq_paths; cbn. intro; cbn. *)
(*     destruct (Eap p not_zero). cbn. now rewrite not_zero, one_sup. *)
(*   - unshelve eapply eq_sigma. exact (Eap p not_one). *)
(*     apply eq_paths; cbn. intro; cbn. *)
(*     destruct (Eap p not_one). cbn. now rewrite not_one, zero_sup. *)
(* Defined. *)



Definition Contr_partial A : Contr (partial A).
Proof.
  exists empty. intros [φ f]. path i.
  - unshelve econstructor. exact (and φ (is1 i)).
    exact (fun w => f (proj1 w)).
  - unshelve eapply eq_sigma; cbn.
    eapply eq_Cof; cbn. eapply eq_False.
    intro; apply zero_one. apply H.
    apply funext. intros [].
  - unshelve eapply eq_sigma; cbn.
    eapply eq_Cof; cbn. eapply propext.
    firstorder. exact (fun x => proj1 x).
    apply funext. intro x.
    rewrite Etransport_arrow_toconst. eapply Eap.
    (* rewrite (Etransport_idmap_Eap _ El). rewrite <- Etransport_pp. *)
    eapply proof_irr.
Defined.



Definition Ext_Contr A : Ext A -> Contr A.
Proof.
  intro H. pose (x := (H empty).1). exists x.
  intro y. path i.
  - refine (H (or (is0 i) (is1 i); _)).1.
    cbn. unshelve eapply join; cof; intros.
    exact x. exact y.
    contradiction (zero_one (u^E E@ v)).
  - cbn; match goal with
         | |- (H ?X).1 = _ => unshelve refine (((H X).2 _)^E E@ _); cbn
         end.
    left; reflexivity.
    apply join_l.
  - cbn; match goal with
         | |- (H ?X).1 = _ => unshelve refine (((H X).2 _)^E E@ _); cbn
         end.
    right; reflexivity.
    apply join_r.
Defined.

Definition Contr_Ext A (FibA : Fib A) : Contr A -> Ext A.
Proof.
  intro p. intros [φ u].
  unshelve refine (let XX := FibA O' (fun _ => tt) φ (fun i w => p.2 (u w) i)
                                  (p.1; _) in _).
  exact (fun x => (p.2 (u x)).2.1). cbn in XX.
  exists XX.1.
  intro w. refine (_ E@ XX.2 w).
  cbn. exact (p.2 (u w)).2.2^E.
Defined.

(* seems to be false *)
Definition SEquiv_ContrExt A : SEquiv (Ext A) (Fib A × Contr A).
Proof.
  unshelve econstructor.
  intro H; split. now apply Ext_Fib.
  now apply Ext_Contr.
  unshelve econstructor.
  intros [H1 H2]. now apply Contr_Ext.
  - intro H. apply funext; intro u.
    unshelve eapply eq_sigma. 2: apply proof_irr.
    cbn. unfold Ext_Fib; cbn.
    unshelve eapply (Eap (fun u : partial A => (H u).1) _).
    eapply (eq_sigma _ E1). cbn.
    apply funext; intro w.
    match goal with
    | |- (H ?X).1 = _ => unshelve refine (((H X).2 _)^E E@ _); cbn
    end. right; reflexivity.
    apply @join_r.
  - intros [H1 H2]; cbn.
    unshelve eapply eq_sigma.
    2: rewrite Etransport_const.
    + apply funext; intro e.
      apply funext; intro a.
      apply funext; intro φ.
      apply funext; intro p.
      apply funext; intro p0.
      unshelve eapply eq_sigma.
      2: eapply proof_irr. cbn.
Abort.

Goal forall A, Ext A <-> Fib A × Contr A.
Proof.
  intro A; split.
  - intro H; split. now apply Ext_Fib.
    now apply Ext_Contr.
  - intros [H1 H2]. now apply Contr_Ext.
Defined.



Instance UFib_paths {A} (B : A -> Type) (HB : UFib B)
  : UFib (fun x : exists x : A, B x × B x => paths (B x.1) (fst x.2) (snd x.2)).
Admitted.

Instance UFib_paths' {A} (B : A -> Type) (HB : UFib B)
           (t u : forall x, B x)
  : UFib (fun x => paths (B x) (t x) (u x))
  := UFib_precomp _ (UFib_paths B HB) (fun x => (x; (t x, u x))).

Instance UFib_paths_const {A B} (HB : Fib B)
           (t u : A -> B)
  : UFib (fun x => paths B (t x) (u x))
  := UFib_paths' (fun _ => B) _ t u.


Definition paths_ind' {A} (a : A)
           (P : sigT (paths A a) -> Type) {FibP: UFib P}
           (f : P (a; idpath)) {y : A} (p : a ~ y) : P (y; p).
Proof.
  eapply transport; try eassumption.
  apply Contr_singleton.2.
Defined.

Definition paths_ind {A} (a : A)
           (P : forall a0, a ~ a0 -> Type) {FibP: UFib2 P}
           (f : P a idpath) {y} (p : a ~ y) : P y p
  := paths_ind' a (fun w => P w.1 w.2) f p.

Definition paths_rec {A} a (P : A -> Type) {FibP: UFib P}
           (f : P a) {y} (p : a ~ y)
  : P y := paths_ind a (fun x _ => P x) f p.

Definition inverse {A} {FibA: Fib A} {x y : A} (p : x ~ y) : y ~ x
  := transport (fun y => y ~ x) p idpath.

Definition paths_rec' A {FibA : Fib A} a y P {FibP: UFib P} (X : P y)
           (H : @paths A a y) : P a
  := paths_rec y P X (inverse H).

Notation "f ~~ g" := (forall x, f x ~ g x) (at level 70, no associativity) : type_scope.



Definition Eq_to_paths {A : Type} {x y : A} (p : x = y) : x ~ y :=
  match p with
    | eq_refl => idpath
  end.


Definition concat {A} {FibA : Fib A} {x y z : A} (p : x ~ y) (q : y ~ z) : x ~ z
  := transport (fun z => x ~ z) q (transport (fun y => x ~ y) p idpath).
Arguments concat {A FibA x y z} !p !q.

Delimit Scope path_scope with path.
Open Scope path_scope.

Notation "p @ q" := (concat p%path q%path) (at level 20) : path_scope.
Notation "p ^" := (inverse p%path) (at level 3, format "p '^'") : path_scope.
Notation " 'P1' " := idpath : path_scope.



(* ****** destruct_path tactic ****** *)

(* auxiliary tactics *)
Definition myid : forall A, A -> A := fun _ x => x.
Ltac mark H := let t := type of H in change (myid _ t) in H.
Ltac unmark H := let t := type of H in
                 match t with
                 | myid _ ?tt => change tt in H
                 end.
Hint Unfold myid : typeclass_instances.

(* If p : x = y  then destruct_path revert all hypothesis depending on x and y.  *)
(* Then, it applies paths_ind and then it reintroduce reverted hypothesis. *)
Ltac destruct_path p :=
  let t := type of p in
  match t with
    @paths _ ?x ?y =>
    mark p;
      repeat match goal with
             | [X: context[y] |- _] =>
               match type of X with
               | myid _ _ => fail 1
               | _ => revert X;
                   match goal with
                   | |- forall (X: ?T), ?G => change (forall (X: myid _ T), G)
                   end
               end
             end;
      unmark p;
      generalize y p; clear p y;
      match goal with
      | |- forall y p, @?P y p => let y := fresh y in
                                  let p := fresh p in
                                  intros y p; refine (paths_ind x P _ (y:=y) p)
      end;
      repeat match goal with
             | |- forall (H: myid _ _), _ => let H := fresh H in
                                             intro H; unfold myid in H
             end
  end.




Definition ap {A B} (f: A -> B) {x y: A} (p: x ~ y)
  : f x ~ f y.
Proof.
  path i. exact (f (p i)).
  exact (Eap f p.2.1). 
  exact (Eap f p.2.2). 
Defined.
  (* := transport (fun y => f x ~ f y) p idpath. *)
Arguments ap {A B}%type_scope f {x y} p%path_scope.

Definition apD {A} x {B: A -> Type} {FibB: UFib B}
           (f: forall a: A, B a) {y: A} (p: x ~ y)
  : p # (f x) ~ f y.
Proof.
  refine (paths_ind x (fun y p => transport B p (f x) ~ f y) _^ p).
  apply transport_idpath.
Defined.
Arguments apD {A%type_scope x B FibB} f {y} p%path_scope : simpl nomatch.

Definition apD10 {A} {B : A -> Type} {f g : forall x, B x} (p : f ~ g) x
           : f x ~ g x.
Proof.
  path i. exact (p i x).
  exact (EapD10 p.2.1 x). 
  exact (EapD10 p.2.2 x). 
Defined.

Definition ap10 {A B} {f g : A -> B} (p : f ~ g) x : f x ~ g x
  := apD10 p x.


Definition ap_pp {A B} {FibA: Fib A}  {FibB: Fib B}
           (f : A -> B) {x y z : A} (p : x ~ y) (q : y ~ z) :
  ap f (p @ q) ~ (ap f p) @ (ap f q).
Proof.
  destruct_path p.
  destruct_path q.
(*   exact P1. *)
(* Defined. *)
Admitted.

Definition ap_V {A B} {FibA: Fib A} {FibB: Fib B} (f : A -> B) {x y : A} (p : x ~ y) :
  ap f (p^) ~ (ap f p)^.
Proof.
(*   destruct_path p. unfold ap, inverse. exact P1. *)
(* Defined. *)
Admitted.

Definition inv_pp {A} {FibA: Fib A} {x y z : A} (p : x ~ y) (q : y ~ z) :
  (p @ q)^ ~ q^ @ p^.
Proof.
(*   destruct_path p. *)
(*   destruct_path q. *)
(*   exact P1. *)
(* Defined. *)
Admitted.

Definition inv_V {A} {FibA: Fib A} {x y : A} (p : x ~ y) :
  p^^ ~ p.
Proof.
  destruct_path p. unfold inverse.
  refine (_ @ _). eapply ap10, ap.
  eapply inverse. refine (transport_idpath _ _ _).
  eapply inverse. refine (transport_idpath _ _ _).
Defined.

(* Definition ap_compose {A B C} {FibA: Fib A} {FibB: Fib B} {FibC: Fib C} (f : A -> B) (g : B -> C) {x y : A} (p : x ~ y) : *)
(*   ap (g o f) p ~ ap g (ap f p). *)
(* Proof. *)
(*   destruct_path p; exact P1. *)
(* Defined. *)

(* Definition concat_p_pp {A} {FibA: Fib A} {x y z t : A} (p : x ~ y) (q : y ~ z) (r : z ~ t) : *)
(*   p @ (q @ r) ~ (p @ q) @ r. *)
(* Proof. *)
(*   destruct_path r. *)
(*   destruct_path q. *)
(*   destruct_path p. *)
(*   exact P1. *)
(* Defined. *)

(* Definition concat_pp_p {A} {FibA: Fib A} {x y z t : A} (p : x ~ y) (q : y ~ z) (r : z ~ t) : *)
(*   (p @ q) @ r ~ p @ (q @ r). *)
(* Proof. *)
(*   destruct_path r. *)
(*   destruct_path q. *)
(*   destruct_path p. *)
(*   exact P1. *)
(* Defined. *)

Definition concat_11 {A} {FibA: Fib A} {x : A} : (idpath x) @ P1 ~ P1.
Proof.
  unfold concat. refine (_ @ _). 
  eapply inverse. refine (transport_idpath _ _ _).
  eapply inverse. refine (transport_idpath _ _ _).
Defined.

Definition concat_1p {A} {FibA: Fib A} {x y: A} (p: x ~ y)
  : P1 @ p ~ p.
Proof.
  destruct_path p. eapply concat_11.
Defined.

Definition concat_p1 {A} {FibA: Fib A} {x y: A} (p: x ~ y)
  : p @ P1 ~ p.
Proof.
  destruct_path p. eapply concat_11.
Defined.

(* Definition concat_pV {A} {FibA: Fib A} {x y : A} (p : x ~ y) : *)
(*   p @ p^ ~ P1 *)
(*   := paths_ind x (fun y p => p @ p^ ~ P1) P1 _ _. *)

(* Definition concat_Vp {A} {FibA: Fib A} {x y : A} (p : x ~ y) : *)
(*   p^ @ p ~ P1 *)
(*   := paths_ind x (fun y p => p^ @ p ~ P1) P1 _ _. *)

(* Definition moveR_Vp {A} {FibA: Fib A} {x y z : A} (p : x ~ z) (q : y ~ z) (r : x ~ y) : *)
(*   p ~ r @ q -> r^ @ p ~ q. *)
(* Proof. *)
(*   destruct_path r. *)
(*   intro h. exact (concat_1p _ @ h @ concat_1p _). *)
(* Defined. *)

(* Definition moveL_Vp {A} {FibA: Fib A} {x y z : A} (p : x ~ z) (q : y ~ z) (r : x ~ y) : *)
(*   r @ q ~ p -> q ~ r^ @ p. *)
(* Proof. *)
(*   destruct_path r. *)
(*   intro h. exact ((concat_1p _)^ @ h @ (concat_1p _)^). *)
(* Defined. *)

(* Definition moveR_M1 {A} {FibA: Fib A} {x y : A} (p q : x ~ y) : *)
(*   P1 ~ p^ @ q -> p ~ q. *)
(* Proof. *)
(*   destruct_path p. *)
(*   intro h. exact (h @ (concat_1p _)). *)
(* Defined. *)

(* Definition concat_pA1 {A} {FibA: Fib A} {f : A -> A} (p : forall x, x ~ f x) {x y : A} (q : x ~ y) : *)
(*   (p x) @ (ap f q) ~  q @ (p y) *)
(*     := paths_ind x (fun y q => (p x) @ (ap f q) ~ q @ (p y)) *)
(*                (concat_p1 _ @ (concat_1p _)^) y q. *)


(* Definition path_sigma {A: Type} *)
(*            (P: A -> Type) {FibP: UFib P} *)
(*            {x x': A} {y: P x} {y': P x'} *)
(*            (p : x ~ x') (q : p # y ~ y') *)
(*   : (x; y) ~ (x'; y'). *)
(* Proof. *)
(*   path i. *)
(*   -  exists (p i). unfold paths in q. *)
(*      refine ((ufill P FibP O' p bot (fun i : I => False_rect (P (p.1 i))) _).1 i). *)
(*      refine (Etransport P ((p.2).1)^E y; _). *)
(*      intros []. *)
(*   - cbn. unshelve eapply eq_sigma'; cbn. exact p.2.1. *)
(*     rewrite ufill_begin; cbn. eapply Etransport_pV. *)
(*   - cbn. unshelve eapply eq_sigma'; cbn. exact p.2.2. *)
(*     rewrite ufill_end; cbn. *)
(*     match goal with *)
(*     | |- Etransport _ _ ?XX.1 = _ => pose XX.2 *)
(*     end. *)
(*     cbn in e. specialize (e *)
(*   - *)

(*     cbn in e. rewrite e. *)
(*     cbn. *)


(* pose (pr1_paths _ _ _ q 1). *)
(*   all: cbn. *)
(*   destruct_path p. *)
(*   destruct_path q. *)
(*   exact P1. *)
(* Defined. *)

(* Definition path_sigma {A: Type} *)
(*            (P: A -> Type) {FibP: UFib P} *)
(*            {x x': A} {y: P x} {y': P x'} *)
(*            (p: x ~ x') (q: p # y ~ y') *)
(*   : (x; y) ~ (x'; y'). *)
(* Proof. *)
(*   path i. exists (p i). pose (pr1_paths _ _ _ q 1). *)
(*   all: cbn. *)
(*   destruct_path p. *)
(*   destruct_path q. *)
(*   exact P1. *)
(* Defined. *)


Definition path_contr {A} {FibA : Fib A} (HA : Contr A) (x y : A)
  : x ~ y.
Proof.
  exact (((contr HA).2 _)^ @ (contr HA).2 _).
Defined.



Definition transport_compose {A B}
           {x y: A} (P: B -> Type) {FibP : UFib P}
           (f : A -> B) (p : x ~ y) (z : P (f x))
  : transport (P o f) p z = transport P (ap f p) z.
Proof.
  unfold transport, UFib_precomp; cbn.
  rewrite Etransport_compose. apply Eap, Eap, Eap.
  unshelve eapply eq_sigma'; cbn. now rewrite Etransport_compose, Eap_V.
  funext w. contradiction w.
Defined.


Definition transport_const {A B} {FibA: Fib A} {FibB: Fib B}
           {x1 x2 : A} (p : x1 ~ x2) (y : B)
  : transport (fun _ => B) p y ~ y.
Proof.
  refine (paths_ind x1 (fun x2 p => p # y ~ y) _ p).
  eapply inverse. refine (transport_idpath _ _ _).
Defined.

Definition transport_paths_r A {FibA: Fib A} {x y1 y2: A}
           (* {FibA' : UFib (fun y  => x ~ y)} *)
           (p : y1 ~ y2) (q : x ~ y1)
  : transport (fun y : A => x ~ y) p q ~ q @ p.
Proof.
  unfold concat. eapply ap.
  destruct_path q. eapply transport_idpath.
Defined.

(* Definition transport_paths_Fl {A B} {FibA: Fib A} {FibB: Fib B} *)
(*            (f: A → B) {x1 x2: A} {y: B} (p: x1 ~ x2) (q: f x1 ~ y) *)
(*   : transport (λ x : A, f x ~ y) p q ~ (ap f p)^ @ q. *)
(* Proof. *)
(*   destruct_path q. *)
(*   destruct_path p. *)
(*   exact P1. *)
(* Defined. *)

(* Definition transport_paths_Fr {A B} {FibA: Fib A} {FibB: Fib B} *)
(*            (f: A → B) {x1 x2: A} {y: B} (p: x1 ~ x2) (q: y ~ f x1) *)
(*   : transport (λ x : A, y ~ f x) p q ~ q @ ap f p. *)
(* Proof. *)
(*   destruct_path p. cbn. *)
(*   exact (concat_p1 _)^. *)
(* Defined. *)



Definition concat_Ep {A} {FibA: Fib A} {x y z : A} (e: x = y) (p: y ~ z) : x ~ z
  := Etransport (fun u => u ~ z) e^E p.

Definition concat_EVp {A} {FibA: Fib A} {x y z : A} (e: y = x) (p: y ~ z) : x ~ z
  := Etransport (fun u => u ~ z) e p.

Definition concat_pE {A} {FibA: Fib A} {x y z : A} (p: x ~ y) (e: y = z) : x ~ z
  := Etransport (fun v => x ~ v) e p.

Definition concat_Ep_ETP {A} {FibA: Fib A} {x y z: A} (e: x = y :> A) (p: y = z)
  : concat_Ep e (Eq_to_paths p) = Eq_to_paths (e E@ p).
Proof.
  now destruct e, p.
Defined.

Definition concat_EVp_ETP {A} {FibA: Fib A} {x y z: A} (e: y = x :> A) (p: y = z)
  : concat_EVp e (Eq_to_paths p) = Eq_to_paths (e^E E@ p).
Proof.
  now destruct e, p.
Defined.

Definition concat_pE_ETP {A} {FibA: Fib A} {x y z: A} (p: x = y) (e: y = z)
  : concat_pE (Eq_to_paths p) e = Eq_to_paths (p E@ e).
Proof.
  now destruct e, p.
Defined.

Definition ap_concat_Ep {A B} {FibA: Fib A}  {FibB: Fib B} (f: A -> B)
           {x y z: A} (e: x = y :> A) (p: y ~ z)
  : ap f (concat_Ep e p) = concat_Ep (Eap f e) (ap f p).
Proof.
    now destruct e.
Defined.

Definition ap_concat_EVp {A B} {FibA: Fib A} {FibB: Fib B} (f: A -> B)
           {x y z: A} (e: y = x) (p: y ~ z)
  : ap f (concat_EVp e p) = concat_EVp (Eap f e) (ap f p).
Proof.
    now destruct e.
Defined.

Definition ap_concat_pE {A B} {FibA: Fib A} {FibB: Fib B} (f: A -> B)
           {x y z: A} (p: x ~ y) (e: y = z)
  : ap f (concat_pE p e) = concat_pE (ap f p) (Eap f e).
Proof.
    now destruct e.
Defined.


Definition Etransport_paths_FlFrE {A B} {FibB: Fib B} {f g: A -> B} {x1 x2: A} (p: x1 = x2) (q: f x1 = g x1)
  : Etransport (fun x => f x ~ g x) p (Eq_to_paths q) = Eq_to_paths ((Eap f p^E) E@ q E@ (Eap g p)).
Proof.
  destruct p; simpl. apply Eap. now destruct q.
Defined.

(* Axiom ap_compose_strict : *)
(*   ∀ {A B C} {FibA: Fib A} {FibB: Fib B} {FibC: Fib C} *)
(*     (f: A → B) (g: B → C) {x y: A} (p: x ~ y), *)
(*     ap (g o f) p = ap g (ap f p). *)


Definition transport_paths_rV {A} {FibA: Fib A}
           {x y : A} (p : x ~ y)
  : transport (fun z => y ~ z) p p^ ~ P1.
Proof.
  destruct_path p. 
  unfold inverse.
  refine (_ @ _)^.
  2: apply transport_idpath.
  exact (transport_idpath _ _ _).
Defined.

Definition transport_paths_rp {A} {FibA: Fib A}
           {x y : A} (p : x ~ y)
  : transport (fun z => z ~ y) p p ~ P1.
Proof.
  destruct_path p. 
  exact (transport_idpath _ _ _)^.
Defined.
