Require Import Id.


Record IsEquiv {A B} (f : A -> B) :=
  { equiv_inv : B -> A ;
    eisretr : f o equiv_inv I~~ idmap;
    eissect : equiv_inv o f I~~ idmap;
    eisadj :  forall x : A, eisretr (f x) I~ Iap f (eissect x) }.

Arguments equiv_inv {_ _ _} _ _.
Arguments eisretr {_ _ _} _ _.
Arguments eissect {_ _ _} _ _.
Arguments eisadj {_ _ _} _ _.

(* Definition isequiv_adjointify {A B} {FibA: Fibrant A} {FibB: Fibrant B} {f: A -> B} *)
(*            (g: B -> A) : f o g == idmap -> g o f == idmap -> IsEquiv f. *)
(* Proof. *)
(*   intros isretr issect. use Build_IsEquiv. *)
(*   exact g. exact isretr. *)
(*   exact (λ x, ap g (ap f (issect x)^) @ ap g (isretr (f x)) @ issect x). *)
(*   intro a; cbn. *)
(*   apply moveR_M1. *)
(*   repeat (rew ap_pp; rew concat_p_pp); rew (ap_compose _ _ _)^. *)
(*   rew (concat_pA1 (fun b => (isretr b)^) (ap f (issect a)^)). *)
(*   repeat rew concat_pp_p; rew ap_V; apply moveL_Vp; rew concat_p1. *)
(*   rew concat_p_pp; rew (ap_compose _ _ _)^. *)
(*   rew (concat_pA1 (fun b => (isretr b)^) (isretr (f a))). *)
(*   rew concat_pV; rew concat_1p. *)
(*   exact 1. *)
(* Defined. *)
(* (* Qed. *) *)

(* Axiom IsEquiv : forall {A B} (f : A -> B), Type. *)
(* Axiom isequiv_adjointify : forall {A B} {FibA: Fib A} {FibB: Fib B} *)
(*                              {f: A -> B} (g: B -> A), *)
(*   f o g == idmap -> g o f == idmap -> IsEquiv f. *)


Record IsQinv {A B} (f : A -> B) :=
  { qinv : B -> A ;
    qretr : f o qinv ~~ idmap;
    qsect : qinv o f ~~ idmap }.
Arguments Build_IsQinv {A B f} _ _ _.

Axiom isequiv_adjointify : forall {A B} {FibA: Fib A} {FibB: Fib B}
                             {f : A -> B} (g : B -> A),
  f o g I~~ idmap -> g o f I~~ idmap -> IsEquiv f.

Definition isequiv_adjointify' {A B} {FibA: Fib A} {FibB: Fib B}
           {f : A -> B} (g : B -> A)
  : f o g ~~ idmap -> g o f ~~ idmap -> IsEquiv f.
Proof.
  intros p1 p2. eapply isequiv_adjointify.
  all: intro; apply paths_Id; auto.
Defined.
