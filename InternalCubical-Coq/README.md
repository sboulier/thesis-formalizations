# InternalCubical-Coq

Formalization of Interval Type Theory, of Orton and Pitts, and of several constructions in Coq.

This formalization borrows a lot to Orton and Pitts' one:

https://www.repository.cam.ac.uk/handle/1810/274644


## Description of the files

- `Interval.v`  Definition of the interval with sup and inf

- `Cof.v`  Definition of the universe of cofibrant propositions, join and the strictness axiom

- `Fibrations.v`  Definition of extension structures, regular and degenerate fibrancy, and transport structures

- `Paths.v`  Definition of path types and lemmas about them (including a few groupoidal laws)

- `Id.v`  Definition of identity types and lemmas about them (including a few groupoidal laws)

- `FibRepl.v`  Definition of the degenerate fibrant replacement as a strict quotient indutive type

- `Cylinder.v`  Definition of the mapping cyinder HIT. It requires to define the homotopy pushout HIT. It is defined by the fibrant replacement of a strict quotient inductive type

- `Equivalence.v`  Definition of equivalences

- `Category.v`  Definition of categories and pre-model structures

- `Model_structure.v`  We plug all together and show that there is a pre-model structure on the universe of pretypes Type
