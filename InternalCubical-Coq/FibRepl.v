Require Export Id.

Module Export Quotient.

  Private Inductive repl (A : Type) :=
  | η : A -> repl A
  | hcomp : forall (e : OI) (φ : Cof) (a : forall i : I, El φ \/ i = e -> repl A), repl A.
  Axiom qq : forall {A} (e : OI) φ (a : forall i : I, El φ \/ i = e -> repl A),
      extends (φ; fun x => a !e (or_introl x)) (hcomp A e φ a).
  (* forall (x : El φ), a !e (or_inl x) = hcomp e φ a *)

  Arguments η {_} _.
  Arguments hcomp {_} _ _.

  Fixpoint repl_ind {A} (P : repl A -> Type)
             (η' : forall a : A, P (η a))
             (hcomp' : forall (e : OI) φ (a : forall i : I, El φ \/ i = e -> repl A),
                 (forall i x, P (a i x)) -> P (hcomp e φ a))
             (qq' : forall e φ a (a' : forall i x, P (a i x)) x,
                 (qq e φ a x) E# (a' !e (or_introl x)) = hcomp' e φ a a')
             w : P w.
  Proof.
    destruct w.
    apply η'. apply hcomp'.
    intros i x. eapply (repl_ind A P η' hcomp' qq').
  Defined.

End Quotient.

Definition qq_const {A} (e : OI) (φ : Cof) (a : repl A)
  : φ -> hcomp e φ (fun _ _ => a) = a
  := fun f => (@qq A e φ (fun _ _ => a) f)^E.

Definition qq' {A} (e : OI) (φ : Cof) (a : I -> repl A)
           (g : I -> I)
  : φ -> hcomp e φ (fun i _ => a (g i)) = a (g !e)
  := fun f => (@qq A e φ (fun i _ => a (g i)) f)^E.



Definition repl_ind' {A} (P : repl A -> Type) {HP : UFib P}
           (η' : forall a : A, P (η a))
  : forall w, P w.
Proof.
  pose proof (snd (ufib' P) HP) as HP'; clear HP; rename HP' into HP.
  unshelve eapply repl_ind. 2: assumption.
  - intros e φ a p.
    + simple refine (
          let XX := (HP e (fun i => hcomp e (or φ (is_e e i)) _) φ _).1
          in _).
      * cbn; intros j f. apply (a (cnx e i j)).
        refine (or_ind _ _ f).
        refine (or_ind _ _). now left.
        right. etransitivity.
        eapply Eap10, Eap. exact H. auto.
        right. etransitivity.
        eapply Eap. exact H. auto.
      * intros i H; cbn.
        refine (_ E# p i H).
        refine (_ E@ qq e (or φ (is_e e i)) _ H); cbn.
        unshelve eapply EapD011.
        now symmetry. eapply proof_irr.
      * refine (_ E# XX).
        unshelve eapply EapD011.
        -- eapply eq_Cof, propext; cbn.
           firstorder. intuition.
           contradiction (not_neg _ H0).
        -- eapply funext; intro j.
           rewrite Etransport_forall_constant.
           eapply funext; intro f.
           rewrite Etransport_arrow_toconst.
           unshelve eapply EapD011.
           auto. eapply proof_irr.
  - intros e φ a a' f; cbn.
    match goal with
    | |- _ = Etransport _ ?pp (HP ?aa ?bb ?cc ?qq).1
      => set (comp := HP aa bb cc qq); set pp; set qq
    end.
    clearbody comp; cbn in *.
    destruct comp as [comp Hcomp]; cbn in *.
    specialize (Hcomp f). destruct Hcomp.
    cbn. rewrite <- !Etransport_pp.
    eapply Eap10, Eap. eapply uip.
Defined.


Instance Fib_repl A : Fib (repl A).
Proof.
  apply (fst (Fib' _)).
  intros e φ a. exists (hcomp e φ a).
  unfold extends. cbn. exact (qq e φ a).
Defined.

Definition repl_rec {A} (P : Type)
           (η' : A -> P)
           (hcomp' : forall (e : OI) φ (a : forall i : I, El φ \/ i = e -> repl A),
               (forall i : I, El φ \/ i = e -> P) -> P)
           (qq' : forall (e : OI) φ a (a' : forall i : I, El φ \/ i = e -> P) x,
               (a' !e (or_introl x)) = hcomp' e φ a a')
  : repl A -> P.
Proof.
  refine (repl_ind (fun _ => P) η' hcomp' _).
  intros e φ a a' x. refine (_ E@ qq' _ _ _ _ x).
  eapply Etransport_const.
Defined.

Definition repl_rec' {A B} {HB : Fib B} (f : A -> B) : repl A -> B.
Proof.
  pose proof (snd (Fib' B) HB) as HB'; clear HB.
  unshelve eapply (repl_rec B f).
  exact (fun e φ _ a => (HB' e φ a).1).   (* Rk: the induction hyp is not used. *)
  exact (fun e φ _ a => (HB' e φ a).2).
Defined.

Definition repl_rec'_η {A B} (HB : Fib B) (f : A -> B) x
  : repl_rec' f (η x) = f x.
Proof.
  reflexivity.
Defined.


Definition lift {A} (P : A -> Type) : repl A -> Type
  := repl_rec' (HB:=Fib_Type) P.


Definition hcomp_false A : repl A -> repl A.
Proof.
  intro u. eapply (hcomp O' false).
  intro i. unshelve eapply join; cof.
  refine (False_rect _).
  exact (fun _ => u).
  intros [].
Defined.


(* (* Higher induction principles (on paths of repl A) *) *)
(* Axiom repl_ind_up : forall {A} (P : (I -> repl A) -> Type) *)
(*       (η' : forall a : I -> A, P (fun i => η (a i))) *)
(*       (hcomp' : forall φ (a : forall i, El φ \/ i = 0 -> I -> repl A), *)
(*           (forall i x, P (a i x)) -> P (fun i => hcomp φ (fun j x => a j x i))) *)
(*       (qq' : forall φ a (a' : forall i x, P (a i x)) (x : El φ), *)
(*           (funext (fun i => qq φ (fun (j : I) (y : El φ \/ j = 0) => a j y i) x)) *)
(*             E# (a' 1 (or_introl x)) = hcomp' φ a a') *)
(*       w, P w. *)


(* Definition Fib_paths {A} (HA : Fib A) : Fib (I -> A). *)
(* Proof. *)
(*   intros φ _ a a0. *)
(*   unfold UFib, extends in *; cbn in *. *)
(*   simple refine (let XX := fun i => HA φ (fun _ => tt) (fun j x => a j x i) _ in _). *)
(*   - cbn. exists (a0.1 i). intro x. *)
(*     eapply Eap10. apply (a0.2 x). *)
(*   - cbn in *. exists (fun i => (XX i).1). *)
(*     intro x. eapply funext; intro i. *)
(*     apply (XX i).2. *)
(* Defined. *)


(* Definition α A (f : repl (I -> A)) : I -> repl A. *)
(*   intro i. refine (repl_rec' _ _ f). *)
(*   apply Fib_repl. intro f'. *)
(*   exact (η (f' i)). *)
(* Defined. *)

(* (* Another way of defining α *) *)
(* Definition α' A (f : repl (I -> A)) : I -> repl A. *)
(*   eapply repl_rec'. eapply Fib_paths, Fib_repl. *)
(*   2: exact f. exact (fun f => η o f). *)
(* Defined. *)


(* Definition eq_join P Q A g1 g2 g12 g1' g2' g12' *)
(*            (e1 : g1 ≡≡ g1') (e2 : g2 ≡≡ g2') *)
(*   : join P Q A g1 g2 g12 = join P Q A g1' g2' g12'. *)
(*   apply funext in e1; destruct e1. *)
(*   apply funext in e2; destruct e2. *)
(*   assert (e12 : g12 = g12'). { *)
(*     repeat (apply funext; intro). apply uip. } *)
(*   destruct e12; reflexivity. *)
(* Defined. *)

(* Definition α_hcomp {A} φ (a : forall i : I, El φ \/ i = 0 -> repl (I -> A)) *)
(*   (i : I) *)
(*   : α A (hcomp φ a) i = hcomp φ *)
(*     (fun i0 : I => *)
(*      join (El φ) (i0 = 0) (repl A) *)
(*        (fun x => α A (a i0 (or_introl x)) i) *)
(*        (fun _ => α A (a 0 (or_intror E1)) i) *)
(*        (fun x p => Eap (fun X : ∃ i1 : I, El φ \/ i1 = 0 => α A (a X.1 X.2) i) *)
(*                        (eq_sigma _ p (proof_irr (El φ \/ 0 = 0) *)
(*                             (Etransport (fun i1 => El φ \/ i1 = 0) p (or_introl x)) *)
(*                             (or_intror E1))))). *)
(*   cbn. eapply Eap. apply funext; intro j. *)
(*   unshelve eapply eq_join. *)
(*   - reflexivity. *)
(*   - intro. rewrite Etransport_const. reflexivity. *)
(* Defined. *)

(* Definition α'_hcomp {A} φ (a : forall i : I, El φ \/ i = 0 -> repl (I -> A)) *)
(*   (i : I) *)
(*   : α' A (hcomp φ a) i = hcomp φ *)
(*     (fun i0 : I => *)
(*      join (El φ) (i0 = 0) (repl A) *)
(*        (fun x => α' A (a i0 (or_introl x)) i) *)
(*        (fun _ => α' A (a 0 (or_intror E1)) i) *)
(*        (fun x p => Eap (fun X : ∃ i1 : I, El φ \/ i1 = 0 => α' A (a X.1 X.2) i) *)
(*                        (eq_sigma _ p (proof_irr (El φ \/ 0 = 0) *)
(*                             (Etransport (fun i1 => El φ \/ i1 = 0) p (or_introl x)) *)
(*                             (or_intror E1))))). *)
(*   cbn. eapply Eap. apply funext; intro j. *)
(*   unshelve eapply eq_join. *)
(*   - reflexivity. *)
(*   - intro. rewrite Etransport_const. reflexivity. *)
(* Defined. *)




(* (* Are α and α' the same? *) *)
(* Goal forall A f, α A f = α' A f. *)
(*   intros A. simple refine (repl_ind _ _ _ _). *)
(*   1: { cbn; reflexivity. } *)
(*   2: {intros φ a a' x. eapply uip. } *)
(*   cbn beta; intros φ a H. apply funext; intro i. *)
(*   rewrite α_hcomp, α'_hcomp. *)
(*   apply Eap, funext; intro j. *)
(*   apply eq_join. *)
(*   + intro. apply Eap10, H. *)
(*   + intro. apply Eap10, H. *)
(* Defined. *)



(* Definition β A (f : I -> repl A) : repl (I -> A). *)

(*   (* simple refine (@repl_ind_up A (fun _ => _) _ _ _ f). *) *)
(*   (* - cbn. refine η. *) *)
(*   (* - cbn. intros φ a X. eapply (X 0). admit. *) *)
(*   (* - intros φ a a' x. rewrite Etransport_const. *) *)
(*   (*   cbn in *. *) *)

(*   refine (repl_rec' _ (fun x => η (fun _ => x)) (f 0)). *)
(*   apply Fib_repl. *)
(* Defined. *)

(* Definition βα A : β A o α' A ≡≡ idmap. *)
(*   simple refine (repl_ind _ _ _ _). *)
(*   - cbn. *)
(* Abort. *)

(* Definition αβ A : α A o β A ≡≡ idmap. *)
(*   intro. cbn. apply funext; intro i. *)
(*   unfold β, α. set (x 0). remember r. subst r. *)
(*   revert r0 Heqr0. simple refine (repl_ind _ _ _ _). *)
(*   - cbn. *)
(* Abort. *)



(* Definition α_iso_higher_induction A (H : isIso (α A)) *)
(*   : forall (P : (I -> repl A) -> Type) *)
(*       (η' : forall a : I -> A, P (fun i => η (a i))) *)
(*       (hcomp' : forall φ (a : forall i, El φ \/ i = 0 -> I -> repl A), *)
(*           (forall i x, P (a i x)) -> P (fun i => hcomp φ (fun j x => a j x i))) *)
(*       (qq' : forall φ a (a' : forall i x, P (a i x)) (x : El φ), *)
(*           (funext (fun i => qq φ (fun (j : I) (y : El φ \/ j = 0) => a j y i) x)) *)
(*             E# (a' 1 (or_introl x)) = hcomp' φ a a') *)
(*       w, P w. *)
(*   intros P η' hcomp' qq' w. *)
(*   destruct H as [g _ fg]. *)
(*   refine (fg w E# _). *)
(*   simple refine (repl_ind (P o (α A)) _ _ _ (g w)). *)
(*   - exact η'. *)
(*   - intros φ a X. unfold α. *)
(*     pose proof (hcomp' φ (fun i x => α A (a i x)) X). *)
(*     cbn in X0. *)
(*     refine (_ E# X0). *)
(*     apply funext; intro i. clear. *)
(*     eapply (Eap (hcomp φ)). *)
(*     apply funext; intro j. apply funext. *)
(*     simple refine (joinD _ _ _ _ _ _). *)
(*     + intro. cbn beta. rewrite join_l. *)
(*       reflexivity. *)
(*     + intro; cbn beta; rewrite join_r. *)
(*       rewrite Etransport_const. *)
(*       unfold pr1. destruct x. reflexivity. *)
(*     + intros. eapply proof_irr. *)
(*   - intros. cbn. rewrite Etransport_compose. *)
(*     rewrite <- (qq' φ (fun i x => α A (a i x)) a' x). *)
(*     rewrite <- Etransport_pp. *)
(*     eapply Eap10, Eap. apply uip. *)
(* Defined. *)





(* Definition extension_rule {A} (P : A -> Type) (HP : UFib P) *)
(*   : UFib (lift P). *)
(*   intro φ. *)
(*   simple refine (repl_ind_up _ _ _ _). *)
(*   - cbn; intros a p X. exact (HP φ a p X). *)
(*   - cbn beta; intros φ0 a X p X0. *)
(*     pose proof (myforall_elim _ _ X0.1).1. *)

(*     pose (QQ := fun i => qq φ0 (fun (j : I) (x : El φ0 \/ j = 0) => a j x i) H). *)
(*     apply funext in QQ; cbn in QQ. *)

(*     revert p X0. *)
(*     (* set (F := fun i : I => hcomp φ0 (fun (j : I) (x : El φ0 \/ j = 0) => a j x i)) in *. *) *)
(*     (* change (forall p : forall i : I, El φ -> lift P (F i), *) *)
(*     (*            (∃ a0 : lift P (F 0), extends (φ; p 0) a0) *) *)
(*     (*            -> ∃ a1 : lift P (F 1), extends (φ; p 1) a1). *) *)
(*     refine (Etransport (fun F : I -> repl A => forall p : forall i : I, El φ -> lift P (F i), *)
(*                             (∃ a0 : lift P (F 0), extends (φ; p 0) a0) -> *)
(*                             ∃ a1 : lift P (F 1), extends (φ; p 1) a1) QQ _). *)
(*     intros p X0. exact (X 1 (or_introl H) p X0). *)
(*   - intros φ0 a a' x. *)
(*     cbn -[lift]. apply funext; intro p. *)
(*     apply funext; intro p0. *)
(*     match goal with *)
(*     | |- _ = Etransport _ (funext (fun _ => qq _ _ ?XX.1)) _ _ _ *)
(*       => set XX *)
(*     end. *)
(*     set s.1. *)
(*     assert (x = e) by apply proof_irr. destruct H. *)
(*     reflexivity. *)
(* Defined. *)


(* Polymorphic Definition transport_lift {A} (P : A -> Type) (H : UFib P) {x y} *)
(*            (p : paths (repl A) (η x) (η y)) *)
(*            (u : P x) : P y. *)
(*   simple refine (let XX := transport (lift P) _ p in _). *)
(*   now apply extension_rule. *)
(*   unfold lift in XX; rewrite !repl_rec'_η in XX. *)
(*   now apply XX. *)
(* Defined. *)

(* (* Goal forall A x y, Fib A -> paths (repl A) (η x) (η y) -> paths A x y. *) *)
(* (*   intros A x y X H. *) *)
(* (*   refine (transport_lift (fun y => paths A x y) _ _ _). *) *)
(* (*   simple refine (UFib_precomp _ (UFib_paths (fun _:unit => A) _) (A':=A) *) *)
(* (*                               (fun y => (tt; (x, y)))). *) *)
(* (*   assumption. eapply H. eapply refl. *) *)
(* (* Defined. *) *)














Definition repl_f {A B} (f: A -> B) : repl A -> repl B
  := repl_rec' (η o f).

Definition repl_f_η {A B} (f: A -> B) x :
  repl_f f (η x) = η (f x).
Proof.
  reflexivity.
Defined.


Definition repl_rec_compose {A B C} {HC: Fib C} (f: A -> B) (g: B -> C)
  : repl_rec' (g o f) == repl_rec' g o repl_f f.
Proof.
  simple refine (repl_ind _ _ _ _).
  - reflexivity.
  - intros e φ a H. cbn.
    unshelve eapply (@eq_comp _ _ HC).
    + reflexivity.
    + intros i w.  cbn.
      rewrite (H i (or_introl w)).
      now rewrite join_l, join_l.
    + cbn. rewrite (H e (or_intror E1)).
      now rewrite join_r, join_r.
  - intros e φ a a' x; eapply uip.
Defined.

Definition repl_f_compose {A B C} (f: A -> B) (g: B -> C)
  : repl_f g o repl_f f == repl_f (g o f).
Proof.
  intro x. unfold repl_f. symmetry. exact (repl_rec_compose _ _ x).
Defined.

Definition repl_f_idmap {A} : repl_f (fun x: A => x) == idmap.
Proof.
  simple refine (repl_ind _ _ _ _).
  - reflexivity.
  - cbn; intros e φ a H. eapply Eap.
    eapply funext; intro i; cbn.
    eapply funext; intros [f|f]; cbn.
    rewrite !join_l. apply H.
    rewrite !join_r; cbn. rewrite Etransport_const.
    destruct f. apply H.
  - intros e φ a a' x; eapply uip.
Defined.


(** ** This is the extension rule hypothesis *)
Instance extension_rule {A} (P : A -> Type) (HP : UFib P)
  : UFib (lift P).
Proof.
Admitted.

Definition repl_sigma {A} (B : repl A -> Type) {FibB : UFib B}
  : sigT B -> repl (sigT (B o η)).
Proof.
  intros [x y]; revert x y.
  refine (repl_ind' _ _); cbn.
  intros x y. exact (η (x; y)).
Defined.

Definition repl_J {A x} (P : forall (y : A), η x I~ η y -> Type)
           {H: UFib (fun w : exists y, η x I~ η y => P w.1 w.2)}
           {y} (p : η x I~ η y) (u : P x idrefl)
  : P y p.
Proof.
  pose (P' := lift (fun X : exists y, η x I~ η y => P X.1 X.2) o repl_sigma _).
  refine (@Itransport _ P' _ (η x; idrefl (η x)) (η y; p) _ u).
  subst P'; exact _.
  clear. set (η y) in *. clearbody r.
  destruct_Id p. exact I1.
Defined.

Definition repl_J_refl {A} {x} (P : forall (y : A), η x I~ η y -> Type)
           {H : UFib2 P}  (u : P x idrefl)
  : repl_J P (idrefl _) u = u.
Proof.
  unfold repl_J. rewrite J_refl.
  rewrite Itransport_refl. reflexivity.
Defined.
