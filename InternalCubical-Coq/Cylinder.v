Require Export FibRepl.

Module Export PushoutQuotient.
Section Pushout.
  Local Unset Elimination Schemes.

  Context {A B C : Type}.

  Inductive PO (f : A -> B) (g : A -> C) :=
  | pol : B -> PO f g
  | por : C -> PO f g
  | push : A -> I -> PO f g.
  Axiom push_l : forall {f : A -> B} {g : A -> C} x, push f g x 0 = pol f g (f x).
  Axiom push_r : forall {f : A -> B} {g : A -> C} x, push f g x 1 = por f g (g x).

  Context {f : A -> B} {g : A -> C}.
  Arguments pol {_ _} _.
  Arguments por {_ _} _.
  Arguments push {_ _} _.

  Fixpoint PO_ind (P : PO f g -> Type) (pol' : forall b, P (pol b)) (por' : forall c, P (por c))
           (push' : forall x i, P (push x i))
           (push_l' : forall x, push_l x E# push' x 0 = pol' (f x))
           (push_r' : forall x, push_r x E# push' x 1 = por' (g x))
           w : P w :=
    match w with
    | pol b => pol' b
    | por c => por' c
    | push a i => push' a i
    end.

  Definition PO_rec P (pol' : B -> P) (por' : C -> P) (push' : A -> I -> P)
             (push_l' : forall x, push' x 0 = pol' (f x))
             (push_r' : forall x, push' x 1 = por' (g x))
    : PO f g -> P.
  Proof.
    refine (PO_ind (fun _ => P) pol' por' push' _ _).
    all: intro; rewrite Etransport_const; auto.
  Defined.

  Definition push0 x : pol (f:=f) (g:=g) (f x) ~ por (g x).
  Proof.
    path i; cbn. now apply push.
    apply push_l. apply push_r.
  Defined.

  Definition PO1 := repl (PO f g).
  Definition pol1 : B -> PO1 := η o pol.
  Definition por1 : C -> PO1 := η o por.
  Definition push1 x : pol1 (f x) ~ por1 (g x) := ap η (push0 x).


  Definition PO1_rec (P : Type) {HP : Fib P}
             (pol' : B -> P) (por' : C -> P)
             (push' : forall x, pol' (f x) ~ por' (g x))
    : PO1 -> P.
  Proof.
    refine (repl_rec' _).
    unshelve refine (PO_rec _ _ _ _ _ _); try assumption.
    exact (fun x i => push' x i). all: cbn.
    intro x. apply (push' x).2.
    intro x. apply (push' x).2.
  Defined.

  

  Definition PO_ind' (P : PO f g -> Type) {HP : UFib P}
             (pol' : forall b, P (pol b)) (por' : forall c, P (por c))
             (push' : forall x, push0 x # pol' (f x) ~ por' (g x))
    : forall w, P w.
  Proof.
    transparent assert (front : (forall x (j i : I), P (push0 x i))). {
      intros x j. unshelve refine (ufill P HP O' (push0 x) (is1 j) _ (_;_)).1.
      + intros i e.
        pose (p' := fun_paths (fun i' => push0 x (inf i i'))). cbn in *.
        refine (_ E# (transport P p' _)).
        now apply Eap.
        refine (_ E# (pol' (f x))).
        refine ((push_l x)^E E@ _).
        now apply Eap.
      + refine ((push_l x)^E E# _). 
        exact (transport_idpath P _ (pol' (f x)) j).
      + intro e; cbn in *. rewrite e. clear e j.
        rewrite Etransport_const.
        unfold transport.
        rewrite !Eap_V. eapply moveL_Etransport_V.
        simpl. rewrite <- Etransport_pp.
        match goal with
        | |- Etransport P ?AA ?BB = ?XX
          => set AA; set BB; set XX
        end. 
        assert (forall i', push (f:=f) (g:=g) x (inf 0 i') = pol (f x)). {
          intro. refine (Eap (push x) _ E@ push_l x).
          auto. }
        transitivity (H I' E# p).
        apply Eap10, Eap, uip. subst p.
        rewrite (Etransport_comp P O' _ _ _ _ _ H). subst p0.
        simple refine (eq_comp P O' (fun _ => pol (f x)) _ _ _ _ _ _ _ _ _).
        * eapply eq_Cof; cbn. symmetry; eapply eq_False.
          intros []. assumption. exact (zero_one H0^E).
        * intros i [].
        * cbn. rewrite Etransport_const, <- Etransport_pp.
          refine (Eap10 (g := Etransport P E1) _ _).
          apply Eap, uip. }

    transparent assert (diagface : (forall x (k i' : I), P (push x i'))). {
      intros x k.
      unshelve refine (ufill P HP I' (push x) (is0 k) _ (_;_)).1.
      + intros i' _. exact (front x i' i').
      + refine (_ E# push' x k). exact (push_r _)^E.
      + intro e. subst front.
        rewrite e; clear k e. rewrite ufill_end. cbn.
        match goal with
        | |- (HP ?aa ?bb ?cc ?dd ?ee).1 = _
          => unshelve rewrite <- (HP aa bb cc dd ee).2; cbn
        end. reflexivity.
        unfold transport; simpl. eapply moveL_Etransport_V.
        rewrite <- Etransport_pp.
        refine (_ E@ (push' x).2.1^E). unfold transport.
        eapply moveL_Etransport_p. rewrite <- Etransport_pp.
        assert (forall i', push x (inf 1 i') = (push0 x).1 i'). {
          intro; cbn. apply (Eap (push x)). auto. }
        match goal with
        | |- Etransport P ?AA ?BB = ?XX
          => transitivity (Etransport P (H I') BB)
        end. 
        apply Eap10, Eap, uip.
        rewrite (Etransport_comp P O' _ _ _ _ _ H). cbn.
        simple refine (eq_comp P O' (fun i : I => (push x i)) _ _ _ _ _ _ _ _ _).
        * reflexivity.
        * intros i [].
        * cbn. rewrite <- Etransport_pp.
          apply Eap10, Eap, uip. }
    unshelve eapply PO_ind; try eassumption.
    - intros x i.
      exact (diagface x i i).
    - subst diagface; intro x.
      cbn -[ufill]. rewrite ufill_end.
      match goal with
      | |- (Etransport _ _ (HP ?aa ?bb ?cc ?dd ?ee).1) = _
        => unshelve rewrite <- (HP aa bb cc dd ee).2
      end. reflexivity.
      subst front. cbn -[ufill].
      rewrite ufill_begin. cbn.
      rewrite Etransport_const.
      rewrite Etransport_pV.
      match goal with
      | |- (HP ?aa ?bb ?cc ?dd ?ee).1 = _
        => unshelve rewrite <- (HP aa bb cc dd ee).2; cbn
      end. right; reflexivity.
      rewrite join_r. cbn.
      apply Etransport_const.
    - subst diagface; intro x. cbn -[ufill].
      rewrite ufill_begin. simpl. clear.
      rewrite Etransport_pV. exact (push' x).2.2.
  Defined.

  Definition PO1_ind (P : PO1 -> Type) {HP : UFib P}
             (pol' : forall b, P (pol1 b)) (por' : forall c, P (por1 c))
             (push' : forall x, push1 x # pol' (f x) ~ por' (g x))
    : forall w, P w.
  Proof.
    unfold PO1. unshelve eapply repl_ind'. assumption.
    refine (PO_ind' (P o η) pol' por' _).
    intro x. refine (concat_Ep _ (push' x)).
    exact (transport_compose P η (push0 x) (pol' (f x))).
  Defined.

  Definition PO1_ind_pol (P : PO1 -> Type) {HP : UFib P}
             (pol' : forall b, P (pol1 b)) (por' : forall c, P (por1 c))
             (push' : forall x, push1 x # pol' (f x) ~ por' (g x)) b
    : PO1_ind P pol' por' push' (pol1 b) = pol' b.
  Proof.
    reflexivity.
  Defined.

  Definition PO1_ind_por (P : PO1 -> Type) {HP : UFib P}
             (pol' : forall b, P (pol1 b)) (por' : forall c, P (por1 c))
             (push' : forall x, push1 x # pol' (f x) ~ por' (g x)) c
    : PO1_ind P pol' por' push' (por1 c) = por' c.
  Proof.
    reflexivity.
  Defined.

  Definition PO1_rec_pol (P : Type) (HP : Fib P)
             (pol' : B -> P) (por' : C -> P)
             (push' : forall x, pol' (f x) ~ por' (g x)) b
    : PO1_rec P pol' por' push' (pol1 b) = pol' b.
  Proof.
    reflexivity.
  Defined.

  Definition PO1_rec_push (P : Type) (HP : Fib P)
             (pol' : B -> P) (por' : C -> P)
             (push' : forall x, pol' (f x) ~ por' (g x)) x
    : ap (PO1_rec P pol' por' push') (push1 x) = push' x.
  Proof.
    eapply eq_paths. reflexivity.
  Defined.

  Definition PO1_ind_push (P : PO1 -> Type) {HP : UFib P}
             (pol' : forall b, P (pol1 b)) (por' : forall c, P (por1 c))
             (push' : forall x, push1 x # pol' (f x) ~ por' (g x)) x
    : apD (PO1_ind P pol' por' push') (push1 x) = push' x.
  Proof.
    (* UFib_paths axiom *)
    eapply eq_paths.
    (* unfold apD, paths_ind, paths_ind', transport. cbn -[PO1_ind]. *)
  Abort.

End Pushout.
End PushoutQuotient.

Arguments pol {A B C f g}.
Arguments por {A B C f g}.
Arguments push {A B C f g}.
Arguments PO1 {A B C} f g.


(** DemiEndo is from Orton repo *)
Record IsDemiEndo (R : Type -> Type) :=
  { homR : forall {A B}, (A -> B) -> (R A -> R B) ;
    presid : forall A, homR (fun x : A => x) = idmap }.

Arguments homR {_} _ {_ _}.
Arguments presid {_} _ _.

Definition endoSubst {R : Type -> Type} (HR : IsDemiEndo R)
           {A} (P : A -> Type) {x y : A} (p : x = y)
  : homR HR (Etransport P p) = Etransport (R o P) p.
Proof.
  destruct p. exact (presid HR _).
Defined.


Definition Trans_DemiEndo {R : Type -> Type} (HR : IsDemiEndo R)
           {A} (P : A -> Type) (TA : Trans P)
  : Trans (R o P).
Proof.
  intros e a φ cst p0. specialize (TA e a φ cst).
  unshelve econstructor.
  - refine (homR HR _ p0). exact (fun p0 => (TA p0).1).
  - intros w. apply Eap10. unshelve refine ((endoSubst HR _ _)^E E@ _).
    apply Eap. apply funext.
    intro p0'. exact ((TA p0').2 w).
Defined.

Definition IsDemiEndo_repl : IsDemiEndo repl.
Proof.
  unshelve econstructor.
  apply @repl_f.
  intros; apply funext, repl_f_idmap.
Defined.

Definition Trans_repl {A} (P : A -> Type) (TA : Trans P)
  : Trans (repl o P).
Proof.
  apply (@Trans_DemiEndo repl). apply IsDemiEndo_repl.
  assumption.
Defined.


(** Definitions from "On Higher Inductive Types in Cubical Type Theory" *)
Local Definition fA {A} (P : A -> Type) (TA : Trans P)
      (e : OI) (a : I -> A) φ cst p0
  : exists (p : forall i : I, P (a i)), (p e = p0) × (p !e = (TA e a φ cst p0).1)
                              × forall u i, p i = cst u i E# p0.
Proof.
  simple refine (let XX := fun i => TA e (fun j => a (cnx e i j)) (or φ (is_e e i))
                                    _ _ in _).
  - cbn. induction 1.
    + intro j. exact ((cst H _)^E E@ cst H _).
    + intro j. eapply Eap.
      rewrite H. etransitivity; eauto.
  - cbn. refine (Etransport (P o a) _ p0).
    symmetry; auto.
  - cbn in XX.
    exists (fun i => Etransport (P o a) (cnx_ne' _ _) (XX i).1). repeat split.
    + pose proof ((XX e).2 (or_intror E1)).
      cbn in H. rewrite <- H.
      rewrite <- !Etransport_compose, <- !Etransport_pp.
      match goal with
      | |- ?XX E# _ = _ => assert (Y : XX = E1) by apply uip;
                           rewrite Y; clear Y; reflexivity
      end.
    + subst XX; cbn.
      rewrite Etransport_compose.
      rewrite (moveL_Etransport_V _ _ _ _
               (Etransport_trans TA e _ _ _ (fun i => Eap a (ne_cnx' e i)))).
      rewrite <-Etransport_pp.
      match goal with
      | |- ?XX E# _ = _ => assert (Y : XX = E1) by apply uip;
                           rewrite Y; clear Y; simpl
      end.
      unshelve eapply eq_trans.
      apply eq_Cof; cbn. apply propext; split; intro.
      induction H. assumption. contradiction (not_neg _ H).
      left; assumption.
      rewrite <- Etransport_compose, <- Etransport_pp.
      match goal with
      | |- ?XX E# _ = _ => assert (Y : XX = E1) by apply uip;
                           rewrite Y; clear Y; reflexivity
      end.
    + intros u i. rewrite <- ((XX i).2 (or_introl u)).
      rewrite !(Etransport_compose P a), <- !Etransport_pp.
      apply Eap10, Eap, uip.
Defined.


Local Definition sq {A} (P : A -> Type) (TA : Trans P)
      (e : OI) (a : I -> A) φ cst (p : forall i : I, P (a i))
  : exists (q : I -> P (a !e)), (q e = (TA e a φ cst (p e)).1) × (q !e = p !e).
Proof.
  simple refine (let XX := fun i => TA e (fun j => a (cnx !e i j)) (or φ (is_e !e i))
                                    _ _ in _).
  - cbn. induction 1.
    + intro j. exact ((cst H _)^E E@ cst H _).
    + intro j. eapply Eap.
      rewrite H. etransitivity; eauto.
  - cbn. refine (Etransport (P o a) _ (p i)).
    symmetry; auto.
  - cbn in XX.
    exists (fun i => Etransport (P o a) (cnx_e _ _) (XX i).1). repeat split.
    + subst XX; cbn.
      rewrite Etransport_compose.
      rewrite (moveL_Etransport_V _ _ _ _
               (Etransport_trans TA e _ _ _ (fun i => Eap a (cnx_ne e i)))).
      rewrite <-Etransport_pp.
      match goal with
      | |- ?XX E# _ = _ => assert (Y : XX = E1) by apply uip;
                           rewrite Y; clear Y; simpl
      end.
      unshelve eapply eq_trans.
      apply eq_Cof; cbn. apply propext; split; intro.
      induction H. assumption. contradiction (not_neg _ H^E).
      left; assumption.
      rewrite <- Etransport_compose, <- Etransport_pp.
      match goal with
      | |- ?XX E# _ = _ => assert (Y : XX = E1) by apply uip;
                           rewrite Y; clear Y; reflexivity
      end.
    + pose proof ((XX !e).2 (or_intror E1)).
      cbn in H. rewrite <- H.
      rewrite <- !Etransport_compose, <- !Etransport_pp.
      match goal with
      | |- ?XX E# _ = _ => assert (Y : XX = E1) by apply uip;
                           rewrite Y; clear Y; reflexivity
      end.
Defined.

Local Definition sq_cst {A} (P : A -> Type) (TA : Trans P)
      (e : OI) (a : I -> A) (φ : Cof) cst (p : forall i : I, P (a i))
      (cst' : forall u i, cst u i E# p e = p i)
  : φ -> forall i, (sq P TA e a φ cst p).1 e = (sq P TA e a φ cst p).1 i.
Proof.
  intros u i. unfold sq. cbn.
  match goal with
  | |- _ E# ?XX.1 = _ E# ?YY.1 => rewrite <- (XX.2 (or_introl u)),
                                <- (YY.2 (or_introl u)); cbn
  end.
  rewrite <- (cst' u i).
  rewrite !(Etransport_compose P a), <- !Etransport_pp.
  apply Eap10, Eap, uip.
Defined.


Definition Iinverse_e {A} {HA : Fib A} {x y : A} (e : OI) (p : I -> A)
           (p1 : p e = x) (p2 : p !e = y) (φ : Cof) (cst : φ -> forall i, p e = p i)
 : exists (p' : I -> A), p' e = y × p' !e = x × (φ -> forall i, p' e = p' i).
Proof.
  simple refine (_; _; _; _).
  - intro i.
    simple refine ((fib A).2 HA e (or φ (or (is_e e i) (is_e !e i))) _ _).1.
    intro j. unshelve eapply join. exact φ.2.
    3: unshelve eapply join.
    apply cof_or. all: try (now (destruct e; (apply cof0 || apply cof1))).
    exact (fun _ => x).  exact (fun _ => p j). exact (fun _ => x).
    all: cbn.
    intros u v. contradiction (not_neg _ (u^E E@ v)^E).
    intros u [v|v]. rewrite join_l. exact (p1^E E@ cst u _).
    rewrite join_r. reflexivity.
    exists x. intros [u|[u|u]]; cbn in *.
    rewrite join_l. reflexivity.
    rewrite join_r, join_l. exact p1.
    rewrite join_r, join_r. reflexivity.
  - cbn. match goal with
         | |- ?XX.1 = _ => unshelve rewrite <- XX.2; cbn
         end. right; left; reflexivity.
    rewrite join_r, join_l. exact p2.
  - cbn. match goal with
         | |- ?XX.1 = _ => unshelve rewrite <- XX.2; cbn
         end. right; right; reflexivity.
    rewrite join_r, join_r. reflexivity.
  - intros u i.
    match goal with
    | |- ?XX.1 = ?YY.1 => unshelve rewrite <- XX.2, <- YY.2; cbn
    end. now left. now left.
    rewrite !join_l. reflexivity.
Defined.
  

Local Definition l {X} {A B : X -> Type} (TA : Trans A) (TB : Trans B)
      (f : forall x, A x -> B x)
      (e : OI) (a : I -> X) φ cst (p0 : A (a e))
  : exists (q : I -> B (a !e)), (q e = (TB e a φ cst (f _ p0)).1)
                          × (q !e = f (a !e) (TA e a φ cst p0).1)
                          × (φ -> forall i, q e = q i).
Proof.
  pose (XX := sq B TB e a φ cst (fun i => f _ ((fA _ TA e a φ cst p0).1 i))).
  exists XX.1. repeat split.
  - refine (XX.2.1 E@ _).
    now rewrite (fA A TA e a φ cst p0).2.1.
  - refine (XX.2.2 E@ _).
    now rewrite (fA A TA e a φ cst p0).2.2.1.
  - subst XX. apply sq_cst.
    intros u i. rewrite !((fA A TA e a φ cst p0).2.2.2 u _).
    set (cst u i). set (a i) in *. destruct e0.
    apply (Eap (f (a e))). apply Eap10, Eap, uip.
Defined.


Local Definition li {X} {A B : X -> Type} (TA : Trans A) (TB : Trans B)
      (HB : forall x, Fib (B x))
      (f : forall x, A x -> B x)
      (e : OI) (a : I -> X) φ cst (p0 : A (a e))
  : exists (q : I -> B (a !e)), (q e = f (a !e) (TA e a φ cst p0).1)
                          × (q !e = (TB e a φ cst (f _ p0)).1)
                          × (φ -> forall i, q e = q i).
Proof.
  pose proof (l TA TB f e a φ cst p0) as p.
  exact (Iinverse_e e p.1 p.2.1 p.2.2.1 _ p.2.2.2).
Defined.


Definition PO1_rec_idmap {A B C} {f : A -> B} {g : A -> C}
  : PO1_rec (PO1 f g) pol1 por1 push1 == idmap.
Proof.
  simple refine (repl_ind _ _ _ _); cbn -[Fib_repl].
  - simple refine (PO_ind _ _ _ _ _ _); cbn.
    all: try reflexivity.
    all: intro; apply uip.
  - intros e φ a H. refine (Eap (hcomp e φ) _).
    funext i x. cbn beta. destruct x.
    + rewrite join_l. rewrite join_l. apply H.
    + rewrite join_r. cbn. rewrite join_r.
      rewrite !Etransport_const. destruct e0. apply H.
  - intros; apply uip.
Defined.
   

Definition PO1_rec_idmap' {X} {A B C : X -> Type}
           (f : forall x, A x -> B x) (g : forall x, A x -> C x)
           (e : OI) (a : I -> X) (cst : a e = a !e)
           (pol' : B (a e) -> B (a !e)) (por' : C (a e) -> C (a !e))
           (push' : forall x, pol1 (f:=f (a !e)) (g:= g _)(pol' (f _ x))
                              ~ por1 (por' (g _ x)))
           (e_l : pol' == Etransport _ cst)
           (e_r : por' == Etransport _ cst)
           (e_p : forall x i, push' x i
                         = Etransport (fun a => PO1 (f a) (g a)) cst (push1 x i))
  : PO1_rec _ (pol1 o pol') (por1 o por') push'
    == Etransport (fun a => PO1 (f a) (g a)) cst.
Proof.
  set (a !e) in *. destruct cst; cbn in *. intro.
  apply funext in e_l. symmetry in e_l. destruct e_l.
  apply funext in e_r. symmetry in e_r. destruct e_r.
  assert (e_p' : push' = push1). {
    funext x'. apply eq_paths. apply e_p. }
  symmetry in e_p'. destruct e_p'. apply PO1_rec_idmap.
Defined.

Opaque fA sq li.


Definition Trans_PO {X} {A B C : X -> Type}
           (HA : UFib A) (HB : UFib B) (HC : UFib C)
           (f : forall x, A x -> B x) (g : forall x, A x -> C x)
  : Trans (fun x => PO1 (f x) (g x)).
Proof.
  pose proof (UFib_Trans _ HA) as TA.
  pose proof (UFib_Trans _ HB) as TB.
  pose proof (UFib_Trans _ HC) as TC.
  intros e a φ cst p0.
  pose (Fib_repl (PO (f (a !e)) (g (a !e)))) as HP.
  unshelve econstructor.
  - unshelve refine (PO1_rec _ _ _ _ p0).
    exact (fun y => pol1 (TB e a φ cst y).1).
    exact (fun y => por1 (TC e a φ cst y).1).
    intro x. path r.
    + simple refine (HP e (fun _ => tt)
                        (or φ (or (is0 r) (is1 r))) _ (_; _)).1; cbn.
      * intro i.
        simple refine (join _ (join _ _ _) _).
        exact φ.2. apply cof_or. all: try (apply cof0 || apply cof1).
        -- refine (fun w => push1 _ r). exact (cst w !e E# x).
        -- intro H. apply pol1.
           exact ((li TA TB (UFib_DFib _ _) f e a φ cst x).1 i).
        -- intro H. apply por1.
           exact ((li TA TC (UFib_DFib _ _) g e a φ cst x).1 i).
        -- intros u v; contradiction (zero_one (u^E E@ v)).
        -- intros u [].
           ++ rewrite join_l. rewrite e0; clear r e0.
              cbn. rewrite push_l. apply (Eap pol1).
              rewrite <- ((li TA TB (UFib_DFib B HB) f e a φ cst x).2.2.2 u).
              rewrite ((li TA TB (UFib_DFib B HB) f e a φ cst x).2.1).
              rewrite (TA e a φ cst x).2. reflexivity.
           ++ rewrite join_r. rewrite e0; clear r e0.
              cbn. rewrite push_r. apply (Eap por1).
              rewrite <- ((li TA TC (UFib_DFib C HC) g e a φ cst x).2.2.2 u).
              rewrite ((li TA TC (UFib_DFib C HC) g e a φ cst x).2.1).
              rewrite (TA e a φ cst x).2. reflexivity.
      * refine (push1 _ r). exact (TA e a φ cst x).1.
      * intros [u|[H|H]]; cbn.
        -- rewrite join_l.
           now rewrite (TA e a φ cst x).2.
        -- rewrite join_r, join_l.
           rewrite H; clear r H.
           rewrite push_l. apply (Eap pol1).
           now rewrite (li TA TB (UFib_DFib B HB) f e a φ cst x).2.1.
        -- rewrite join_r, join_r.
           rewrite H; clear r H.
           rewrite push_r. apply (Eap por1).
           now rewrite (li TA TC (UFib_DFib C HC) g e a φ cst x).2.1.
    + cbn beta.
      match goal with
      | |- ?XX.1 = _ => unshelve rewrite <- XX.2; cbn
      end. right; left; reflexivity.
      rewrite join_r, join_l.
      apply Eap.
      now rewrite (li TA TB (UFib_DFib B HB) f e a φ cst x).2.2.1.
    + cbn beta.
      match goal with
      | |- ?XX.1 = _ => unshelve rewrite <- XX.2; cbn
      end. right; right; reflexivity.
      rewrite join_r, join_r.
      apply Eap.
      now rewrite (li TA TC (UFib_DFib C HC) g e a φ cst x).2.2.1.
  - intro u. refine (PO1_rec_idmap' f g e a (cst u !e) _ _ _ _ _ _ _)^E.
    intro x. symmetry; exact ((TB e a φ cst x).2 u).
    intro x. symmetry; exact ((TC e a φ cst x).2 u).
    intros x i. cbn -[HP].
    match goal with
    | |- ?XX.1 = _ => unshelve rewrite <- XX.2; cbn
    end. now left.
    rewrite join_l. clear. set (cst u !e).
    destruct e0. reflexivity.
Defined.



Instance UFib_PO1 {X} {A B C : X -> Type} (HA : UFib A) (HB : UFib B) (HC : UFib C)
         (f : forall x, A x -> B x) (g : forall x, A x -> C x)
  : UFib (fun x => PO1 (f x) (g x)).
Proof.
  eapply TransFib_HFib. exact _.
  now apply Trans_PO.
Defined.



Definition cone A := PO1 idmap (fun _ : A => tt).
Definition inc {A} (x : A) : cone A := pol1 x.
Definition point {A} : cone A := por1 tt.
Definition cone_eq {A} (x : A) : inc x ~ point := push1 x.

Definition cone_ind {A} (P : cone A -> Type) {HP : UFib P} (l' : forall x, P (inc x))
           (r' : P point) (eq' : forall x, cone_eq x # l' x ~ r')
  : forall w, P w.
Proof.
  unshelve refine (PO1_ind P _ _ _). assumption.
  intros []; assumption.
  assumption.
Defined.

Instance UFib_cone {X} {A : X -> Type} (HA : UFib A)
  : UFib (fun x => cone (A x)).
Proof.
  unfold cone. exact _.
Defined.

Definition Contr_cone A : Contr (cone A).
Proof.
  exists point.
  intro z; eapply inverse; revert z.
  simple refine (cone_ind _ _ _ _); cbn.
  exact cone_eq. exact P1.
  intro x; exact (transport_paths_rp (push1 x)).
Defined.

Section Cylinder.
  Context {A B} (f : A -> B).

  Definition Cyl (y : B) := cone (exists x, f x I~ y).
  Definition top x : Cyl (f x) := inc (x; idrefl (f x)).
  Definition base y : Cyl y := point.
  Definition cyl_eq x : top x ~ base (f x)  := cone_eq _.
End Cylinder.

Arguments top {A B f}.
Arguments base {A B f}.
Arguments cyl_eq {A B f}.

Instance UFib3_Cyl {X} {A B : X -> Type} (FibA: UFib A) (FibB: UFib B)
  : UFib (fun x : exists (x : X) (f : A x -> B x), B x => Cyl x.2.1 x.2.2).
Proof.
  unfold Cyl. exact _.
Defined.

Instance UFib_Cyl {X} {A B : X -> Type} (FibA: UFib A) (FibB: UFib B)
         {f : forall x, A x -> B x} (t : forall x, B x)
  : UFib (fun x => Cyl (f x) (t x)).
Proof.
  exact (UFib_precomp _ (UFib3_Cyl FibA FibB) (fun x => (x; f x; t x))).
Defined.

Hint Extern 0 (UFib (Cyl ?t)) =>
  simple refine (UFib_Cyl _ _ (f:=fun _ => t) _) : typeclass_instances.


Section Cylinder2.
  Context {A B} {FibA: Fib A} {FibB: Fib B} {f: A -> B}.

  Definition Cyl_ind (P : forall y, Cyl f y -> Type) {FibP : UFib2 P}
             (top' : forall x, P (f x) (top x))
             (base' : forall y, P y (base y))
             (cyl_eq' : forall x, cyl_eq x # top' x ~ base' _)
  : forall y (w : Cyl f y), P y w.
  Proof.
    intro y. unfold Cyl. unshelve refine (PO1_ind _ _ _ _).
    exact (UFib2_UFib P _ y).
    - intros [x p]. pose (top' x). unfold top in *.
      refine (J (fun y (p : f x I~ y) => P y (pol1 (x; p))) p (top' x)).
      refine (UFib_precomp _ FibP (fun w => (w.1; _))).
    - intros []. exact (base' y).
    - intros [x p]; cbn.
      Typeclasses eauto :=0.
      refine (J (fun y p => transport (P y) (push1 (x; p)) (J (fun (y0 : B) (p0 : f x I~ y0) => P y0 (pol1 (x; p0))) p (top' x)) ~ base' y) p _).
      eapply UFib_paths'.
      exact (UFib_precomp _ FibP (fun w => (w.1; base (f:=f) w.1))).
      Typeclasses eauto :=4.
      rewrite J_refl.
      exact (cyl_eq' x).
  Defined.

  Definition Cyl_ind_top (P : forall y, Cyl f y -> Type) {FibP : UFib2 P}
             (top' : forall x, P (f x) (top x))
             (base' : forall y, P y (base y))
             (cyl_eq' : forall x, cyl_eq x # top' x ~ base' _) x
    : Cyl_ind P top' base' cyl_eq' _ (top x) = top' x.
  Proof.
    unfold Cyl_ind, top, inc. rewrite PO1_ind_pol.
    now rewrite J_refl.
  Defined.

  Definition Cyl_ind_base (P : forall y, Cyl f y -> Type) {FibP : UFib2 P}
             (top' : forall x, P (f x) (top x))
             (base' : forall y, P y (base y))
             (cyl_eq' : forall x, cyl_eq x # top' x ~ base' _) y
    : Cyl_ind P top' base' cyl_eq' _ (base y) = base' y.
  Proof.
    unfold Cyl_ind, base, point. refine (PO1_ind_por _ _ _ _ _).
  Defined.


  Definition Cyl_Contr (y: B) : Contr (Cyl f y)
    := Contr_cone _.

  (* ∃ y, Cyl f y  is the homotopy pushout of f and idmap *)
  Definition sig_cyl_ind (P: sigT (Cyl f) -> Type) {FibP: UFib P}
             (top': forall x, P (f x; top x))
             (base': forall y, P (y; base y))
             (cyl_eq': forall x,
                 transport (fun w => P (f x; w)) (cyl_eq x) (top' x) ~ base' (f x))
    : forall w, P w.
  Proof.
    intros [y w].
    exact (Cyl_ind (fun y w => P (y; w)) top' base' cyl_eq' y w).
  Defined.

  Definition sig_cyl_rec P {FibP: Fib P}
             (top': A -> P) (base': B -> P)
             (cyl_eq': forall x, top' x ~ base' (f x))
    : sigT (Cyl f) -> P.
  Proof.
    intros [y w].
    ref (Cyl_ind (fun y w => P) top' base' _ y w); cbn.
    intro x. exact (transport_const _ _ @ cyl_eq' _).
  Defined.
End Cylinder2.
