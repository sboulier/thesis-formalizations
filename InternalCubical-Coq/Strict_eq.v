Require Export Overture.

Delimit Scope eq_scope with eq.
Open Scope eq_scope.

Arguments eq_refl {A x} , [A] x.

Scheme eq_ind := Induction for eq Sort Type.
Arguments eq_ind [A] x P f y e.
Scheme eq_rec := Minimality for eq Sort Type.
Arguments eq_rec [A] x P f y e.

Bind Scope eq_scope with eq.

Arguments eq_sym {A x y} p : simpl nomatch.
Arguments eq_trans {A x y z} p q : simpl nomatch.
Arguments eq_rect {A x} P _ {y} _.

Notation "'E1'" := eq_refl : eq_scope.
Notation "p E@ q" := (eq_trans p%eq q%eq) (at level 20) : eq_scope.
Notation "p ^E" := (eq_sym p%eq) (at level 3, format "p '^E'") : eq_scope.

Definition eq_trans' {A} {x y z : A} : x = y -> y = z -> x = z.
  destruct 1. exact idmap.
Defined.

Notation "p E@' q" := (eq_trans' p%eq q%eq) (at level 20) : eq_scope.

Definition Etransport {A : Type} (P : A -> Type) {x y : A} (p : x = y) (u : P x) : P y :=
  match p with eq_refl => u end.
Arguments Etransport {A}%type_scope P {x y} p%eq_scope u : simpl nomatch.

Notation "p E# x"
  := (Etransport _ p x) (right associativity, at level 65, only parsing) : eq_scope.

Notation "f == g" := (forall x, f x = g x) (at level 70, no associativity) : type_scope.

Definition Eap {A B:Type} (f:A -> B) {x y:A} (p:x = y) : f x = f y
  := match p with eq_refl => eq_refl end.
Global Arguments Eap {A B}%type_scope f {x y} p%eq_scope.

Definition EapD10 {A} {B: A -> Type} {f g: forall x, B x} (h: f = g)
  : f == g
  := fun x => match h with eq_refl => E1 end.
Global Arguments EapD10 {A%type_scope B} {f g} h%eq_scope _.

Definition Eap10 {A B} {f g: A -> B} (h: f = g) : f == g
  := EapD10 h.
Global Arguments Eap10 {A B}%type_scope {f g} h%eq_scope _.

Axiom funext: forall {A: Type} {P : A -> Type} {f g : forall x : A, P x},
    f == g -> f = g.

Tactic Notation "funext" ident(i) := apply funext; intro i.
Tactic Notation "funext" ident(i) ident(j) := funext i; funext j.
Tactic Notation "funext" ident(i) ident(j) ident(k)
  := funext i; funext j; funext k.


(* Axiom uip : forall {A: Type} {x y: A} (p q: x = y), p = q. *)
Definition uip : forall {A: Type} {x y: A} (p q: x = y), p = q.
  intros A x y p q; apply proof_irr.
Defined.

Record IsSEquiv {A B} (f : A -> B) :=
  { sequiv_g : B -> A ;
    sequiv_gf : sequiv_g o f == idmap ; sequiv_fg : f o sequiv_g == idmap }.


Record SEquiv A B :=
  { sequiv_f : A -> B ; sequiv_is_sequiv : IsSEquiv sequiv_f }.

Coercion sequiv_f : SEquiv >-> Funclass.
Coercion sequiv_is_sequiv : SEquiv >-> IsSEquiv.



Definition EapD {A:Type} {B:A->Type} (f:forall a:A, B a) {x y:A} (p:x=y):
  p E# (f x) = f y
  :=
  match p with eq_refl => eq_refl end.
Arguments EapD {A%type_scope B} f {x y} p%eq_scope : simpl nomatch.

Definition Etransport_Vp {A: Type} (P: A -> Type) {x y: A} (p: x = y) (z: P x)
  : p^E E# p E# z = z.
Proof.
  destruct p; reflexivity.
Defined.

Definition Etransport_pV {A: Type} (P: A -> Type) {x y: A} (p: x = y) (z: P y)
  : p E# p^E E# z = z.
Proof.
  destruct p; reflexivity.
Defined.


Definition Etransport_compose {A B : Type} {x y : A} (P : B -> Type) (f : A -> B)
           (p : x = y) (z : P (f x)) :
  Etransport (fun x0 : A => P (f x0)) p z = Etransport P (Eap f p) z.
destruct p. reflexivity.
Defined.


Definition eq_sigma {A: Type} (P: A -> Type) {x x': A} {y: P x} {y': P x'}
           (p: x = x') (q: p E# y = y')
  : (x; y) = (x'; y').
Proof.
  destruct p, q; reflexivity.
Defined.

Definition eq_sigma' {A: Type} (P: A -> Type) {u v : sigT P}
           (p : u.1 = v.1) (q : p E# u.2 = v.2)
  : u = v.
Proof.
  destruct u, v; cbn in *. eapply eq_sigma; eassumption.
Defined.

Definition Etransport_sigma' {A B : Type} {C : A -> B -> Type}
           {x1 x2 : A} (p : x1 = x2) yz
: Etransport (fun x => sigT (C x)) p yz =
  (yz.1 ; Etransport (fun x => C x yz.1) p yz.2).
Proof.
  destruct p. destruct yz. reflexivity.
Defined.

Definition pr1_eq {A : Type} `{P : A -> Type} {u v : sigT P} (p : u = v)
  : u.1 = v.1
  := Eap pr1 p.

Notation "p ..1E" := (pr1_eq p) (at level 3).

Definition pr2_eq {A : Type} `{P : A -> Type} {u v : sigT P} (p : u = v)
  : p..1E E# u.2 = v.2
  := (Etransport_compose P pr1 p u.2)^E
     E@ (@EapD { x & P x} _ pr2 _ _ p).

Notation "p ..2E" := (pr2_eq p) (at level 3).



(* inverse and composition *)
Definition Einv_V {A} {x y : A} (p : x = y)
  : (p^E)^E = p.
  now destruct p.
Defined.

Definition Econcat_Vp {A} {x y : A} (p : x = y)
  : p^E E@ p = E1.
Proof.
  now destruct p.
Defined.

Definition Econcat_pV {A} {x y : A} (p : x = y)
  : p E@ p^E = E1.
Proof.
  now destruct p.
Defined.

Definition Econcat_1p {A} {x y : A} (p : x = y)
  : E1 E@ p = p.
Proof.
  now destruct p.
Defined.

Definition Econcat_p1 {A} {x y : A} (p : x = y)
  : p E@ E1 = p.
Proof.
  now destruct p.
Defined.

Definition Econcat_p_pp {A} {x y z t : A} (p : x = y)
           (q : y = z) (r : z = t)
  : p E@ (q E@ r) = (p E@ q) E@ r.
  now destruct p, q, r.
Defined.

Definition Econcat_pp_p {A} {x y z t : A} (p : x = y)
           (q : y = z) (r : z = t)
  : (p E@ q) E@ r = p E@ (q E@ r).
  now destruct p, q, r.
Defined.


(* Eap *)
Definition Eap_pp {A B} (f : A -> B) {x y z} (p : x = y) (q : y = z)
  : Eap f (p E@ q) = Eap f p E@ Eap f q.
  now destruct p, q.
Defined.

Definition Eap_V {A B} (f : A -> B) {x y : A} (p : x = y)
  : Eap f p^E = (Eap f p)^E.
Proof.
  now destruct p.
Defined.

Definition Eap_const {A B} {x y : A} (p : x = y) (z : B)
  : Eap (fun _ => z) p = E1.
Proof.
  now destruct p.
Defined.

Definition Eap_compose {A B C} (f : A -> B) (g : B -> C) {x y}
           (p : x = y)
  : Eap (g o f) p = Eap g (Eap f p).
  now destruct p.
Defined.






Definition Etransport_arrow_toconst {A} {B : A -> Type} {C}
           {x1 x2 : A} (p : x1 = x2) (f : B x1 -> C) (y : B x2)
  : (Etransport (fun x => B x -> C) p f) y  =  f (p^E E# y).
Proof.
  destruct p; simpl; auto.
Defined.

Definition Etransport_idmap_Eap A (P : A -> Type) x y (p : x = y) (u : P x)
  : Etransport P p u = Etransport idmap (Eap P p) u.
  destruct p; reflexivity.
Defined.

Definition Etransport_pp {A : Type} (P : A -> Type) {x y z : A} (p : x = y) (q : y = z) (u : P x) :
  p E@ q E# u = q E# p E# u.
  destruct p, q; reflexivity.
Defined.

Definition Etransport_const {A B : Type} {x1 x2 : A} (p : x1 = x2) (y : B)
  : Etransport (fun x => B) p y = y.
Proof.
  destruct p.  exact E1.
Defined.

Definition Etransport_forall_constant
  {A B : Type} {C : A -> B -> Type}
  {x1 x2 : A} (p : x1 = x2) (f : forall y : B, C x1 y)
  : (Etransport (fun x => forall y : B, C x y) p f)
    == (fun y => Etransport (fun x => C x y) p (f y)).
  destruct p; reflexivity.
Defined.

Definition EtransportD {A : Type} (B : A -> Type) (C : forall a:A, B a -> Type)
  {x1 x2 : A} (p : x1 = x2) (y : B x1) (z : C x1 y)
  : C x2 (p E# y).
  now destruct p.
Defined.

Definition Etransport_forall
  {A : Type} {P : A -> Type} {C : forall x, P x -> Type}
  {x1 x2 : A} (p : x1 = x2) (f : forall y : P x1, C x1 y)
  :
Etransport (fun x : A => forall y : P x, C x y) p f ==
(fun y : P x2 =>
 Etransport (C x2) (Etransport_pV P p y)
   (EtransportD P C p (Etransport P p^E y) (f (Etransport P p^E y)))).
Proof.
  destruct p; reflexivity.
Defined.

Definition Etransport_arrow {A : Type} {B C : A -> Type}
  {x1 x2 : A} (p : x1 = x2) (f : B x1 -> C x1) (y : B x2)
  : (Etransport (fun x => B x -> C x) p f) y  =  p E# (f (p^E E# y)).
Proof.
  destruct p; simpl; auto.
Defined.


Definition EapD011 {A} {B : A -> Type} {C} (f : forall x, B x -> C) {x x' y y'}
           (p : x = x')
           (q : p E# y = y')
  : f x y = f x' y'.
Proof.
  destruct p, q; reflexivity.
Defined.


Definition Etransport2 {A} {B : A -> Type} (P : forall x, B x -> Type)
           {x x' y y'} (e : x = x') (e' : e E# y = y')
  : P x y -> P x' y'.
Proof.
  destruct e, e'; exact idmap.
Defined.

Definition eta_unit (x y : unit) : x = y.
Proof.
  destruct x, y; reflexivity.
Defined.


Definition moveR_Etransport_p {A : Type} (P : A -> Type) {x y : A}
  (p : x = y) (u : P x) (v : P y)
  : u = p^E E# v -> p E# u = v.
Proof.
  destruct p.
  exact idmap.
Defined.

Definition moveR_Etransport_V {A : Type} (P : A -> Type) {x y : A}
  (p : y = x) (u : P x) (v : P y)
  : u = p E# v -> p^E E# u = v.
Proof.
  destruct p.
  exact idmap.
Defined.

Definition moveL_Etransport_V {A : Type} (P : A -> Type) {x y : A}
  (p : x = y) (u : P x) (v : P y)
  : p E# u = v -> u = p^E E# v.
Proof.
  destruct p.
  exact idmap.
Defined.

Definition moveL_Etransport_p {A : Type} (P : A -> Type) {x y : A}
  (p : y = x) (u : P x) (v : P y)
  : p^E E# u = v -> u = p E# v.
Proof.
  destruct p.
  exact idmap.
Defined.
