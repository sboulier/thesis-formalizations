# thesis-formalizations

Formalizations accompanying my thesis "Extending Type Theory with Syntactical Models":

https://gitlab.inria.fr/sboulier/thesis-formalizations/blob/master/thesis-boulier-latest.pdf

Each folder contains a Readme explaining how to compile.

## CwF-Coq

Formalization of categories with families in Coq.
We give the standard model and the setoid model.
For the setoid model, formalization of the equivalence between SetoidFam and SetoidFam'.
Requires Coq-sprop for the setoid model.

## HTS-Coq

Implementation of a Two Level Type Theory in Coq

## InternalCubical-Coq

Formalization of Chapter 3, "Adventures in Cubical Type Theory".
Formalization in Coq of Interval Type Theory and of several constructions, including fibrant replacement and homotopy pushouts.

## SetoidUniverse-Agda

Formalization of a universe closed under Π types for the setoid model.

## TemplateCoq

Copy of Template Coq repository with several improvements for the translation plugin.

## TimesBoolCorrectness-Coq

Formalization of typing soundness for the "times bool" translations.

## TYPE.agda

Construction, in Agda, of the two first internal universes needed for the Ad-hoc polymorphism translation.

This file is self contained, tested with Agda 2.6.0.
