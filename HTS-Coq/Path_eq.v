(* -*- coq-prog-args: ("-top" "HTS.Path_eq") -*-  *)
Require Import HTS.Strict_eq.

(** The fibrancy condition *)
Axiom Fibrant : Type -> Type.
(** We use typeclasses to infer it automatically *)
Existing Class Fibrant.

Notation "'FibrantF' P" := (forall x, Fibrant (P x)) (at level 10) : Fib_scope.
Notation "'FibrantF2' P" := (forall x y, Fibrant (P x y)) (at level 10) : Fib_scope.
Open Scope Fib_scope.


(** The universe of fibrant types *)
Record TypeF := {
  TypeF_T : Type;
  TypeF_F : Fibrant TypeF_T
}.

Arguments Build_TypeF _ {_}.

(** We use a coercion to be able to write [forall (A : TypeF), A] instead of [forall (A : TypeF), TypeF_T A] *)
Coercion TypeF_T : TypeF >-> Sortclass.

Existing Instance TypeF_F.


(** The fibrancy rules *)
Axiom Fibrant_forall
  : forall A (B: A -> Type), Fibrant A -> (forall x, Fibrant (B x)) -> Fibrant (forall x, B x).
Existing Instance Fibrant_forall.

Axiom Fibrant_sigma
  : forall A (B: A -> Type), Fibrant A -> (forall x, Fibrant (B x)) -> Fibrant (sigT B).
Existing Instance Fibrant_sigma.

(* Axiom Fibrant_Type : Fibrant Type. *)

(* Axiom Fibrant_TypeF : Fibrant FType. *)

Axiom Fibrant_unit : Fibrant unit.
Existing Instance Fibrant_unit.


(** * Definition of paths * *)

(** We use a private inductive type to disable pattern matching on a path so *)
(** that the only way to destruct an equality is to use one of the eliminators. *)
(** The eliminators ensure the fibrancy condition. *)

Module Export Paths.
  Private Inductive paths {A : Type} (a : A) : A -> Type :=
    idpath : paths a a.

  Arguments idpath {A a} , [A] a.

  Definition paths_ind (A : Type) {FibA: Fibrant A} (a : A)
             (P : forall a0 : A, paths a a0 -> Type) {FibP: FibrantF2 P}
             (f: P a idpath) (y : A) (p : paths a y) : P y p
    := match p as p0 in (paths _ y0) return (P y0 p0) with
       | idpath => f
       end.
  Arguments paths_ind [A _] a P [_] f y p.

  Definition paths_rec (A : Type) {FibA: Fibrant A} (a : A) (P : A -> Type)
             {FibP: FibrantF P} (f : P a) (y : A) (p : paths a y)
    : P y :=
    match p in (paths _ y0) return (P y0) with
    | idpath => f
    end.

  Axiom Fibrant_paths : forall (A: Type) {FibA: Fibrant A} (x y: A), Fibrant (paths x y).
  Existing Instance Fibrant_paths.

  (** The inverse of a path. *)
  Definition inverse {A : Type} {FibA: Fibrant A} {x y : A} (p : paths x y) : paths y x
    := @paths_rec A FibA x (fun y => paths y x) _ idpath y p.

  Definition paths_rec' A {FibA: Fibrant A} a y P {FibP: FibrantF P} (X : P y)
             (H : @paths A a y) : P a
    := @paths_rec A FibA y P FibP X _ (inverse H).


  (** * myrewrite plugin * *)
  (** This plugin avoids a flaw in the mechanism of private inductive types. *)
  (** When we rewrite with a path equality, Coq uses the automatically generated terms internal_paths_rew and internal_paths_rew_r.*)
  (** However, those terms doesn't check the fibrancy condition.  *)
  (** Hence this plugin forces Coq to use paths_rec and paths_rec' instead. *)
  Declare ML Module "myrewrite".
End Paths.

Arguments paths_rec [A _] a P [_] f y p.

Notation "x = y :> A" := (@paths A x y) : type_scope.
Notation "x = y" := (x = y :> _) : type_scope.

Notation "f == g" := (forall x, f x = g x) (at level 70, no associativity) : type_scope.


Tactic Notation "rew" open_constr(H)
  := rewrite H; try exact _.
Tactic Notation "rewi" open_constr(H)
  := rewrite <- H; try exact _.

(* This does not fail if you comment the "Declare ML Module..." line above. *)
Lemma  paths_rew_r_test_should_fail A {FibA: Fibrant A} a y P (X : P y) (H : a = y :> A) : P a.
Proof. Fail rewrite H; assumption. Abort.

Lemma paths_rew_test A {FibA: Fibrant A} a y P {FibP: FibrantF P} (X : P a) (H : a = y :> A) : P y.
Proof. rewi H; assumption. Defined.

Lemma paths_rew_r_test A {FibA: Fibrant A} a y P {FibP: FibrantF P} (X : P y) (H : a = y :> A) : P a.
Proof. rew H; assumption. Defined.


Definition Eq_to_paths {A : Type} {FibA: Fibrant A} {x y : A} (p : x ≡ y) : x = y :=
  match p with
    | refl => idpath
  end.


Definition concat {A : Type} {FibA: Fibrant A} {x y z : A} (p : x = y) (q : y = z) : x = z.
Proof.
  refine (@paths_rec A _ y (fun z => x = z) _
                  (@paths_rec A _ x (fun y => x = y)_ idpath y p) z q).
Defined.

Arguments concat {A FibA x y z} !p !q.

Delimit Scope path_scope with path.
Open Scope path_scope.

Notation "p @ q" := (concat p%path q%path) (at level 20) : path_scope.
Notation "p ^" := (inverse p%path) (at level 3, format "p '^'") : path_scope.
Notation "1" := idpath : path_scope.



Definition transport {A : Type} {FibA: Fibrant A} (P : A -> Type)
           {FibP: FibrantF P}  {x y : A} (p : x = y) (u : P x) : P y
  := paths_rec x P u y p.

Arguments transport {A}%type_scope {FibA} P {FibP} {x y} p%path_scope u : simpl nomatch.

Notation "p # x"
  := (transport _ p x) (right associativity, at level 65, only parsing) : path_scope.


Record Contr (A: Type) {FibA: Fibrant A} :=
  { center : A;
    contr : ∀ x, center = x }.



(** * destruct_path tactic * *)

(** Because of the private inductive type we cannot use the [destruct] tactic on a path equality. *)
(** This is very annoying... [destruct_path] does its best to play the same role but it is *)
(** much less powerful. *)

(* auxiliary tactics *)
Definition myid : forall A, A -> A := fun _ x => x.
Ltac mark H := let t := type of H in change (myid _ t) in H.
Ltac unmark H := let t := type of H in
                 match t with
                 | myid _ ?tt => change tt in H
                 end.
Hint Unfold myid : typeclass_instances.

(* If p : x = y  then destruct_path revert all hypothesis depending on x and y.  *)
(* Then, it applies paths_ind and then it reintroduces reverted hypothesis. *)
Ltac destruct_path p :=
  let t := type of p in
  match t with
    @paths _ ?x ?y =>
    mark p;
      repeat match goal with
             | [X: context[y] |- _] =>
               match type of X with
               | myid _ _ => fail 1
               | _ => revert X;
                   match goal with
                   | |- forall (X: ?T), ?G => change (forall (X: myid _ T), G)
                   end
               end
             end;
      unmark p;
      generalize y p; clear p y;
      match goal with
      | |- forall y p, @?P y p => let y := fresh y in
                                  let p := fresh p in
                                  intros y p; refine (paths_ind x P _ y p)
      end;
      repeat match goal with
             | |- forall (H: myid _ _), _ => let H := fresh H in
                                             intro H; unfold myid in H
             end
  end.


(** * Groupoidal laws * *)


Definition ap {A B: Type} {FibA: Fibrant A} {FibB: Fibrant B} (f: A -> B) {x y: A} (p: x = y)
  : f x = f y
  := paths_rec x (fun y => f x = f y) idpath y p.

Arguments ap {A B}%type_scope {FibA FibB} f {x y} p%path_scope.

Definition apD {A: Type} {FibA: Fibrant A} {B: A -> Type} {FibB: FibrantF B}
           (f: forall a: A, B a) {x y: A} (p: x = y)
  : p # (f x) = f y
  := paths_ind x (fun y p => transport B p (f x) = f y) 1 y p.

Arguments apD {A%type_scope FibA B FibB} f {x y} p%path_scope : simpl nomatch.


Definition ap_pp {A B} {FibA: Fibrant A} {FibB: Fibrant B} (f : A -> B) {x y z : A} (p : x = y) (q : y = z) :
  ap f (p @ q) = (ap f p) @ (ap f q).
Proof.
  destruct_path p.
  destruct_path q.
  exact 1.
Defined.

Definition ap_V {A B} {FibA: Fibrant A} {FibB: Fibrant B} (f : A -> B) {x y : A} (p : x = y) :
  ap f (p^) = (ap f p)^.
Proof.
  destruct_path p; exact 1.
Defined.

Definition inv_pp {A} {FibA: Fibrant A} {x y z : A} (p : x = y) (q : y = z) :
  (p @ q)^ = q^ @ p^.
Proof.
  destruct_path p.
  destruct_path q.
  exact 1.
Defined.

Definition inv_V {A} {FibA: Fibrant A} {x y : A} (p : x = y) :
  p^^ = p.
Proof.
  destruct_path p; exact 1.
Defined.

Definition ap_compose {A B C} {FibA: Fibrant A} {FibB: Fibrant B} {FibC: Fibrant C} (f : A -> B) (g : B -> C) {x y : A} (p : x = y) :
  ap (g o f) p = ap g (ap f p).
Proof.
  destruct_path p; exact 1.
Defined.

Definition concat_p_pp {A} {FibA: Fibrant A} {x y z t : A} (p : x = y) (q : y = z) (r : z = t) :
  p @ (q @ r) = (p @ q) @ r.
Proof.
  destruct_path r.
  destruct_path q.
  destruct_path p.
  exact 1.
Defined.

Definition concat_pp_p {A} {FibA: Fibrant A} {x y z t : A} (p : x = y) (q : y = z) (r : z = t) :
  (p @ q) @ r = p @ (q @ r).
Proof.
  destruct_path r.
  destruct_path q.
  destruct_path p.
  exact 1.
Defined.

Definition concat_1p {A} {FibA: Fibrant A} {x y: A} (p: x = y)
  : 1 @ p = p.
Proof.
  destruct_path p.
  exact 1.
Defined.

Definition concat_p1 {A} {FibA: Fibrant A} {x y: A} (p: x = y)
  : p @ 1 = p.
Proof.
  destruct_path p.
  exact 1.
Defined.

Definition concat_pV {A} {FibA: Fibrant A} {x y : A} (p : x = y) :
  p @ p^ = 1
  := paths_ind x (fun y p => p @ p^ = 1) 1 _ _.

Definition concat_Vp {A} {FibA: Fibrant A} {x y : A} (p : x = y) :
  p^ @ p = 1
  := paths_ind x (fun y p => p^ @ p = 1) 1 _ _.

Definition moveR_Vp {A} {FibA: Fibrant A} {x y z : A} (p : x = z) (q : y = z) (r : x = y) :
  p = r @ q -> r^ @ p = q.
Proof.
  destruct_path r.
  intro h. exact (concat_1p _ @ h @ concat_1p _).
Defined.

Definition moveL_Vp {A} {FibA: Fibrant A} {x y z : A} (p : x = z) (q : y = z) (r : x = y) :
  r @ q = p -> q = r^ @ p.
Proof.
  destruct_path r.
  intro h. exact ((concat_1p _)^ @ h @ (concat_1p _)^).
Defined.

Definition moveR_M1 {A} {FibA: Fibrant A} {x y : A} (p q : x = y) :
  1 = p^ @ q -> p = q.
Proof.
  destruct_path p.
  intro h. exact (h @ (concat_1p _)).
Defined.

Definition concat_pA1 {A} {FibA: Fibrant A} {f : A -> A} (p : forall x, x = f x) {x y : A} (q : x = y) :
  (p x) @ (ap f q) =  q @ (p y)
    := paths_ind x (fun y q => (p x) @ (ap f q) = q @ (p y))
               (concat_p1 _ @ (concat_1p _)^) y q.


Definition path_sigma {A: Type} {FibA: Fibrant A}
           (P: A -> Type) {FibP: FibrantF P}
           {x x': A} {y: P x} {y': P x'}
           (p: x = x') (q: p # y = y')
  : (x; y) = (x'; y').
Proof.
  destruct_path p.
  destruct_path q.
  exact 1.
Defined.

Definition path_contr {A} {FibA: Fibrant A} {HA: Contr A} (x y : A)
  : x = y.
Proof.
  exact ((contr _ _ _)^ @ contr _ HA _).
Defined.



Definition transport_compose {A B} {FibA: Fibrant A} {FibB: Fibrant B}
           {x y: A} (P: B → Type) {FibP : FibrantF P}
           (f : A → B) (p : x = y) (z : P (f x))
  : transport (P o f) p z = transport P (ap f p) z.
Proof.
  destruct_path p.
  exact 1.
Defined.

Definition transport_const {A B} {FibA: Fibrant A} {FibB: Fibrant B}
           {x1 x2 : A} (p : x1 = x2) (y : B)
  : transport (fun x => B) p y = y.
Proof.
  refine (paths_ind x1 (fun x2 p => p # y = y) 1 x2 p).
Defined.

Definition transport_paths_r A {FibA: Fibrant A} {x y1 y2: A}
           (p : y1 = y2) (q : x = y1)
  : transport (λ y : A, x = y) p q = q @ p.
Proof.
  destruct_path p. 
  destruct_path q.
  exact 1.
Defined.

Definition transport_paths_Fl {A B} {FibA: Fibrant A} {FibB: Fibrant B} 
           (f: A → B) {x1 x2: A} {y: B} (p: x1 = x2) (q: f x1 = y)
  : transport (λ x : A, f x = y) p q = (ap f p)^ @ q.
Proof.
  destruct_path q.
  destruct_path p.
  exact 1.
Defined.

Definition transport_paths_Fr {A B} {FibA: Fibrant A} {FibB: Fibrant B} 
           (f: A → B) {x1 x2: A} {y: B} (p: x1 = x2) (q: y = f x1)
  : transport (λ x : A, y = f x) p q = q @ ap f p.
Proof.
  destruct_path p. cbn.
  exact (concat_p1 _)^.
Defined.



Definition concat_Ep {A} {FibA: Fibrant A} {x y z : A} (e: x ≡ y) (p: y = z) : x = z
  := Etransport (λ u, u = z) e^E p.

Definition concat_EVp {A} {FibA: Fibrant A} {x y z : A} (e: y ≡ x) (p: y = z) : x = z
  := Etransport (λ u, u = z) e p.

Definition concat_pE {A} {FibA: Fibrant A} {x y z : A} (p: x = y) (e: y ≡ z) : x = z
  := Etransport (λ v, x = v) e p.

Definition concat_Ep_ETP {A} {FibA: Fibrant A} {x y z: A} (e: x ≡ y :> A) (p: y ≡ z)
  : concat_Ep e (Eq_to_paths p) ≡ Eq_to_paths (e E@ p).
Proof.
  now destruct e, p.
Defined.

Definition concat_EVp_ETP {A} {FibA: Fibrant A} {x y z: A} (e: y ≡ x :> A) (p: y ≡ z)
  : concat_EVp e (Eq_to_paths p) ≡ Eq_to_paths (e^E E@ p).
Proof.
  now destruct e, p.
Defined.

Definition concat_pE_ETP {A} {FibA: Fibrant A} {x y z: A} (p: x ≡ y) (e: y ≡ z)
  : concat_pE (Eq_to_paths p) e ≡ Eq_to_paths (p E@ e).
Proof.
  now destruct e, p.
Defined.

Definition ap_concat_Ep {A B} {FibA: Fibrant A}  {FibB: Fibrant B} (f: A -> B)
           {x y z: A} (e: x ≡ y :> A) (p: y = z)
  : ap f (concat_Ep e p) ≡ concat_Ep (Eap f e) (ap f p).
Proof.
    now destruct e.
Defined.

Definition ap_concat_EVp {A B} {FibA: Fibrant A} {FibB: Fibrant B} (f: A -> B)
           {x y z: A} (e: y ≡ x) (p: y = z)
  : ap f (concat_EVp e p) ≡ concat_EVp (Eap f e) (ap f p).
Proof.
    now destruct e.
Defined.

Definition ap_concat_pE {A B} {FibA: Fibrant A} {FibB: Fibrant B} (f: A -> B)
           {x y z: A} (p: x = y) (e: y ≡ z)
  : ap f (concat_pE p e) ≡ concat_pE (ap f p) (Eap f e).
Proof.
    now destruct e.
Defined.
