Require Export HTS.Overture.

Open Scope type_scope.

(* ********* Strict Eq ********* *)
Delimit Scope eq_scope with eq.
Open Scope eq_scope.

Local Unset Elimination Schemes.

Inductive Eq {A: Type} (a: A) : A -> Type :=
  refl : Eq a a.

Arguments refl {A a} , [A] a.

Scheme Eq_ind := Induction for Eq Sort Type.
Arguments Eq_ind [A] a P f y e.
Scheme Eq_rec := Minimality for Eq Sort Type.
Arguments Eq_rec [A] a P f y e.

Notation "x ≡ y :> A"
  := (@Eq A x y) (at level 70, y at next level, no associativity) : type_scope.
Notation "x ≡ y"
  := (@Eq _ x y) (at level 70, no associativity) : type_scope.

Axiom Eq_UIP : forall {A: Type} {x y: A} (p q: x ≡ y), p ≡ q.

Lemma Eq_rew A a y P (X : P a) (H : a ≡ y :> A) : P y.
Proof. rewrite <- H. assumption. Defined.

Lemma Eq_rew_r A a y P (X : P y) (H : a ≡ y :> A) : P a.
Proof. rewrite H. assumption. Defined.

Bind Scope eq_scope with Eq.

Definition Einverse {A : Type} {x y : A} (p : x ≡ y) : y ≡ x.
  symmetry; assumption.
Defined.
Arguments Einverse {A x y} p : simpl nomatch.

Definition Econcat {A : Type} {x y z : A} (p : x ≡ y) (q : y ≡ z) : x ≡ z :=
  match p, q with refl, refl => refl end.
Arguments Econcat {A x y z} p q : simpl nomatch.

Notation "'E1'" := refl : eq_scope.
Notation "p E@ q" := (Econcat p%eq q%eq) (at level 20) : eq_scope.
Notation "p ^E" := (Einverse p%eq) (at level 3, format "p '^E'") : eq_scope.

Definition Etransport {A : Type} (P : A -> Type) {x y : A} (p : x ≡ y) (u : P x) : P y :=
  match p with refl => u end.
Arguments Etransport {A}%type_scope P {x y} p%eq_scope u : simpl nomatch.

Notation "p E# x"
  := (Etransport _ p x) (right associativity, at level 65, only parsing) : eq_scope.

Notation "f ≡≡ g" := (forall x, f x ≡ g x) (at level 70, no associativity) : type_scope.

Definition Eap {A B:Type} (f:A -> B) {x y:A} (p:x ≡ y) : f x ≡ f y
  := match p with refl => refl end.
Global Arguments Eap {A B}%type_scope f {x y} p%eq_scope.

Definition EapD10 {A} {B: A -> Type} {f g: forall x, B x} (h: f ≡ g)
  : f ≡≡ g
  := fun x => match h with refl => E1 end.
Global Arguments EapD10 {A%type_scope B} {f g} h%eq_scope _.

Definition Eap10 {A B} {f g: A -> B} (h: f ≡ g) : f ≡≡ g
  := EapD10 h.
Global Arguments Eap10 {A B}%type_scope {f g} h%eq_scope _.

Axiom eq_funext: forall {A: Type} {P : A -> Type} {f g : forall x : A, P x},
    f ≡≡ g -> f ≡ g.


Definition EapD {A:Type} {B:A->Type} (f:forall a:A, B a) {x y:A} (p:x≡y):
  p E# (f x) ≡ f y
  :=
  match p with refl => refl end.
Arguments EapD {A%type_scope B} f {x y} p%eq_scope : simpl nomatch.

Definition Etransport_Vp {A: Type} (P: A -> Type) {x y: A} (p: x ≡ y) (z: P x)
  : p^E E# p E# z ≡ z.
Proof.
  destruct p; reflexivity.
Defined.

Definition Etransport_compose {A B : Type} {x y : A} (P : B -> Type) (f : A -> B)
           (p : x ≡ y) (z : P (f x)) :
  Etransport (fun x0 : A => P (f x0)) p z ≡ Etransport P (Eap f p) z.
destruct p. reflexivity.
Defined.


Definition eq_sigma {A: Type} (P: A -> Type) {x x': A} {y: P x} {y': P x'}
           (p: x ≡ x') (q: p E# y ≡ y')
  : (x; y) ≡ (x'; y').
Proof.
  destruct p, q; reflexivity.
Defined.

Definition Etransport_sigma' {A B : Type} {C : A -> B -> Type}
           {x1 x2 : A} (p : x1 ≡ x2) yz
: Etransport (fun x => sigT (C x)) p yz ≡
  (yz.1 ; Etransport (fun x => C x yz.1) p yz.2).
Proof.
  destruct p. destruct yz. reflexivity.
Defined.

Definition pr1_eq {A : Type} `{P : A -> Type} {u v : sigT P} (p : u ≡ v)
  : u.1 ≡ v.1
  := Eap pr1 p.

Notation "p ..1E" := (pr1_eq p) (at level 3).

Definition pr2_eq {A : Type} `{P : A -> Type} {u v : sigT P} (p : u ≡ v)
  : p..1E E# u.2 ≡ v.2
  := (Etransport_compose P pr1 p u.2)^E
     E@ (@EapD { x & P x} _ pr2 _ _ p).

Notation "p ..2E" := (pr2_eq p) (at level 3).



(* inverse and composition *)
Definition Einv_V {A} {x y : A} (p : x ≡ y)
  : (p^E)^E ≡ p.
  now destruct p.
Defined.

Definition Econcat_Vp {A} {x y : A} (p : x ≡ y)
  : p^E E@ p ≡ E1.
Proof.
  now destruct p.
Defined.

Definition Econcat_pV {A} {x y : A} (p : x ≡ y)
  : p E@ p^E ≡ E1.
Proof.
  now destruct p.
Defined.

Definition Econcat_1p {A} {x y : A} (p : x ≡ y)
  : E1 E@ p ≡ p.
Proof.
  now destruct p.
Defined.

Definition Econcat_p1 {A} {x y : A} (p : x ≡ y)
  : p E@ E1 ≡ p.
Proof.
  now destruct p.
Defined.

Definition Econcat_p_pp {A} {x y z t : A} (p : x ≡ y)
           (q : y ≡ z) (r : z ≡ t)
  : p E@ (q E@ r) ≡ (p E@ q) E@ r.
  now destruct p, q, r.
Defined.

Definition Econcat_pp_p {A} {x y z t : A} (p : x ≡ y)
           (q : y ≡ z) (r : z ≡ t)
  : (p E@ q) E@ r ≡ p E@ (q E@ r).
  now destruct p, q, r.
Defined.


(* Eap *)
Definition Eap_pp {A B} (f : A → B) {x y z} (p : x ≡ y) (q : y ≡ z)
  : Eap f (p E@ q) ≡ Eap f p E@ Eap f q.
  now destruct p, q.
Defined.

Definition Eap_V {A B} (f : A → B) {x y : A} (p : x ≡ y)
  : Eap f p^E ≡ (Eap f p)^E.
Proof.
  now destruct p.
Defined.

Definition Eap_const {A B} {x y : A} (p : x ≡ y) (z : B)
  : Eap (fun _ => z) p ≡ E1.
Proof.
  now destruct p.
Defined.

Definition Eap_compose {A B C} (f : A → B) (g : B → C) {x y}
           (p : x ≡ y)
  : Eap (g o f) p ≡ Eap g (Eap f p).
  now destruct p.
Defined.
