# HTS-Coq

This folder contains the implementation of a Two Level Type Theory like Homotopy Type System in Coq.

We use:

  - typeclasses to infer fibrancy conditions
  
  - a Private Inductive Type to define path equality so that fibrancy is always required

The important part is the definition of path equality in `Path_eq.v`.

See also there for:

  - a comment explaining the plugin *myrewrite* defined in folder `src`
	
  - the `destruct_path` tactic try to emulate the `destruct` tactic on a path equality


## Compilation

To compile run:

```
	coq_makefile -f _CoqProject -o Makefile
	make
```

Tested with Coq 8.5 and Coq 8.8.
