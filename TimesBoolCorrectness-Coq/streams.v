Require Import List.
Require Import Peano_dec.
Require Import Compare_dec.
Require Import Lt Le Gt.
Require Import Omega.

Require Import Streams.


CoFixpoint stream_corec (A : Type) (S : Type) (M0 : S -> A) (M1 : S -> S) (N : S)
  : Stream A
  := Cons (M0 N) (stream_corec A S M0 M1 (M1 N)).

Definition hd_b (A : Type) (P0 P1 : Stream A) (H : EqSt P0 P1)
  : hd P0 = hd P1
  := match H with
     | eqst _ _ h t => h
     end.

Definition tl_b (A : Type) (P0 P1 : Stream A) (H : EqSt P0 P1)
  : EqSt (tl P0) (tl P1)
  := match H with
     | eqst _ _ h t => t
     end.

CoFixpoint EqSt_corec (A : Type) (S : Stream A -> Stream A -> Type)
           (M0 : forall (s0 s1 : Stream A), S s0 s1 -> hd s0 = hd s1)
           (M1 : forall (s0 s1 : Stream A), S s0 s1 -> S (tl s0) (tl s1))
           (P0 P1 : Stream A) (N : S P0 P1)
  : EqSt P0 P1
  := eqst _ _ (M0 P0 P1 N) (EqSt_corec A S M0 M1 (tl P0) (tl P1) (M1 P0 P1 N)).

CoFixpoint EqSt_corec' (A : Type) (S : Stream A -> Stream A -> Type)
           (M0 : forall (s0 s1 : Stream A), S s0 s1 -> hd s0 = hd s1)
           (M1 : forall (s0 s1 : Stream A), S s0 s1 -> S (tl s0) (tl s1))
           (P0 P1 : Stream A) (N : S P0 P1)
  : EqSt P0 P1.
simple refine (eqst _ _ (M0 P0 P1 N) _).
simple refine (EqSt_corec A (fun s0 s1 => S (tl s0) (tl s1)) _ _ _ _ _); cbn.

(fun s0 s1 s => tl_b (M0 (tl s0) (tl s1) s))
  (fun s0 s1 s => tl_b (M1 s0 s1 s))
  (tl P0) (tl P1) (M1 P0 P1 N)).