open import Data.Bool renaming (Bool to 𝔹)


data TYPE₀ : Set
El₀ : TYPE₀ → Set

data TYPE₀ where
  π₀₀  : (a : TYPE₀) → (b : El₀ a → TYPE₀) → TYPE₀
  b₀ : TYPE₀

El₀ (π₀₀ a b) = (x : El₀ a) → El₀ (b x)
El₀ b₀ = 𝔹


data TYPE₁ : Set
El₁ : TYPE₁ → Set

data TYPE₁ where
  π₀₁  : (a : TYPE₀) → (b : El₀ a → TYPE₁) → TYPE₁
  π₁₀  : (a : TYPE₁) → (b : El₁ a → TYPE₀) → TYPE₁
  π₁₁  : (a : TYPE₁) → (b : El₁ a → TYPE₁) → TYPE₁
  b₁ : TYPE₁
  u₀ : TYPE₁

El₁ (π₀₁ a b) = (x : El₀ a) → El₁ (b x)
El₁ (π₁₀ a b) = (x : El₁ a) → El₀ (b x)
El₁ (π₁₁ a b) = (x : El₁ a) → El₁ (b x)
El₁ b₁ = 𝔹
El₁ u₀ = TYPE₀



TYPE-rec₀ : ∀{ℓ}(P : TYPE₀ → Set ℓ)(b₀' : P b₀)(π₀₀' : ∀ (a : TYPE₀)(b : El₀ a → TYPE₀) → P a → (∀ x → P (b x)) → P (π₀₀ a b))(a : TYPE₀) → P a
TYPE-rec₀ P b₀' π₀₀' (π₀₀ a b) = π₀₀' a b (TYPE-rec₀ P b₀' π₀₀' a) (λ x → TYPE-rec₀ P b₀' π₀₀' (b x))
TYPE-rec₀ P b₀' π₀₀' b₀ = b₀'

TYPE-rec₁ : ∀{ℓ}(P : TYPE₁ → Set ℓ)(b₁' : P b₁)(u₀' : P u₀)
             (π₀₁' : ∀ (a : TYPE₀)(b : El₀ a → TYPE₁) → (∀ x → P (b x)) → P (π₀₁ a b))
             (π₁₀' : ∀ (a : TYPE₁)(b : El₁ a → TYPE₀) → P a → P (π₁₀ a b))
             (π₁₁' : ∀ (a : TYPE₁)(b : El₁ a → TYPE₁) → P a → (∀ x → P (b x)) → P (π₁₁ a b))
             (a : TYPE₁) → P a
TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' (π₀₁ a b) = π₀₁' a b (λ x → TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' (b x))
TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' (π₁₀ a b) = π₁₀' a b (TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' a)
TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' (π₁₁ a b) = π₁₁' a b (TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' a) (λ x → TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' (b x))
TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' b₁ = b₁'
TYPE-rec₁ P b₁' u₀' π₀₁' π₁₀' π₁₁' u₀ = u₀'
