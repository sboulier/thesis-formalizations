# SetoidUniverse-Agda

Formalization of a universe closed under Π types for the setoid model.

- `U.agda` contains the definition described in my thesis

- `Un.agda` uses type-in-type to construct a universe satisfying type-in-type
