module U where

open import lib

---------------------------------
--  Signatures
---------------------------------

-- U is the set of codes of all types considered
-- It is defined as an inductive-recursive type
data U₀ : Set
-- U₀ with ≡u is itslef a Setoid
_≡ᵤ_ : U₀ → U₀ → Set
≡ᵤrefl : (a : U₀) → a ≡ᵤ a
≡ᵤsym : {a a' : U₀} → .(a ≡ᵤ a') → a' ≡ᵤ a
≡ᵤtrans : {a b c : U₀} → .(a ≡ᵤ b) → .(b ≡ᵤ c) → a ≡ᵤ c

-- El₀ : U₀ → Setoid
El₀ : U₀ → Set
_~_ : {a : U₀} → El₀ a → El₀ a → Set
~refl : {a : U₀} → (x : El₀ a) → x ~ x
~sym : {a : U₀} → {x y : El₀ a} → .(x ~ y) → y ~ x -- We emulate the fact that _~_ takes its values in a definitional Prop using irrelevant arguments
~trans : {a : U₀} → {x y z : El₀ a} → .(x ~ y) → .(y ~ z) → x ~ z


-- subst p is a Setoid map El₀ a ⇨ El₀ a'
subst  : {a a' : U₀} → .(p : a ≡ᵤ a') → El₀ a → El₀ a'  -- coe
subst* : {a a' : U₀} → .(p : a ≡ᵤ a') → {x y : El₀ a} → x ~ y → subst p x ~ subst p y

-- El₀ is a Setoid family indexed by U₀
refl* : {a : U₀} → (x : El₀ a) → subst (≡ᵤrefl a) x ~ x 
trans* : {a₀ a₁ a₂ : U₀} → (p : a₀ ≡ᵤ a₁) (q : a₁ ≡ᵤ a₂) → (x : El₀ a₀) → subst q (subst p x) ~ subst (≡ᵤtrans p q) x
sym* : {a₀ a₁ : U₀} → (p : a₀ ≡ᵤ a₁) → (x : El₀ a₀) → subst (≡ᵤsym p) (subst p x) ~ x




---------------------------------
--  Definitions
---------------------------------

data U₀ where
  n : U₀
  π : (a : U₀) → (b : El₀ a → U₀) → (b₁ : ∀ {x y : El₀ a} (p : x ~ y) → b x ≡ᵤ b y) → U₀

El₀ n = ℕ
El₀ (π a b b₁) = Σ (∀ x → El₀ (b x)) λ f → ∀ {x y : El₀ a} (p : x ~ y) → subst (b₁ p) (f x) ~ f y
-- El₀ (π a b b₁) = ∀ x → El₀ (b x)

n ≡ᵤ n = ⊤
π a b b₁ ≡ᵤ π a' b' b₁' = Σ (a ≡ᵤ a') (λ p → ∀ x → b x ≡ᵤ b' (subst p x))
_ ≡ᵤ _ = ⊥
≡ᵤrefl n = tt
≡ᵤrefl (π a b b₁) = (≡ᵤrefl a) , (λ x → ≡ᵤsym (b₁ (refl* x)))
≡ᵤsym {n} {n} _ = tt
≡ᵤsym {n} {π a' b b₁} ()
≡ᵤsym {π a b b₁} {n} ()
≡ᵤsym {π a b b₁} {π a' b' b₁'} (p₁ , p₂) = (≡ᵤsym p₁) , (λ x → ≡ᵤsym (≡ᵤtrans (p₂ _) (b₁' (sym* _ x))))
≡ᵤtrans {n} {n} {n} _ _ = tt
≡ᵤtrans {n} {n} {π a b b₁} _ ()
≡ᵤtrans {n} {π a' b b₁} ()
≡ᵤtrans {π a b b₁} {n} ()
≡ᵤtrans {π a b b₁} {π a' b₂ b₃} {n} _ ()
≡ᵤtrans {π a b b₁} {π a' b' b₁'} {π a'' b'' b₁''} (p₁ , p₂) (q₁ , q₂) =
  ≡ᵤtrans p₁ q₁ , λ x → ≡ᵤtrans (p₂ _) (≡ᵤtrans (q₂ _) (b₁'' (trans* _ _ _)))

_~_ {n} = _≡ℕ_
_~_ {π a b b₁} (f , f') (g , g') = ∀ (x y : El₀ a) (p : x ~ y) → subst (b₁ p) (f x) ~ g y

~refl {n} = ℕrefl
~refl {π a b b₁} (f , f') x y p = f' p
~sym {n} {x} {y} = ℕsym {x} {y}
~sym {π a b b₁} q x y p = ~sym {b y} (~trans {b y} (~sym {b y} (sym* (≡ᵤsym (b₁ p)) _)) (subst* (b₁ p) (q y x (~sym {a} p))))
~trans {n} {x} = ℕtrans {x}
~trans {π a b b₁} q q' x y p = ~trans {b y} (subst* (b₁ p) (~trans {b x} (~sym {b x} (refl* {b x} _)) (q x x (~refl x)))) (q' _ _ p)



-- subst  : {a a' : U₀} → .(p : a ≡u a') → El₀ a → El₀ a'
subst {n} {n} p x = x
subst {n} {π a' b' b₁'} () x
subst {π a b b₁} {n} () x
subst {π a b b₁} {π a' b' b₁'} (p₁ , p₂) (f , f₁) =
  (λ x → subst (≡ᵤtrans (p₂ (subst (≡ᵤsym p₁) x)) (b₁' (sym* (≡ᵤsym p₁) x))) (f (subst (≡ᵤsym p₁) x)))
  , λ {x} {y} p → ~trans {b' y} (~trans {b' y}
                         (trans* _ (b₁' p) (f (subst (≡ᵤsym p₁) x)))
                         (~sym {b' y} (trans* (b₁ (subst* (≡ᵤsym p₁) p)) (≡ᵤtrans (p₂ (subst (≡ᵤsym p₁) y)) (b₁' (sym* (≡ᵤsym p₁) y))) _)))
                         (subst* (≡ᵤtrans (p₂ (subst (≡ᵤsym p₁) y)) (b₁' (sym* (≡ᵤsym p₁) y))) (f₁ (subst* (≡ᵤsym p₁) p)))


-- subst* : {a a' : U₀} → .(p : a ≡ᵤ a') → {x y : El₀ a} → x ~ y → subst p x ~ subst p y
subst* {n} {n} p q = q
subst* {n} {π a' b b₁} ()
subst* {π a b b₁} {n} ()
subst* {π a b b₁} {π a' b' b₁'} (p₁ , p₂) {f , f₁} {g , g₁} q x y p = ~trans {b' y} (~trans {b' y}
                (trans* _ (b₁' p) (f (subst (≡ᵤsym p₁) x)))
                (~sym {b' y} (trans* (b₁ (subst* (≡ᵤsym p₁) p)) (≡ᵤtrans (p₂ (subst (≡ᵤsym p₁) y)) (b₁' (sym* (≡ᵤsym p₁) y))) _)))
                (subst* (≡ᵤtrans (p₂ (subst (≡ᵤsym p₁) y)) (b₁' (sym* (≡ᵤsym (p₁)) y))) (q (subst (≡ᵤsym p₁) x) (subst (≡ᵤsym p₁) y) (subst* (≡ᵤsym p₁) p)))


-- refl* : {a : U₀} → (x : El₀ a) → subst (~refl a) x ~ x 
refl* {n} = ℕrefl
refl* {π a b b₁} (f , f₁) x y p = ~trans {b y} (trans* _ (b₁ p) (f (subst (≡ᵤrefl a) x))) (f₁ (~trans {a} (refl* x) p))
      

-- trans* : {a₀ a₁ a₂ : U₀} → (p : a₀ ≡ᵤ a₁) (q : a₁ ≡ᵤ a₂) → (x : El₀ a₀) → subst q (subst p x) ~ subst (~trans p q) x
trans* {n} {n} {n} p q x = ~refl x
trans* {n} {n} {π a b b₁} p ()
trans* {n} {π a₁ b b₁} {_} ()
trans* {π a b b₁} {n} {a₂} ()
trans* {π a b b₁} {π a' b' b₁'} {n} p ()
trans* {π a b b₁} {π a' b' b₁'} {π a'' b'' b₁''} (p₁ , p₂) (p₁' , p₂') (f , f₁) x y p =
  ~trans {b'' y} (~trans {b'' y} (~trans {b'' y}
              (trans* (≡ᵤtrans (p₂' (subst (≡ᵤsym p₁') x)) (b₁'' (sym* (≡ᵤsym p₁') x))) (b₁'' p) _)
              (trans* (≡ᵤtrans (p₂ (subst (≡ᵤsym p₁) (subst (≡ᵤsym p₁') x))) (b₁' (sym* (≡ᵤsym p₁) (subst (≡ᵤsym p₁') x))))
                      (≡ᵤtrans (≡ᵤtrans (p₂' (subst (≡ᵤsym p₁') x)) (b₁'' (sym* (≡ᵤsym p₁') x))) (b₁'' p)) _))
              (~sym {b'' y} (trans* (b₁ (~trans {a} (trans* (≡ᵤsym p₁') (≡ᵤsym p₁) x) (subst* (≡ᵤsym (≡ᵤtrans p₁ p₁')) p)))
                                    (≡ᵤtrans (≡ᵤtrans (p₂ (subst (≡ᵤsym (≡ᵤtrans p₁ p₁')) y)) (≡ᵤtrans (p₂' (subst p₁ (subst (≡ᵤsym (≡ᵤtrans p₁ p₁')) y)))
                                             (b₁'' (trans* p₁ p₁' (subst (≡ᵤsym (≡ᵤtrans p₁ p₁')) y))))) (b₁'' (sym* (≡ᵤsym (≡ᵤtrans p₁ p₁')) y))) _)))
              ((subst* (≡ᵤtrans (≡ᵤtrans (p₂ (subst (≡ᵤsym (≡ᵤtrans p₁ p₁')) y)) (≡ᵤtrans (p₂' (subst p₁ (subst (≡ᵤsym (≡ᵤtrans p₁ p₁')) y)))
                                    (b₁'' (trans* p₁ p₁' (subst (≡ᵤsym (≡ᵤtrans p₁ p₁')) y))))) (b₁'' (sym* (≡ᵤsym (≡ᵤtrans p₁ p₁')) y)))
                                    (f₁ (~trans {a} (trans* (≡ᵤsym p₁') (≡ᵤsym p₁) x) (subst* (≡ᵤsym (≡ᵤtrans p₁ p₁')) p)))))

-- sym* can be defined in with refl* and trans*
sym* {a} {a'} p x = ~trans {a} (trans* p (≡ᵤsym {a} p) x) (refl* x)


data U₁ : Set
-- U₁ with ≡ᵤ₁ is itself a Setoid
_≡ᵤ₁_ : U₁ → U₁ → Set
≡ᵤ₁refl : (a : U₁) → a ≡ᵤ₁ a
≡ᵤ₁sym : {a a' : U₁} → .(a ≡ᵤ₁ a') → a' ≡ᵤ₁ a
≡ᵤ₁trans : {a b c : U₁} → .(a ≡ᵤ₁ b) → .(b ≡ᵤ₁ c) → a ≡ᵤ₁ c

-- El₁ : U₁ → Setoid
El₁ : U₁ → Set
_~₁_ : {a : U₁} → El₁ a → El₁ a → Set
~refl₁ : {a : U₁} → (x : El₁ a) → x ~₁ x
~sym₁ : {a : U₁} → {x y : El₁ a} → .(x ~₁ y) → y ~₁ x
~trans₁ : {a : U₁} → {x y z : El₁ a} → .(x ~₁ y) → .(y ~₁ z) → x ~₁ z


-- subst₁ p is a Setoid map El₁ a ⇨ El₁ a'
subst₁  : {a a' : U₁} → .(p : a ≡ᵤ₁ a') → El₁ a → El₁ a'
subst*₁ : {a a' : U₁} → (p : a ≡ᵤ₁ a') → {x y : El₁ a} → x ~₁ y → subst₁ p x ~₁ subst₁ p y

data U₁ where
  ↑₀ : U₀ → U₁
  u₀ : U₁
  π₀₁ : (a : U₀) → (b : El₀ a → U₁) → (b₁ : ∀ {x y : El₀ a} (p : x ~ y) → b x ≡ᵤ₁ b y) → U₁
  π₁₀ : (a : U₁) → (b : El₁ a → U₀) → (b₁ : ∀ {x y : El₁ a} (p : x ~₁ y) → b x ≡ᵤ b y) → U₁
  π₁₁ : (a : U₁) → (b : El₁ a → U₁) → (b₁ : ∀ {x y : El₁ a} (p : x ~₁ y) → b x ≡ᵤ₁ b y) → U₁

El₁ (↑₀ c) = El₀ c
El₁ u₀ = U₀
El₁ (π₀₁ a b b₁) = Σ (∀ x → El₁ (b x)) λ f → ∀ {x y : El₀ a} (p : x ~ y) → _~₁_ {b y} (subst₁ (b₁ p) (f x)) (f y)
El₁ (π₁₀ a b b₁) = Σ (∀ x → El₀ (b x)) λ f → ∀ {x y : El₁ a} (p : x ~₁ y) → _~_ {b y} (subst (b₁ p) (f x)) (f y)
El₁ (π₁₁ a b b₁) = Σ (∀ x → El₁ (b x)) λ f → ∀ {x y : El₁ a} (p : x ~₁ y) → _~₁_ {b y} (subst₁ (b₁ p) (f x)) (f y)

↑₀ c ≡ᵤ₁ ↑₀ c' = c ≡ᵤ c'
u₀ ≡ᵤ₁ u₀ = ⊤
π₀₁ a b b₁ ≡ᵤ₁ π₀₁ a' b' b₁' = Σ (a ≡ᵤ a') (λ p → ∀ x → b x ≡ᵤ₁ b' (subst p x))
π₁₀ a b b₁ ≡ᵤ₁ π₁₀ a' b' b₁' = Σ (a ≡ᵤ₁ a') (λ p → ∀ x → b x ≡ᵤ b' (subst₁ {a} p x)) -- ??
π₁₁ a b b₁ ≡ᵤ₁ π₁₁ a' b' b₁' = Σ (a ≡ᵤ₁ a') (λ p → ∀ x → b x ≡ᵤ₁ b' (subst₁ {a} p x))  -- ??
_ ≡ᵤ₁ _ = ⊥


-- El₁ is a Setoid family indexed by U₁
refl*₁ : {a : U₁} → (x : El₁ a) → _~₁_ {a} (subst₁ {a} (≡ᵤ₁refl a) x) x 
trans*₁ : {a₀ a₁ a₂ : U₁} → (p : a₀ ≡ᵤ₁ a₁) (q : a₁ ≡ᵤ₁ a₂) → (x : El₁ a₀) → _~₁_ {a₂} (subst₁ {a₁} q (subst₁ {a₀} p x)) (subst₁ {a₀} (≡ᵤ₁trans {a₀} p q) x)
sym*₁ : {a₀ a₁ : U₁} → (p : a₀ ≡ᵤ₁ a₁) → (x : El₁ a₀) → _~₁_ {a₀} (subst₁ {a₁} (≡ᵤ₁sym {a₀} p) (subst₁ {a₀} p x)) x

_~₁_ {↑₀ c} = _~_ {c}
_~₁_ {u₀} = _≡ᵤ_
_~₁_ {π₀₁ a b b₁} (f , f₁) (g , g₁) = ∀ (x y : El₀ a) (p : x ~ y) → _~₁_ {b y} (subst₁ {b x} (b₁ p) (f x)) (g y)
_~₁_ {π₁₀ a b b₁} (f , f₁) (g , g₁) = ∀ (x y : El₁ a) (p : _~₁_ {a} x y) → subst (b₁ p) (f x) ~ g y
_~₁_ {π₁₁ a b b₁} (f , f₁) (g , g₁) = ∀ (x y : El₁ a) (p : _~₁_ {a} x y) → _~₁_ {b y} (subst₁ {b x} (b₁ p) (f x)) (g y)

≡ᵤ₁refl (↑₀ x) = ≡ᵤrefl x
≡ᵤ₁refl u₀ = tt
≡ᵤ₁refl (π₀₁ a b b₁) = (≡ᵤrefl a)  , (λ x → ≡ᵤ₁sym {_} {b x} (b₁ (refl* x)))
≡ᵤ₁refl (π₁₀ a b b₁) = (≡ᵤ₁refl a) , (λ x → ≡ᵤsym {_} {b x} (b₁ (refl*₁ {a} x)))
≡ᵤ₁refl (π₁₁ a b b₁) = (≡ᵤ₁refl a) , (λ x → ≡ᵤ₁sym {_} {b x} (b₁ (refl*₁ {a} x)))
≡ᵤ₁sym {↑₀ x} {↑₀ x₁} = ≡ᵤsym
≡ᵤ₁sym {↑₀ x} {u₀} ()
≡ᵤ₁sym {↑₀ x} {π₀₁ a b b₁} ()
≡ᵤ₁sym {↑₀ x} {π₁₀ a' b b₁} ()
≡ᵤ₁sym {↑₀ x} {π₁₁ a' b b₁} ()
≡ᵤ₁sym {u₀} {↑₀ x} ()
≡ᵤ₁sym {u₀} {u₀} _ = tt
≡ᵤ₁sym {u₀} {π₀₁ a b b₁} ()
≡ᵤ₁sym {u₀} {π₁₀ a' b b₁} ()
≡ᵤ₁sym {u₀} {π₁₁ a' b b₁} ()
≡ᵤ₁sym {π₀₁ a b b₁} {↑₀ x} ()
≡ᵤ₁sym {π₀₁ a b b₁} {u₀} ()
≡ᵤ₁sym {π₀₁ a b b₁} {π₀₁ a' b' b₁'} (p₁ , p₂) = (≡ᵤsym p₁) , (λ x → ≡ᵤ₁sym {_} {b' x} (≡ᵤ₁trans {b (subst (≡ᵤsym p₁) x)} (p₂ _) (b₁' (sym* (≡ᵤsym p₁) x))))
≡ᵤ₁sym {π₀₁ a b b₁} {π₁₀ a' b₂ b₃} ()
≡ᵤ₁sym {π₀₁ a b b₁} {π₁₁ a' b₂ b₃} ()
≡ᵤ₁sym {π₁₀ a b b₁} {↑₀ x} ()
≡ᵤ₁sym {π₁₀ a b b₁} {u₀} ()
≡ᵤ₁sym {π₁₀ a b b₁} {π₀₁ a₁ b₂ b₃} ()
≡ᵤ₁sym {π₁₀ a b b₁} {π₁₀ a' b' b₁'} (p₁ , p₂) = (≡ᵤ₁sym {a} p₁) , (λ x → ≡ᵤsym (≡ᵤtrans (p₂ _) (b₁' (sym*₁ {a'}{a}(≡ᵤ₁sym {a} p₁) x))))
≡ᵤ₁sym {π₁₀ a b b₁} {π₁₁ a' b₂ b₃} ()
≡ᵤ₁sym {π₁₁ a b b₁} {↑₀ x} ()
≡ᵤ₁sym {π₁₁ a b b₁} {u₀} ()
≡ᵤ₁sym {π₁₁ a b b₁} {π₀₁ a₁ b₂ b₃} ()
≡ᵤ₁sym {π₁₁ a b b₁} {π₁₀ a' b₂ b₃} ()
≡ᵤ₁sym {π₁₁ a b b₁} {π₁₁ a' b' b₁'} (p₁ , p₂) = (≡ᵤ₁sym {a} p₁) ,
                                                (λ x → ≡ᵤ₁sym {_} {b' x} (≡ᵤ₁trans {b (subst₁ {a'} (≡ᵤ₁sym {a} p₁) x)} (p₂ _) (b₁' (sym*₁ {a'}{a} (≡ᵤ₁sym {a} p₁) x))))
≡ᵤ₁trans {↑₀ x} {↑₀ x₁} {↑₀ x₂} = ≡ᵤtrans
≡ᵤ₁trans {↑₀ x} {↑₀ x₁} {u₀} _ ()
≡ᵤ₁trans {↑₀ x} {↑₀ x₁} {π₀₁ a b b₁} _ ()
≡ᵤ₁trans {↑₀ x} {↑₀ x₁} {π₁₀ c b b₁} _ ()
≡ᵤ₁trans {↑₀ x} {↑₀ x₁} {π₁₁ c b b₁} _ ()
≡ᵤ₁trans {↑₀ x} {u₀} {c} ()
≡ᵤ₁trans {↑₀ x} {π₀₁ a b b₁} {c} ()
≡ᵤ₁trans {↑₀ x} {π₁₀ b b₁ b₂} {c} ()
≡ᵤ₁trans {↑₀ x} {π₁₁ b b₁ b₂} {c} ()
≡ᵤ₁trans {u₀} {↑₀ x} {c} ()
≡ᵤ₁trans {u₀} {u₀} {↑₀ x} _ ()
≡ᵤ₁trans {u₀} {u₀} {u₀} _ _ = tt
≡ᵤ₁trans {u₀} {u₀} {π₀₁ a b b₁} _ ()
≡ᵤ₁trans {u₀} {u₀} {π₁₀ c b b₁} _ ()
≡ᵤ₁trans {u₀} {u₀} {π₁₁ c b b₁} _ ()
≡ᵤ₁trans {u₀} {π₀₁ a b b₁} {c} ()
≡ᵤ₁trans {u₀} {π₁₀ b b₁ b₂} {c} ()
≡ᵤ₁trans {u₀} {π₁₁ b b₁ b₂} {c} ()

≡ᵤ₁trans {π₀₁ a b₁ b₂} {↑₀ x} {c} ()
≡ᵤ₁trans {π₀₁ a b₁ b₂} {u₀} {c} ()
≡ᵤ₁trans {π₀₁ a b₁ b₂} {π₀₁ a₁ b b₃} {↑₀ x} _ ()
≡ᵤ₁trans {π₀₁ a b₁ b₂} {π₀₁ a₁ b b₃} {u₀} _ ()
≡ᵤ₁trans {π₀₁ a b₁ b₂} {π₀₁ a' b' b₁'} {π₀₁ a'' b'' b₁''} (p₁ , p₂) (q₁ , q₂) =
  ≡ᵤtrans p₁ q₁ , λ x → ≡ᵤ₁trans {b₁ x} (p₂ x) (≡ᵤ₁trans {b' _} (q₂ _) (b₁'' (trans* p₁ q₁ x)))
≡ᵤ₁trans {π₀₁ a b₁ b₂} {π₀₁ a₁ b b₃} {π₁₀ c b₄ b₅} _ ()
≡ᵤ₁trans {π₀₁ a b₁ b₂} {π₀₁ a₁ b b₃} {π₁₁ c b₄ b₅} _ ()
≡ᵤ₁trans {π₀₁ a b₁ b₂} {π₁₀ b b₃ b₄} {c} ()
≡ᵤ₁trans {π₀₁ a b₁ b₂} {π₁₁ b b₃ b₄} {c} ()
≡ᵤ₁trans {π₁₀ a b₁ b₂} {↑₀ x} {c} ()
≡ᵤ₁trans {π₁₀ a b₁ b₂} {u₀} {c} ()
≡ᵤ₁trans {π₁₀ a b₁ b₂} {π₁₀ a₁ b b₃} {↑₀ x} _ ()
≡ᵤ₁trans {π₁₀ a b₁ b₂} {π₁₀ a₁ b b₃} {u₀} _ ()
≡ᵤ₁trans {π₁₀ a b₁ b₂} {π₁₀ a' b' b₁'} {π₁₀ a'' b'' b₁''} (p₁ , p₂) (q₁ , q₂) =
  ≡ᵤ₁trans {a} p₁ q₁ , λ x → ≡ᵤtrans (p₂ x) (≡ᵤtrans (q₂ _) (b₁'' (trans*₁ {a} p₁ q₁ x)))
≡ᵤ₁trans {π₁₀ a b₁ b₂} {π₁₀ a₁ b b₃} {π₀₁ c b₄ b₅} _ ()
≡ᵤ₁trans {π₁₀ a b₁ b₂} {π₁₀ a₁ b b₃} {π₁₁ c b₄ b₅} _ ()
≡ᵤ₁trans {π₁₀ a b₁ b₂} {π₀₁ b b₃ b₄} {c} ()
≡ᵤ₁trans {π₁₀ a b₁ b₂} {π₁₁ b b₃ b₄} {c} ()
≡ᵤ₁trans {π₁₁ a b₁ b₂} {↑₀ x} {c} ()
≡ᵤ₁trans {π₁₁ a b₁ b₂} {u₀} {c} ()
≡ᵤ₁trans {π₁₁ a b₁ b₂} {π₁₁ a₁ b b₃} {↑₀ x} _ ()
≡ᵤ₁trans {π₁₁ a b₁ b₂} {π₁₁ a₁ b b₃} {u₀} _ ()
≡ᵤ₁trans {π₁₁ a b₁ b₂} {π₁₁ a' b' b₁'} {π₁₁ a'' b'' b₁''} (p₁ , p₂) (q₁ , q₂) =
  ≡ᵤ₁trans {a} p₁ q₁ , λ x → ≡ᵤ₁trans {b₁ x} (p₂ x) (≡ᵤ₁trans {b' _} (q₂ _) (b₁'' (trans*₁ {a} p₁ q₁ x)))
≡ᵤ₁trans {π₁₁ a b₁ b₂} {π₁₁ a₁ b b₃} {π₀₁ c b₄ b₅} _ ()
≡ᵤ₁trans {π₁₁ a b₁ b₂} {π₁₁ a₁ b b₃} {π₁₀ c b₄ b₅} _ ()
≡ᵤ₁trans {π₁₁ a b₁ b₂} {π₀₁ b b₃ b₄} {c} ()
≡ᵤ₁trans {π₁₁ a b₁ b₂} {π₁₀ b b₃ b₄} {c} ()


~refl₁ {↑₀ x} = ~refl
~refl₁ {u₀} = ≡ᵤrefl
~refl₁ {π₀₁ a b b₁}  (f , f') x y p = f' p
~refl₁ {π₁₀ a b b₁}  (f , f') x y p = f' p
~refl₁ {π₁₁ a b b₁}  (f , f') x y p = f' p
~sym₁ = {!!}
~trans₁ = {!!}

subst₁ = {!!}
subst*₁ = {!!}

refl*₁ = {!!}
trans*₁ = {!!}
sym*₁ = {!!}
