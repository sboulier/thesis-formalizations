{-# OPTIONS  --no-positivity-check --no-termination-check --show-irrelevant #-}
-- We use type in type and we interpret type in type

module Un where

open import lib


---------------------------------
--  Signatures
---------------------------------

-- U is the set of codes of all types considered
-- It is defined as an inductive-recursive type
data U : Set
-- U with ≡u is itslef a Setoid
_≡ᵤ_ : U → U → Set

-- El : U → Setoid
El : U → Set
_~_ : {a : U} → El a → El a → Set
≡ᵤrefl : {a : U} → (x : El a) → x ~ x
≡ᵤsym : {a : U} → {x y : El a} → .(x ~ y) → y ~ x    -- We emulate the fact that _~_ takes its values in a definitional Prop using irrelevant arguments
≡ᵤtrans : {a : U} → {x y z : El a} → .(x ~ y) → .(y ~ z) → x ~ z


-- subst p is a Setoid map El a ⇨ El a'
subst  : {a a' : U} → .(p : a ≡ᵤ a') → El a → El a'  -- coe
subst* : {a a' : U} → .(p : a ≡ᵤ a') → {x y : El a} → x ~ y → subst p x ~ subst p y

data U where
  u : U
  π : (a : U) → (b : El a → U) → (b₁ : ∀ {x y : El a} (p : x ~ y) → b x ≡ᵤ b y) → U

El u = U
El (π a b b₁) = Σ (∀ x → El (b x)) λ f → ∀ {x y : El a} (p : x ~ y) → subst (b₁ p) (f x) ~ f y
-- El (π a b b₁) = ∀ x → El (b x)

_≡ᵤ_ = _~_ {u}

-- El is a Setoid family indexed by U
refl* : {a : U} → (x : El a) → subst (≡ᵤrefl a) x ~ x 
trans* : {a₀ a₁ a₂ : U} → (p : a₀ ≡ᵤ a₁) (q : a₁ ≡ᵤ a₂) → (x : El a₀) → subst q (subst p x) ~ subst (≡ᵤtrans p q) x
sym* : {a₀ a₁ : U} → (p : a₀ ≡ᵤ a₁) → (x : El a₀) → subst (≡ᵤsym p) (subst p x) ~ x



---------------------------------
--  Definitions
---------------------------------

_~_ {u} u u = ⊤
_~_ {u} u (π a b b₁) = ⊥
_~_ {u} (π a b b₁) u = ⊥
_~_ {u} (π a b b₁) (π a' b' b₁') = Σ (a ≡ᵤ a') (λ p → ∀ x → b x ≡ᵤ b' (subst p x))
_~_ {π a b b₁} (f , f₁) (g , g₁) = ∀ (x y : El a) (p : x ~ y) → subst (b₁ p) (f x) ~ g y

≡ᵤrefl {u} u = tt
≡ᵤrefl {u} (π a b b₁) = ≡ᵤrefl a , λ x → b₁ (≡ᵤsym {a} (refl* x))
≡ᵤrefl {π a b b₁} = λ {(f , f₁) x y p → f₁ p}

≡ᵤsym {u} {u} {u} p = tt
≡ᵤsym {u} {u} {π a b b₁} ()
≡ᵤsym {u} {π a b b₁} {u} ()
≡ᵤsym {u} {π a b b₁} {π a' b' b₁'} (p , q) =
    ≡ᵤsym p , λ x → ≡ᵤsym (≡ᵤtrans (q (subst (≡ᵤsym p) x)) (b₁' (sym* (≡ᵤsym p) x)))
≡ᵤsym {π a b b₁} {f , f₁} {g , g₁} q =
    λ x y p → ≡ᵤsym (≡ᵤtrans (≡ᵤsym (sym* (≡ᵤsym (b₁ p)) (f y))) (subst* (b₁ p) (q y x (≡ᵤsym p))))

≡ᵤtrans {u} {u} {u} {u} p q = tt
≡ᵤtrans {u} {u} {u} {π z b b₁} p ()
≡ᵤtrans {u} {u} {π y b b₁} {z} ()
≡ᵤtrans {u} {π x b b₁} {u} {z} ()
≡ᵤtrans {u} {π x b b₁} {π y b₂ b₃} {u} p ()
≡ᵤtrans {u} {π a b b₁} {π a' b' b₁'} {π a'' b'' b₁''} (p , p') (q , q') =
      ≡ᵤtrans p q , λ x → ≡ᵤtrans (p' x) (≡ᵤtrans (q' (subst p x)) (b₁'' (trans* p q x)))
≡ᵤtrans {π a b b₁} {f , f₁} {g , g₁} {h , h₁} q q' =
      λ x y p → ≡ᵤtrans (subst* (b₁ p) (≡ᵤtrans (≡ᵤsym (refl* (f x))) (q x x (≡ᵤrefl x)))) (q' _ _ p)




-- subst  : {a a' : U} → .(p : a ≡ᵤ a') → El a → El a'
subst {u} {u} p x = x
subst {u} {π a' b' b₁'} () x
subst {π a b b₁} {u} () x
subst {π a b b₁} {π a' b' b₁'} (p₁ , p₂) (f , f₁) =
  (λ x → subst (≡ᵤtrans {u} {b (subst (≡ᵤsym {u} {a} p₁) x)} (p₂ (subst (≡ᵤsym {u} {a} {a'} p₁) x)) (b₁' (sym* (≡ᵤsym {u} {a} p₁) x)) )
               (f (subst (≡ᵤsym {u} {a} {a'} p₁) x)))
  , λ {x} {y} p → let p₁ₛ = ≡ᵤsym {u} {a} p₁ in
                  let i = (f₁ {subst p₁ₛ x} {subst p₁ₛ y} (subst* {a'} {a} p₁ₛ p)) in
                  let j1 = (b₁' (sym* {a'} {a} p₁ₛ y)) in
                  let j2 = (≡ᵤtrans {u} {b (subst p₁ₛ y)} {b' (subst p₁ (subst p₁ₛ y))} {b' y} ((p₂ (subst p₁ₛ y))) j1) in
                  let j3 = subst* {_} {b' y} j2 {_} {f (subst p₁ₛ y)} i in
                  let mg = (subst {b' x} {b' y} (b₁' {x} {y} p) (subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)} {b' x} (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)} {b' (subst {a} {a'} p₁ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))} {b' x} (p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)) (b₁' {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)) (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)} {x} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))) (f (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)))) in
                  ≡ᵤtrans {b' y} {mg}
                  (≡ᵤtrans {b' y} {mg} (trans* (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)}
         {b' (subst {a} {a'} p₁ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))}
         {b' x} (p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))
         (b₁'
          {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)}
          {x} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))) (b₁' p) (f (subst p₁ₛ x)))
          (≡ᵤsym {b' y} {(subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)} {b' y}
       (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
        {b' (subst {a} {a'} p₁ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))}
        {b' y} (p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))
        (b₁'
         {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
         {y} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)))
       (subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)}
        {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
        (b₁ {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x}
         {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y}
         (subst* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) {x} {y} p))
        (f (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))))}
        ((trans* {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)}
        {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
        (b₁ {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x}
         {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y}
         (subst* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) {x} {y} p))  (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
        {b' (subst {a} {a'} p₁ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))}
        {b' y} (p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))
        (b₁'
         {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
         {y} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))) (f (subst p₁ₛ x)))))) j3


-- subst* : {a a' : U} → .(p : a ≡ᵤ a') → {x y : El a} → x ~ y → subst p x ~ subst p y
subst* {u} {u} p q = q
subst* {u} {π a' b b₁} ()
subst* {π a b b₁} {u} ()
subst* {π a b b₁} {π a' b' b₁'} (p₁ , p₂) {f , f₁} {g , g₁} q =
       λ x y p → ≡ᵤtrans {b' y} {(subst {b' x} {b' y} (b₁' {x} {y} p)
       (subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)} {b' x}
        (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)}
         {b'
          (subst {a} {a'} (p₁) (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x))}
         {b' x} ((p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)))
         (b₁'
          {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)))
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)}
          {x} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)))
        (f (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x))))} {_} {(subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) y)} {b' y}
       (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) y)}
        {b'
         (subst {a} {a'} (p₁) (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) y))}
        {b' y} ((p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) y)))
        (b₁'
         {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)))
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) y)}
         {y} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) y)))
       (g (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) y)))} (trans* (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)}
         {b'
          (subst {a} {a'} (p₁) (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x))}
         {b' x} ((p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)))
         (b₁'
          {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)))
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)}
          {x} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x))) (b₁' {x} {y} p) (f (subst {a'} {a} (≡ᵤsym {u} {a} {a'} (p₁)) x)))

       ( ≡ᵤtrans {b' y} {(subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)} {b' y}
       (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)} {b' x}
        {b' y}
        (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)}
         {b' (subst {a} {a'} p₁ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))}
         {b' x} (p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))
         (b₁'
          {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)}
          {x} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)))
        (b₁' {x} {y} p))
       (f (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)))}

       (≡ᵤsym {b' y} {(subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)} {b' y}
       (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
        {b' (subst {a} {a'} p₁ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))}
        {b' y} (p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))
        (b₁'
         {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
         {y} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)))
       (subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)}
        {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
        (b₁ {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x}
         {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y}
         (subst* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) {x} {y} p))
        (f (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))))} (trans* (b₁ {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x}
         {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y}
         (subst* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) {x} {y} p)) (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
        {b' (subst {a} {a'} p₁ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))}
        {b' y} (p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))
        (b₁'
         {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
         {y} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))) (f (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x))))

       (subst* (≡ᵤtrans {u} {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
        {b' (subst {a} {a'} p₁ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))}
        {b' y} (p₂ (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))
        (b₁'
         {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
         {y} (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y))) {(subst {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)}
       {b (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y)}
       (b₁ {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x}
        {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) y}
        (subst* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) {x} {y} p))
       (f (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) x)))} (q _ _ (subst* (≡ᵤsym {u} {a} {a'} p₁) {x} {y} p))) )


-- refl* : {a : U} → (x : El a) → subst (≡ᵤrefl a) x ~ x 
refl* {u} u = tt
refl* {u} (π a b b₁) = ≡ᵤrefl a , λ x → b₁ (≡ᵤsym (refl* x))
refl* {π a b b₁} (f , f₁) x y p =
      ≡ᵤtrans {b y} {_} {subst (b₁ (≡ᵤtrans {a} {_} {x} {y} (refl* x) p)) (f (subst (≡ᵤrefl a) x))} {f y}
            (trans* {b (subst (≡ᵤrefl a) x)} {b x} {b y}
                    (≡ᵤtrans {u} {b (subst (≡ᵤrefl a) x)} {b (subst (≡ᵤrefl a) (subst (≡ᵤrefl a) x))} {b x}
                           (b₁ (≡ᵤsym {a} (refl* (subst (≡ᵤrefl a) x)))) (b₁ (sym* (≡ᵤrefl a) x)))
              (b₁ p) (f (subst (≡ᵤrefl a) x)))
            (f₁ {subst (≡ᵤrefl a) x} {y} (≡ᵤtrans {a} {subst (≡ᵤrefl a) x} {x} {y} (refl* x) p))


-- trans* : {a₀ a₁ a₂ : U} → (p : a₀ ≡ᵤ a₁) (q : a₁ ≡ᵤ a₂) → (x : El a₀) → subst q (subst p x) ~ subst (≡ᵤtrans p q) x
trans* {u} {u} {u} p q x = ≡ᵤrefl x
trans* {u} {u} {π a b b₁} p ()
trans* {u} {π a₁ b b₁} {_} ()
trans* {π a b b₁} {u} {a₂} ()
trans* {π a b b₁} {π a' b' b₁'} {u} p ()
trans* {π a b b₁} {π a' b' b₁'} {π a'' b'' b₁''} (p₁ , p₂) (q₁ , q₂) (f , f₁) x y p =
       ≡ᵤtrans {b'' y} {(subst {b'' x} {b'' y} (b₁'' {x} {y} p)
       (subst {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)} {b'' x}
        (≡ᵤtrans {u} {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         {b''
          (subst {a'} {a''} q₁ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b'' x} (q₂ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))
         (b₁''
          {subst {a'} {a''} (≡ᵤsym {u} {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁))
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          {x} (sym* {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
        (subst
         {b
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         (≡ᵤtrans {u}
          {b
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
          {b'
           (subst {a} {a'} p₁
            (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
             (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))}
          {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          (p₂
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
          (b₁'
           {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
            (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
             (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
           {subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x}
           (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))))
         (f
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))))))} {_} {(subst
       {b
        (subst {a''} {a}
         (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
       {b'' y}
       (≡ᵤtrans {u}
        {b
         (subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
        {b''
         (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
        {b'' y}
        (≡ᵤtrans {u}
         {b
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {b'
          (subst {a} {a'} p₁
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         {b''
          (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         (p₂
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))
         (≡ᵤtrans {u}
          {b'
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          {b''
           (subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}
          {b''
           (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          (q₂
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
          (b₁''
           {subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
           {subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
           (trans* {a} {a'} {a''} p₁ q₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))))
        (b₁''
         {subst {a} {a''}
          (≡ᵤsym {u} {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)))
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {y}
         (sym* {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
       (f
        (subst {a''} {a}
         (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}
       (trans* (≡ᵤtrans {u} {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         {b''
          (subst {a'} {a''} q₁ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b'' x} (q₂ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))
         (b₁''
          {subst {a'} {a''} (≡ᵤsym {u} {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁))
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          {x} (sym* {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))) (b₁'' {x} {y} p) (subst
         {b
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         (≡ᵤtrans {u}
          {b
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
          {b'
           (subst {a} {a'} p₁
            (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
             (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))}
          {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          (p₂
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
          (b₁'
           {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
            (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
             (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
           {subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x}
           (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))))
         (f
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))))
       (≡ᵤtrans {b'' y} {(subst {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)} {b'' y}
       (≡ᵤtrans {u} {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
        {b'' x} {b'' y}
        (≡ᵤtrans {u} {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         {b''
          (subst {a'} {a''} q₁ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b'' x} (q₂ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))
         (b₁''
          {subst {a'} {a''} (≡ᵤsym {u} {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁))
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          {x} (sym* {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
        (b₁'' {x} {y} p))
       (subst
        {b
         (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
        {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
        (≡ᵤtrans {u}
         {b
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b'
          (subst {a} {a'} p₁
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))}
         {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         (p₂
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
         (b₁'
          {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
          {subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x}
          (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))))
        (f
         (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))))} {_} {(subst
       {b
        (subst {a''} {a}
         (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
       {b'' y}
       (≡ᵤtrans {u}
        {b
         (subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
        {b''
         (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
        {b'' y}
        (≡ᵤtrans {u}
         {b
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {b'
          (subst {a} {a'} p₁
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         {b''
          (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         (p₂
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))
         (≡ᵤtrans {u}
          {b'
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          {b''
           (subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}
          {b''
           (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          (q₂
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
          (b₁''
           {subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
           {subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
           (trans* {a} {a'} {a''} p₁ q₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))))
        (b₁''
         {subst {a} {a''}
          (≡ᵤsym {u} {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)))
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {y}
         (sym* {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
       (f
        (subst {a''} {a}
         (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}
         (trans* (≡ᵤtrans {u}
         {b
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b'
          (subst {a} {a'} p₁
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))}
         {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         (p₂
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
         (b₁'
          {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
          {subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x}
          (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))) (≡ᵤtrans {u} {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
        {b'' x} {b'' y}
        (≡ᵤtrans {u} {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         {b''
          (subst {a'} {a''} q₁ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b'' x} (q₂ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))
         (b₁''
          {subst {a'} {a''} (≡ᵤsym {u} {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁))
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          {x} (sym* {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
        (b₁'' {x} {y} p)) (f
         (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))))
          let f₁' = f₁ {(subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))} {(subst {a''} {a} (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)} (≡ᵤtrans {a} {(subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁) (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))} {_} {(subst {a''} {a} (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)} (trans* (≡ᵤsym {u} {a'} {a''} q₁) (≡ᵤsym {u} {a} {a'} p₁) x) (subst* (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) {x} {y} p)) in
          ≡ᵤtrans {b'' y} {(subst
       {b
        (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
         (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
       {b'' y}
       (≡ᵤtrans {u}
        {b
         (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
        {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)} {b'' y}
        (≡ᵤtrans {u}
         {b
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
         {b'
          (subst {a} {a'} p₁
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))}
         {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         (p₂
          (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
         (b₁'
          {subst {a} {a'} (≡ᵤsym {u} {a'} {a} (≡ᵤsym {u} {a} {a'} p₁))
           (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
          {subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x}
          (sym* {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))))
        (≡ᵤtrans {u} {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         {b'' x} {b'' y}
         (≡ᵤtrans {u} {b' (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          {b''
           (subst {a'} {a''} q₁ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
          {b'' x} (q₂ (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))
          (b₁''
           {subst {a'} {a''} (≡ᵤsym {u} {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁))
            (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
           {x} (sym* {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))
         (b₁'' {x} {y} p)))
       (f
        (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
         (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))))} {_} {(subst
       {b
        (subst {a''} {a}
         (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
       {b'' y}
       (≡ᵤtrans {u}
        {b
         (subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
        {b''
         (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
        {b'' y}
        (≡ᵤtrans {u}
         {b
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {b'
          (subst {a} {a'} p₁
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         {b''
          (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         (p₂
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))
         (≡ᵤtrans {u}
          {b'
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          {b''
           (subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}
          {b''
           (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          (q₂
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
          (b₁''
           {subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
           {subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
           (trans* {a} {a'} {a''} p₁ q₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))))
        (b₁''
         {subst {a} {a''}
          (≡ᵤsym {u} {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)))
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {y}
         (sym* {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
       (f
        (subst {a''} {a}
         (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}

       (≡ᵤsym {b'' y} {(subst
       {b
        (subst {a''} {a}
         (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
       {b'' y}
       (≡ᵤtrans {u}
        {b
         (subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
        {b''
         (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
        {b'' y}
        (≡ᵤtrans {u}
         {b
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {b'
          (subst {a} {a'} p₁
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         {b''
          (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         (p₂
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))
         (≡ᵤtrans {u}
          {b'
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          {b''
           (subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}
          {b''
           (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          (q₂
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
          (b₁''
           {subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
           {subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
           (trans* {a} {a'} {a''} p₁ q₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))))
        (b₁''
         {subst {a} {a''}
          (≡ᵤsym {u} {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)))
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {y}
         (sym* {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
       (subst
        {b
         (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x))}
        {b
         (subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
        (b₁
         {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         {subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y}
         (≡ᵤtrans {a}
          {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          {subst {a''} {a}
           (≡ᵤtrans {u} {a''} {a'} {a} (≡ᵤsym {u} {a'} {a''} q₁)
            (≡ᵤsym {u} {a} {a'} p₁))
           x}
          {subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y}
          (trans* {a''} {a'} {a} (≡ᵤsym {u} {a'} {a''} q₁)
           (≡ᵤsym {u} {a} {a'} p₁) x)
          (subst* {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) {x} {y} p)))
        (f
         (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))))} (trans* (b₁
         {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
         {subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y}
         (≡ᵤtrans {a}
          {subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
           (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)}
          {subst {a''} {a}
           (≡ᵤtrans {u} {a''} {a'} {a} (≡ᵤsym {u} {a'} {a''} q₁)
            (≡ᵤsym {u} {a} {a'} p₁))
           x}
          {subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y}
          (trans* {a''} {a'} {a} (≡ᵤsym {u} {a'} {a''} q₁)
           (≡ᵤsym {u} {a} {a'} p₁) x)
          (subst* {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) {x} {y} p))) (≡ᵤtrans {u}
        {b
         (subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
        {b''
         (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
        {b'' y}
        (≡ᵤtrans {u}
         {b
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {b'
          (subst {a} {a'} p₁
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         {b''
          (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         (p₂
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))
         (≡ᵤtrans {u}
          {b'
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          {b''
           (subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}
          {b''
           (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          (q₂
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
          (b₁''
           {subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
           {subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
           (trans* {a} {a'} {a''} p₁ q₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))))
        (b₁''
         {subst {a} {a''}
          (≡ᵤsym {u} {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)))
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {y}
         (sym* {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))) (f
         (subst {a'} {a} (≡ᵤsym {u} {a} {a'} p₁)
          (subst {a''} {a'} (≡ᵤsym {u} {a'} {a''} q₁) x)))))


       (subst* (≡ᵤtrans {u}
        {b
         (subst {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
        {b''
         (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
        {b'' y}
        (≡ᵤtrans {u}
         {b
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {b'
          (subst {a} {a'} p₁
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         {b''
          (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
           (subst {a''} {a}
            (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
         (p₂
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))
         (≡ᵤtrans {u}
          {b'
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          {b''
           (subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))}
          {b''
           (subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
          (q₂
           (subst {a} {a'} p₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))
          (b₁''
           {subst {a'} {a''} q₁
            (subst {a} {a'} p₁
             (subst {a''} {a}
              (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))}
           {subst {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
           (trans* {a} {a'} {a''} p₁ q₁
            (subst {a''} {a}
             (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)))))
        (b₁''
         {subst {a} {a''}
          (≡ᵤsym {u} {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)))
          (subst {a''} {a}
           (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y)}
         {y}
         (sym* {a''} {a}
          (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))) {_} {(f
        (subst {a''} {a} (≡ᵤsym {u} {a} {a''} (≡ᵤtrans {u} {a} {a'} {a''} p₁ q₁)) y))} f₁')
          )


-- sym* can be defined in with refl* and trans*
sym* {a} {a'} p x = ≡ᵤtrans {a} (trans* p (≡ᵤsym {u} {a} p) x) (refl* x)
