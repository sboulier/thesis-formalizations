module lib where

record Σ (A : Set) (B : A → Set) : Set where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

infixl 5 _,_

open Σ public

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

data ⊥ : Set where

⊥-elim : ∀{A : Set} → ⊥ → A
⊥-elim ()

record ⊤ : Set where
  constructor tt

_≡ℕ_ : ℕ → ℕ → Set
zero ≡ℕ zero = ⊤
zero ≡ℕ (suc _) = ⊥
(suc _) ≡ℕ zero = ⊥
(suc m₁) ≡ℕ (suc m₂) = m₁ ≡ℕ m₂

ℕrefl : (m : ℕ) → m ≡ℕ m
ℕrefl zero = tt
ℕrefl (suc m) = ℕrefl m

ℕsym : {m₁ m₂ : ℕ} → .(m₁ ≡ℕ m₂) → m₂ ≡ℕ m₁
ℕsym {zero} {zero} _ = tt
ℕsym {zero} {suc m₂} ()
ℕsym {suc m₁} {zero} ()
ℕsym {suc m₁} {suc m₂} = ℕsym {m₁} {m₂}

ℕtrans : {m₁ m₂ m₃ : ℕ} → .(m₁ ≡ℕ m₂) → .(m₂ ≡ℕ m₃) → m₁ ≡ℕ m₃
ℕtrans {zero} {zero} {zero} _ _ = tt
ℕtrans {zero} {zero} {suc m₃} _ ()
ℕtrans {zero} {suc m₂} {m₃} ()
ℕtrans {suc m₁} {zero} {m₃} ()
ℕtrans {suc m₁} {suc m₂} {zero} _ ()
ℕtrans {suc m₁} {suc m₂} {suc m₃} = ℕtrans {m₁} {m₂} {m₃}
