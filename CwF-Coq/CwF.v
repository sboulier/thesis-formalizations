(* -*- coq-prog-name : "/home/rascar/Dépôts/coq-sprop/bin/coqtop" coq-prog-args : ("-allow-sprop")-*-  *)

Require Export MiniHoTT.


(* The collection of all the operations needed to form a CwF. *)
(* We use a module instead of a record to be able to use some notations. *)
(* CwF supporting Π types and an (empty) universe. *)

Module Type CwF.
  Parameter Con : Type.
  Parameter Ty  : Con -> Type.
  Parameter Tms : Con -> Con -> Type.
  Parameter Tm  : forall (Γ : Con), Ty Γ -> Type.

  Parameter nil  : Con.
  Parameter cons : forall (Γ : Con), Ty Γ -> Con.
  Notation "∙" := nil.
  Notation "( x , y )" := (cons x y).

  Parameter TyS : forall {Γ Δ}, Tms Γ Δ -> Ty Δ -> Ty Γ.
  Notation " A [ σ ]T " := (TyS σ A) (at level 3).
  Parameter TmS  : forall {Γ Δ A}(σ : Tms Γ Δ), Tm Δ A -> Tm Γ A[σ]T.
  Notation " t [ σ ]t " := (TmS σ t) (at level 3).

  Parameter ε    : forall {Γ}, Tms Γ nil.
  Parameter id   : forall {Γ}, Tms Γ Γ.
  Parameter comp : forall {Γ Δ Σ}, Tms Δ Σ -> Tms Γ Δ -> Tms Γ Σ.
  Parameter ext  : forall {Γ Δ A}(σ : Tms Γ Δ), Tm Γ A[σ]T -> Tms Γ (Δ, A).
  Notation "( x ,s y )" := (ext x y).
  Infix "∘" := comp (at level 42, right associativity).
  Parameter π₁   : forall {Γ Δ A}, Tms Γ (Δ, A) -> Tms Γ Δ.
  Parameter π₂   : forall {Γ Δ A}(σ : Tms Γ (Δ, A)), Tm Γ A[π₁ σ]T.

  Parameter idl     : forall {Γ Δ}{σ : Tms Γ Δ}, id ∘ σ = σ.
  Parameter idr     : forall {Γ Δ}{σ : Tms Γ Δ}, σ ∘ id = σ.
  Parameter ass     : forall {Γ Δ Σ Ξ}{σ : Tms Σ Ξ}{δ : Tms Δ Σ}{ν : Tms Γ Δ},
      σ ∘ (δ ∘ ν) = (σ ∘ δ) ∘ ν.
  Parameter TyId    : forall {Γ}{A : Ty Γ}, TyS id A = A.
  Parameter TyComp  : forall {Γ Δ Σ}{A : Ty Σ}{σ : Tms Δ Σ}{δ : Tms Γ Δ},
      TyS δ (TyS σ A) = TyS (comp σ δ) A.
  Parameter εη      : forall {Γ}{σ : Tms Γ nil}, σ = ε.
  Parameter π₁β     : forall {Γ Δ A}{σ : Tms Γ Δ}{t : Tm Γ A[σ]T}, π₁ (σ ,s t) = σ.
  Parameter π₂β     : forall {Γ Δ A}{σ : Tms Γ Δ}{t : Tm Γ (TyS σ A)},
      transport (fun σ => Tm Γ A[σ]T) π₁β (π₂ (σ ,s t)) = t.
  Parameter extComp : forall {Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ A[δ]T},
      (δ ,s a) ∘ σ = (δ ∘ σ ,s TyComp # a[σ]t).
  Parameter extη    : forall {Γ Δ A}{σ : Tms Γ (Δ, A)}, (π₁ σ ,s π₂ σ) = σ.

  (* Definitions *)
  Definition wk {Γ}{A : Ty Γ} : Tms (Γ, A) Γ := π₁ id.
  Definition vz {Γ}{A : Ty Γ} : Tm (Γ, A) (TyS wk A) := π₂ id.
  Definition vs {Γ}{A B : Ty Γ}(t : Tm Γ A) : Tm (Γ, B) (TyS wk A) := TmS wk t.
  Definition drop {Γ Δ}{A : Ty Γ}(σ : Tms Γ Δ) : Tms (Γ, A) Δ := σ ∘ wk.
  Definition keep {Γ Δ}{A : Ty Δ}(σ : Tms Γ Δ) : Tms (Γ, A[σ]T) (Δ, A)
    := (drop σ ,s TyComp # vz).
  Notation " σ ^^ A " := (@keep _ _ A σ) (at level 30).
  Definition ext' {Γ} {A : Ty Γ} (t : Tm Γ A) : Tms Γ (Γ , A)
    := (id ,s TyId^ # t).
  Notation "<< t >>" := (ext' t) (at level 1000).

  (* Universe *)
  Parameter U   : forall {Γ}, Ty Γ.
  Parameter El  : forall {Γ}, Tm Γ U -> Ty Γ.
  Parameter US  : forall {Γ Δ}{σ : Tms Γ Δ}, TyS σ U = U.
  Parameter ElS : forall {Γ Δ}{σ : Tms Γ Δ}{A : Tm Δ U},
      (El A)[σ]T = El (transport (Tm Γ) US A[σ]t).

  (* Π types *)
  Parameter Π   : forall {Γ}(A : Ty Γ), Ty (Γ, A) -> Ty Γ.
  Parameter lam  : forall {Γ A B}, Tm (Γ, A) B -> Tm Γ (Π A B).
  Parameter app  : forall {Γ A B}, Tm Γ (Π A B) -> Tm (Γ, A) B.

  Parameter ΠS : forall {Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ, A)},
      TyS σ (Π A B) = Π (TyS σ A) (TyS (keep σ) B).
  Parameter lamS : forall {Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ, A)}{t : Tm (Δ, A) B},
      transport (Tm Γ) ΠS (TmS δ (lam t)) = lam (TmS (keep δ) t).
  Parameter Πβ : forall {Γ}{A : Ty Γ}{B : Ty (Γ, A)}{t : Tm (Γ, A) B}, app (lam t) = t.
  Parameter Πη : forall {Γ}{A : Ty Γ}{B : Ty (Γ, A)}{t : Tm Γ (Π A B)}, lam (app t) = t.

  Definition app' {Γ} {A} {B} (t : Tm Γ (Π A B)) (u : Tm Γ A) : Tm Γ B[<<u>>]T
    := (app t)[_]t.


  (* π₁∘ *)
  Definition π₁_comp {Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
    : π₁ δ ∘ ρ = π₁ (δ ∘ ρ).
    etransitivity. symmetry. eapply π₁β.
    eapply ap. etransitivity.
    symmetry. eapply extComp.
    eapply (ap (fun z => z ∘ ρ)), extη.
  Defined.

  Definition π₁idβ {Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ A[ρ]T}
    : π₁ id ∘ (ρ ,s t) = ρ.
    etransitivity. eapply π₁_comp.
    etransitivity. 2: eapply π₁β.
    eapply ap, idl.
  Defined.

  Definition ap_π₂ {Γ Δ} {A : Ty Δ} {ρ₀ ρ₁ : Tms Γ (Δ , A)} (ρ₂ : ρ₀ = ρ₁)
    : ap (fun ρ => A[π₁ ρ]T) ρ₂ # π₂ ρ₀ = π₂ ρ₁.
    destruct ρ₂; reflexivity.
  Defined.

  Definition ap_π₂' {Γ Δ} {A : Ty Δ} {ρ₀ ρ₁ : Tms Γ (Δ , A)} (ρ₂ : ρ₀ = ρ₁)
    : π₂ ρ₀ = ap (fun ρ => A[π₁ ρ]T) ρ₂^ # π₂ ρ₁.
    destruct ρ₂; reflexivity.
  Defined.

  Notation "'TyS'' A" := (fun σ => TyS σ A) (at level 30).

  Definition π₂_subst {Γ Δ Θ} {A : Ty Δ} {δ : Tms Γ (Δ , A)} {ρ : Tms Θ Γ}
    : TyComp @ ap (fun σ => A[σ]T) π₁_comp # (π₂ δ)[ ρ ]t =  π₂ (δ ∘ ρ).
    (* : TyComp @ ap10 (ap TyS π₁_comp) A # (π₂ δ)[ ρ ]t =  π₂ (δ ∘ ρ). *)
    etransitivity. 2: exact (ap_π₂ (ap (fun z => z ∘ _) extη)).
    rewrite transport_pp. rewrite <- (@π₂β _ _ A (π₁ δ ∘ ρ) (TyComp # (π₂ δ) [ρ ]t)).
    eapply moveL_transport_p. etransitivity.
    2: exact (ap_π₂ extComp^).
    rewrite (transport_compose (Tm Θ) (TyS' A)).
    rewrite !(ap_compose (π₁) (TyS' A)).
    rewrite <- !transport_pp. eapply ap10, ap.
    rewrite concat_p_pp. refine (_ @@ 1 @ _). exact (ap_pp _ _ _)^.
    refine (1 @@ (ap_V _ _)^ @ _ @ _).
    exact (ap_pp _ _ _)^. eapply ap.
    unfold π₁_comp. rewrite concat_p_pp. rewrite concat_pV, concat_1p.
    rewrite ap_pp. rewrite concat_pp_p. rewrite concat_pV. apply concat_p1.
  Defined.

  Definition π₂idβ {Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ A[ρ]T}
    : transport (fun ρ => Tm Γ A [ρ]T) π₁idβ
                (transport (Tm Γ) TyComp ((π₂ id) [ (ρ ,s t) ]t)) = t.
    rewrite transport_compose. rewrite <- transport_pp.
    refine (_ @ π₂β).
    pose proof (π₂_subst (δ:=id) (ρ:=(ρ,s t))).
    eapply moveL_transport_V in X. rewrite X; clear X.
    rewrite <- transport_pp. etransitivity.
    eapply ap. eapply (ap_π₂ idl^)^.
    rewrite (transport_compose (Tm Γ) (TyS' A)).
    rewrite <- transport_pp. eapply ap10, ap.
    rewrite (ap_compose (π₁) (TyS' A)).
    rewrite !inv_pp. rewrite !ap_V. eapply moveR_Vp.
    rewrite concat_pp_p. eapply moveR_Vp.
    rewrite concat_p_pp. rewrite concat_Vp, concat_1p.
    refine (_ @ (1 @@ ap_pp _ _ _)).
    refine (ap_pp _ _ _).
  Defined.

  Definition π₂idβ' {Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ A[ρ]T}
    : (π₂ id) [ (ρ ,s t) ]t
      = transport (Tm Γ) (ap (TyS' A) π₁idβ^ @ TyComp^) t.
  Proof.
    rewrite transport_pp. rewrite <- (transport_compose (Tm Γ) (fun ρ => A [ρ ]T)).
    eapply moveL_transport_V.
    eapply (moveL_transport_V (fun ρ  => Tm Γ A[ρ]T)).
    apply π₂idβ.
  Defined.

  Definition transport_TmS {Γ Δ A A'} {u : Tm Δ A} {δ : Tms Γ Δ} (p : A = A')
    : (transport (Tm Δ) p u)[δ]t = transport (Tm Γ) (ap (TyS δ) p) (u[δ]t).
    destruct p; reflexivity.
  Defined.

  (* <>∘ *)
  Definition ext'_comp {Γ Δ A} {u : Tm Δ A} {δ : Tms Γ Δ}
    : (<< u >> ∘ δ) = (δ ^^ A) ∘ << u [ δ ]t >>.
  etransitivity. eapply extComp.
  etransitivity. 2: symmetry; eapply extComp.
  unfold drop, wk.
  unshelve eapply (ap011D (@ext _ _ _)).
  - etransitivity. eapply (@idl _ _ δ).
    etransitivity. symmetry; eapply idr.
    etransitivity. 2: eapply ass.
    eapply ap. symmetry; eapply π₁idβ.
  - rewrite transport_compose, <- transport_pp.
    eapply moveR_transport_p.
    rewrite <- transport_pp.
    unfold ext', vz. rewrite !transport_TmS.
    rewrite <- transport_pp. eapply moveL_transport_p.
    rewrite <- transport_pp. 
    symmetry. etransitivity. eapply π₂idβ'.
    rewrite  <- transport_pp. eapply ap10, ap.
    rewrite !inv_pp. rewrite !inv_V.
    rewrite !ap_pp. rewrite <- !ap_compose.
    match goal with
    | |- ?pp1 @ (?pp2 @ ?pp3) = ?pp4 @ (?XX @ ?pp11) => set (p1:=pp1); set (p2:=pp2); set (p3:=pp3); set (p4:=pp4); set (p11:=pp11);
         change (p1 @ (p2 @ p3) = p4 @ (XX @ p11))
    end.
    set (@π₁idβ _ _ id A [δ ]T (transport (Tm Γ) TyId^ u [δ ]t)).
  Admitted.
End CwF.


(* CwF supporting Σ types. *)

Module Type hasΣ (C : CwF).
  Import C.
  Parameter Σ' : forall {Γ}(A : Ty Γ)(B : Ty (Γ , A)), Ty Γ.

  Parameter ΣS : forall {Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)},
      (Σ' A B)[σ]T = Σ' (A [σ]T) B[σ ^^ A]T.

  Parameter pair' : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}(u : Tm Γ A)(v : Tm Γ B[<<u>>]T),
      Tm Γ (Σ' A B).
  Parameter proj₁' : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B)), Tm Γ A.
  Parameter proj₂' : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B)),
      Tm Γ B[<<proj₁' w>>]T.
  Parameter Σβ₁ : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ B[<<u>>]T},
      proj₁' (pair' u  v) = u.
  Parameter Σβ₂ : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ B[<<u>>]T},
      transport (fun u => Tm Γ (B [(<< u >>) ]T)) Σβ₁ (proj₂' (pair' u v)) = v.
  Parameter Ση : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}{w : Tm Γ (Σ' A B)},
      pair' (proj₁' w) (proj₂' w) = w.

  Parameter pairS : forall {Γ}{A : Ty Γ}{B : Ty (Γ, A)}{u : Tm Γ A}{v : Tm Γ B[<<u>>]T}
                      {Θ}{σ : Tms Θ Γ},
      transport (Tm _) ΣS (pair' u v)[σ]t
      = pair' u[σ]t (transport (Tm _)
                               (TyComp @ ap (fun σ => TyS σ B) ext'_comp @ TyComp^)
                               v[σ]t).
End hasΣ.
