# CwF-Coq

Formalization of categories with families in Coq.

- `MiniHoTT.v` contains a small library on equality

- `CwF.v` contains the definition of a CwF

- `StandardModel.v` contains the definition the standard model.
  Some rules are admitted because we don't have the η-rule on unit type

- `SetoidModel.v` contains the definition of the setoid model of Altenkirsh.
  It also contains:
	  - the equivalence between SetoidFam and SetoidFam'
	  - the construction of Funext and the proof that it computes somehow
  

## Compilation

To compile run:

```
	coq_makefile -f _CoqProject -o Makefile
	make
```

The last file requires Coq-sprop to compile. Todo explain.
