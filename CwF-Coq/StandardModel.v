(* -*- coq-prog-name : "/home/rascar/Dépôts/coq-sprop/bin/coqtop" coq-prog-args : ("-allow-sprop")-*-  *)

Require Import CwF.

Module SetModel <: CwF.
  Definition Con : Type := Type.
  Definition Ty  : Con -> Type := fun Γ => Γ -> Type.
  Definition Tms : Con -> Con -> Type := fun Γ Δ => Γ -> Δ.
  Definition Tm  : forall (Γ : Con), Ty Γ -> Type := fun Γ A => forall γ, A γ.

  Definition nil  : Con := unit.
  Definition cons : forall (Γ : Con), Ty Γ -> Con := fun Γ A => @sigT Γ A.
  Notation "∙" := nil.
  Notation "( x , y )" := (cons x y).

  Definition U   : forall {Γ}, Ty Γ := fun _ _ => Type.
  Definition El  : forall {Γ}, Tm Γ U -> Ty Γ := fun _ A => A.
  Definition Π   : forall {Γ}(A : Ty Γ), Ty (Γ, A) -> Ty Γ.
    intros Γ A B γ. exact (forall x : A γ, B (γ; x)). Defined.
  Definition TyS : forall {Γ Δ}, Tms Γ Δ -> Ty Δ -> Ty Γ.
    intros Γ Δ σ A. exact (A o σ). Defined.

  Definition ε    : forall {Γ}, Tms Γ nil.
    intros _ _. exact tt. Defined.
  Definition id   : forall {Γ}, Tms Γ Γ.
    intros Γ. exact idmap. Defined.
  Definition comp : forall {Γ Δ Σ}, Tms Δ Σ -> Tms Γ Δ -> Tms Γ Σ.
    intros Γ Δ Σ σ σ'. exact (σ o σ'). Defined.
  Definition ext  : forall {Γ Δ A}(σ : Tms Γ Δ), Tm Γ (TyS σ A) -> Tms Γ (Δ, A).
    intros Γ Δ A σ t. exact (fun γ => (σ γ; t γ)). Defined.
  Notation "( x ,s y )" := (ext x y).
  Infix "∘" := comp (at level 42, right associativity).

  Definition π₁   : forall {Γ Δ A}, Tms Γ (Δ, A) -> Tms Γ Δ.
    intros Γ Δ A σ. exact (@projT1 _ _ o σ). Defined.
  Definition π₂   : forall {Γ Δ A}(σ : Tms Γ (Δ, A)), Tm Γ (TyS (π₁ σ) A).
    intros Γ Δ A σ. exact (@projT2 _ _ o σ). Defined.
  Definition lam  : forall {Γ A B}, Tm (Γ, A) B -> Tm Γ (Π A B).
    intros Γ A B t γ x. exact (t  (γ; x)). Defined.
  Definition app  : forall {Γ A B}, Tm Γ (Π A B) -> Tm (Γ, A) B.
    intros Γ A B t γx. exact (t γx.1 γx.2). Defined.
  Definition TmS  : forall {Γ Δ A}(σ : Tms Γ Δ), Tm Δ A -> Tm Γ (TyS σ A).
    intros Γ Δ A σ t. exact (t o σ). Defined.

  Definition TyId    : forall {Γ}{A : Ty Γ}, TyS id A = A.
    intros; reflexivity. Defined.
  Definition TyComp  : forall {Γ Δ Σ}{A : Ty Σ}{σ : Tms Δ Σ}{δ : Tms Γ Δ}, TyS δ (TyS σ A) = TyS (comp σ δ) A.
    intros; reflexivity. Defined.
  Definition idl     : forall {Γ Δ}{σ : Tms Γ Δ}, id ∘ σ = σ.
    intros; reflexivity. Defined.
  Definition idr     : forall {Γ Δ}{σ : Tms Γ Δ}, σ ∘ id = σ.
    intros; reflexivity. Defined.
  Definition ass     : forall {Γ Δ Σ Ξ}{σ : Tms Σ Ξ}{δ : Tms Δ Σ}{ν : Tms Γ Δ}, σ ∘ (δ ∘ ν) = (σ ∘ δ) ∘ ν.
    intros; reflexivity. Defined.
  Definition εη      : forall {Γ}{σ : Tms Γ nil}, σ = ε.
    compute.
  Admitted.
  Definition π₁β     : forall {Γ Δ A}{σ : Tms Γ Δ}{t : Tm Γ (TyS σ A)}, π₁ (ext σ t) = σ.
    intros; reflexivity. Defined.
  Definition π₂β     : forall {Γ Δ A}{σ : Tms Γ Δ}{t : Tm Γ (TyS σ A)}, transport (fun σ => Tm Γ (TyS σ A)) π₁β (π₂ (ext σ t)) = t.
    intros; reflexivity. Defined.
  Definition extη    : forall {Γ Δ A}{σ : Tms Γ (Δ, A)}, ext (π₁ σ) (π₂ σ) = σ.
    intros; reflexivity. Defined.

  Definition extComp : forall {Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (TyS δ A)},
      (δ ,s a) ∘ σ = (δ ∘ σ ,s transport (Tm Σ) TyComp (TmS σ a)).
    intros; reflexivity. Defined.

  Definition US  : forall {Γ Δ}{σ : Tms Γ Δ}, TyS σ U = U.
    intros; reflexivity. Defined.
  Definition ElS : forall {Γ Δ}{σ : Tms Γ Δ}{A : Tm Δ U}, TyS σ (El A) = El (transport (Tm Γ) US (TmS σ A)).
    intros; reflexivity. Defined.

  Definition wk {Γ}{A : Ty Γ} : Tms (Γ, A) Γ := π₁ id.
  Definition vz {Γ}{A : Ty Γ} : Tm (Γ, A) (TyS wk A) := π₂ id.
  Definition vs {Γ}{A B : Ty Γ}(t : Tm Γ A) : Tm (Γ, B) (TyS wk A) := TmS wk t.
  Definition drop {Γ Δ}{A : Ty Γ}(σ : Tms Γ Δ) : Tms (Γ, A) Δ := σ ∘ wk.
  Definition keep {Γ Δ}{A : Ty Δ}(σ : Tms Γ Δ) : Tms (Γ, (TyS σ A)) (Δ, A) := ext (drop σ) (transport (Tm _) TyComp vz).

  Definition ΠS : forall {Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ, A)}, TyS σ (Π A B) = Π (TyS σ A) (TyS (keep σ) B).
    intros; reflexivity. Defined.

  Definition lamS : forall {Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ, A)}{t : Tm (Δ, A) B},
      transport (Tm Γ) ΠS (TmS δ (lam t)) = lam (TmS (keep δ) t).
    intros; reflexivity. Defined.

  Definition Πβ : forall {Γ}{A : Ty Γ}{B : Ty (Γ, A)}{t : Tm (Γ, A) B}, app (lam t) = t.
    intros; reflexivity. Defined.
  Definition Πη : forall {Γ}{A : Ty Γ}{B : Ty (Γ, A)}{t : Tm Γ (Π A B)}, lam (app t) = t.
    intros; reflexivity. Defined.

  Definition ext' {Γ} {A : Ty Γ} (t : Tm Γ A) : Tms Γ (Γ , A)
    := (id ,s transport (Tm Γ) TyId^ t).

  Notation " A [ σ ]T " := (TyS σ A) (at level 3).
  Notation " σ ^^ A " := (@keep _ _ A σ) (at level 30).
  Notation "<< t >>" := (ext' t) (at level 1000).
  Notation " t [ σ ]t " := (TmS σ t) (at level 3).

  Definition app' {Γ} {A} {B} (t : Tm Γ (Π A B)) (u : Tm Γ A) : Tm Γ B[<<u>>]T
    := (app t)[_]t.


  (* π₁∘ *)
  Definition π₁_comp {Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
    : π₁ δ ∘ ρ = π₁ (δ ∘ ρ).
    etransitivity. symmetry. eapply π₁β.
    eapply ap. etransitivity.
    symmetry. eapply extComp.
    eapply (ap (fun z => z ∘ ρ)), extη.
  Defined.

  Definition π₁idβ {Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ A[ρ]T}
    : π₁ id ∘ (ρ ,s t) = ρ.
    etransitivity. eapply π₁_comp.
    etransitivity. 2: eapply π₁β.
    eapply ap, idl.
  Defined.

  Definition ap_π₂ {Γ Δ} {A : Ty Δ} {ρ₀ ρ₁ : Tms Γ (Δ , A)} (ρ₂ : ρ₀ = ρ₁)
    : ap (fun ρ => A[π₁ ρ]T) ρ₂ # π₂ ρ₀ = π₂ ρ₁.
    destruct ρ₂; reflexivity.
  Defined.

  Definition ap_π₂' {Γ Δ} {A : Ty Δ} {ρ₀ ρ₁ : Tms Γ (Δ , A)} (ρ₂ : ρ₀ = ρ₁)
    : π₂ ρ₀ = ap (fun ρ => A[π₁ ρ]T) ρ₂^ # π₂ ρ₁.
    destruct ρ₂; reflexivity.
  Defined.

  Notation "'TyS'' A" := (fun σ => TyS σ A) (at level 30).

  Definition π₂_subst {Γ Δ Θ} {A : Ty Δ} {δ : Tms Γ (Δ , A)} {ρ : Tms Θ Γ}
    : TyComp @ ap (fun σ => A[σ]T) π₁_comp # (π₂ δ)[ ρ ]t =  π₂ (δ ∘ ρ).
    (* : TyComp @ ap10 (ap TyS π₁_comp) A # (π₂ δ)[ ρ ]t =  π₂ (δ ∘ ρ). *)
    reflexivity.
  Defined.

  Definition π₂idβ {Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ A[ρ]T}
    : transport (fun ρ => Tm Γ A [ρ]T) π₁idβ
                (transport (Tm Γ) TyComp ((π₂ id) [ (ρ ,s t) ]t)) = t.
    reflexivity.
  Defined.

  Definition π₂idβ' {Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ A[ρ]T}
    : (π₂ id) [ (ρ ,s t) ]t
      = transport (Tm Γ) (ap (TyS' A) π₁idβ^ @ TyComp^) t.
  Proof.
    rewrite transport_pp. rewrite <- (transport_compose (Tm Γ) (fun ρ => A [ρ ]T)).
    eapply moveL_transport_V.
    eapply (moveL_transport_V (fun ρ  => Tm Γ A[ρ]T)).
    apply π₂idβ.
  Defined.

  Definition transport_TmS {Γ Δ A A'} {u : Tm Δ A} {δ : Tms Γ Δ} (p : A = A')
    : (transport (Tm Δ) p u)[δ]t = transport (Tm Γ) (ap (TyS δ) p) (u[δ]t).
    destruct p; reflexivity.
  Defined.

  (* <>∘ *)
  Definition ext'_comp {Γ Δ A} {u : Tm Δ A} {δ : Tms Γ Δ}
    : (<< u >> ∘ δ) = (δ ^^ A) ∘ << u [ δ ]t >>.
  reflexivity.
  Defined.
End SetModel.


Module SetModelΣ <: hasΣ SetModel.
  Import SetModel.

  Definition Σ' : forall {Γ}(A : Ty Γ)(B : Ty (Γ , A)), Ty Γ.
    intros Γ A B. unfold Ty. exact (fun γ => {x : A γ & B (γ; x)}). Defined.

  Definition ΣS : forall {Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)},
      ((Σ' A B) [ σ ]T) = (Σ' (A [ σ ]T) (B [ σ ^^ A ]T)).
    intros; reflexivity. Defined.

  Definition pair'  : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}(u : Tm Γ A)(v : Tm Γ (B [ << u >> ]T)), Tm Γ (Σ' A B).
    intros Γ A B u v. exact (fun γ => (u γ; v γ)). Defined.
  Definition proj₁' : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B)), Tm Γ A.
    intros Γ A B w. exact (fun γ => (w γ).1). Defined.
  Definition proj₂' : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B)), Tm Γ (B [ << proj₁' w >> ]T).
    intros Γ A B w. exact (fun γ => (w γ).2). Defined.
  Definition Σβ₁    : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ (B [ << u >> ]T)},
             proj₁' (pair' u  v) = u.
    intros; reflexivity. Defined.
  Definition Σβ₂    : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ (B [ << u >> ]T)},
             transport (fun u => Tm Γ (B [(<< u >>) ]T)) Σβ₁ (proj₂' (pair' u v)) = v.
    intros; reflexivity. Defined.
  Definition Ση     : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}{w : Tm Γ (Σ' A B)},
             pair' (proj₁' w) (proj₂' w) = w.
    intros; reflexivity. Defined.

  Definition pairS  : forall {Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ (B [ << u >> ]T)}
             {Θ}{σ : Tms Θ Γ},
              transport (Tm _) ΣS ((pair' u v) [ σ ]t)
              = pair' (u [ σ ]t) (transport (Tm _)
                  (TyComp @ ap (fun σ => TyS σ B) ext'_comp @ TyComp^)
                  (v [ σ ]t)).
    reflexivity.
  Defined.
End SetModelΣ.